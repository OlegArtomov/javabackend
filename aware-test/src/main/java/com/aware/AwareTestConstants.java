package com.aware;

/**
 * @author Oleg Artomov
 */
public final class AwareTestConstants {

    public static final String SET_MYSQL_SYNTAX_SCRIPT = "classpath:setMySQLSyntax.sql";

    public static final String TRUNCATE_ALL_DATA_SCRIPT = "classpath:truncateAllData.sql";

    public static final String DEFAULTS_TEST_PROPERTIES = "classpath:awareTestDefaults.properties";

    public static final String LIQUEBASE_CHANGELOG_XML = "db/db-changelog.xml";
}
