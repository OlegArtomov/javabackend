package com.aware.spring.mock;

import com.google.common.collect.ImmutableList;
import org.mockito.Mockito;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;

import java.util.Collection;

import static org.springframework.beans.factory.support.BeanDefinitionBuilder.rootBeanDefinition;
import static org.springframework.context.annotation.AnnotationConfigUtils.COMMON_ANNOTATION_PROCESSOR_BEAN_NAME;
import static org.springframework.util.Assert.isTrue;

/**
 * @author Oleg Artomov
 */
public class MocksReplacer implements BeanFactoryPostProcessor, BeanNameAware {

    private static final String FACTORY_METHOD_NAME = "getMock";

    private final Collection<String> mocks;

    private String beanName;

    public MocksReplacer(Collection<String> mocks) {
        this.mocks = new ImmutableList.Builder<String>()
                .addAll(mocks).add(COMMON_ANNOTATION_PROCESSOR_BEAN_NAME).build();
    }

    //OAR: If you rename this method, please, change value of  FACTORY_METHOD_NAME too
    public Object getMock(Class objectClass) {
        return Mockito.mock(objectClass);
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        String[] beanNames = beanFactory.getBeanDefinitionNames();
        for (String currentBeanName : beanNames) {
            if (mocks.contains(currentBeanName)) {
                try {
                    configureMock(beanFactory, currentBeanName);
                } catch (ClassNotFoundException e) {
                    throw new IllegalArgumentException();
                }
            }
        }
    }

    private void configureMock(ConfigurableListableBeanFactory beanFactory,
                               String currentBeanName) throws ClassNotFoundException {
        isTrue(beanFactory instanceof BeanDefinitionRegistry, "BeanDefinition must be BeanDefinitionRegistry too");
        BeanDefinition beanDefinition = beanFactory.getBeanDefinition(currentBeanName);
        Class<?> value = Class.forName(beanDefinition.getBeanClassName());
        BeanDefinition newBeanDefinition = rootBeanDefinition(value, FACTORY_METHOD_NAME)
                .setAutowireMode(0).addConstructorArgValue(value).getBeanDefinition();
        newBeanDefinition.setFactoryBeanName(beanName);
        BeanDefinitionRegistry registry = (BeanDefinitionRegistry) beanFactory;
        registry.registerBeanDefinition(currentBeanName, newBeanDefinition);
    }

    @Override
    public void setBeanName(String name) {
        this.beanName = name;
    }
}
