package com.aware.spring;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.util.Collection;

import static org.mockito.Mockito.spy;

/**
 * @author Oleg Artomov
 */
public class SpyDefinder implements BeanPostProcessor {

    private final Collection<String> spyBeans;

    public SpyDefinder(Collection<String> spyBeans) {
        this.spyBeans = spyBeans;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return spyBeans.contains(beanName) ? spy(bean) : bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }
}
