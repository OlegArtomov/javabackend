package com.aware.spring;

import org.mockito.Mockito;
import org.mockito.cglib.proxy.Factory;
import org.springframework.context.ApplicationContext;

import java.util.Collection;

import static java.beans.Introspector.decapitalize;
import static java.util.stream.Collectors.toList;
import static org.springframework.beans.factory.BeanFactoryUtils.beansOfTypeIncludingAncestors;

/**
 * @author Oleg Artomov
 */
public class BeanUtils {

    public static void resetMocksInApplicationContext(ApplicationContext context) {
        beansOfTypeIncludingAncestors(context, Factory.class).values().forEach(Mockito::reset);
    }

    public static String beanNameForClass(Class beanClass) {
        return decapitalize(beanClass.getSimpleName());
    }

    public static Collection<String> beanNamesForClasses(Collection<Class> beanClasses) {
        return beanClasses.stream().map(BeanUtils::beanNameForClass).collect(toList());
    }
}
