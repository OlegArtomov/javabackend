package com.aware.rest.controllers;

import com.aware.config.AwareProperties;
import com.aware.model.AwareUser;
import com.aware.repositories.AwareUserRepository;
import com.aware.rest.config.RestMvcConfiguration;
import com.aware.rest.controllers.config.TestControllersConfiguration;
import com.aware.services.config.AwareServicesConfiguration;
import com.aware.services.security.JwtService;
import com.aware.slackbot.config.SlackBotSharedConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;

import static com.aware.spring.BeanUtils.resetMocksInApplicationContext;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * @author Oleg Artomov
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextHierarchy({
        @ContextConfiguration(
                classes = {AwareServicesConfiguration.class, AwareProperties.class,
                        TestControllersConfiguration.class, SlackBotSharedConfig.class}
        ),
        @ContextConfiguration(
                classes = {RestMvcConfiguration.class}
        )
})
public abstract class AbstractControllerIT {

    protected MockMvc mockMvc;

    @Resource
    private WebApplicationContext webApplicationContext;

    @Resource
    private AwareUserRepository awareUserRepository;

    @Resource
    private JwtService jwtService;

    private Long currentUserId = 1L;

    private String currentUserEmail = "testUser@ukr.net";

    protected String currentAwareAccessToken = "JwtToken";

    protected AwareUser currentUser;


    @Before
    public void beforeEach() {
        mockMvc = webAppContextSetup(webApplicationContext).alwaysDo(print()).build();
        currentUser = new AwareUser.Builder().id(currentUserId).email(currentUserEmail).build();
        when(awareUserRepository.findOne(currentUserId)).thenReturn(currentUser);
        when(jwtService.parseJWT(currentAwareAccessToken)).thenReturn(currentUserId.toString());
    }

    @After
    public void afterEachTest(){
        resetMocksInApplicationContext(webApplicationContext);
    }
}