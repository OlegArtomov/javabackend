package com.aware.rest.controllers;

import com.aware.model.Meditation;
import com.aware.model.UserSubscription;
import com.aware.services.MeditationService;
import com.aware.services.billing.PaymentService;
import com.aware.services.notification.NotificationService;
import org.junit.Test;

import javax.annotation.Resource;

import static com.aware.model.SubscriptionType.FREE_TRIAL;
import static com.aware.model.SubscriptionType.REGULAR;
import static com.aware.rest.controllers.ApiUrls.API_V1_SUBSCRIBE_PAYMENT_URL;
import static com.aware.rest.controllers.ApiUrls.API_V1_TRIAL_PAYMENT_URL;
import static com.google.common.net.HttpHeaders.AUTHORIZATION;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Oleg Artomov
 */
public class PaymentControllerIT extends AbstractControllerIT {

    @Resource
    private PaymentService paymentService;

    @Resource
    private NotificationService notificationService;

    @Resource
    private MeditationService meditationService;

    @Test
    public void testApplySubscription() throws Exception {
        UserSubscription userSubscription = null;
        when(paymentService.provideSubscriptionForUser(eq(currentUser), isNull(String.class),  isNull(String.class), eq(REGULAR))).thenReturn(userSubscription);
        mockMvc.perform(post(API_V1_SUBSCRIBE_PAYMENT_URL)
                .header(AUTHORIZATION, currentAwareAccessToken))
                .andExpect(content().string(""))
                .andExpect(status().isOk());
        verify(paymentService).provideSubscriptionForUser(eq(currentUser), isNull(String.class), isNull(String.class), eq(REGULAR));
    }

    @Test
    public void testApplyFreeTrialWithoutAnswerToSlackChannel() throws Exception {
        Long userSubscriptionId = 1L;
        Long meditationId = 2L;
        UserSubscription userSubscription = new UserSubscription.Builder().id(userSubscriptionId).build();
        when(paymentService.provideSubscriptionForUser(eq(currentUser), isNull(String.class), isNull(String.class), eq(FREE_TRIAL))).thenReturn(userSubscription);
        Meditation meditation = new Meditation.Builder().id(meditationId).build();
        when(meditationService.find(eq(meditationId))).thenReturn(meditation);
        doNothing().when(notificationService).notifyTeachersByEmailAboutNewMeditation(eq(currentUser), eq(meditation),
                isNull(String.class), isNull(Long.class));
        doNothing().when(notificationService).notifyTeachersBySMSAboutNewMeditation(eq(currentUser)
        );
        mockMvc.perform(post(API_V1_TRIAL_PAYMENT_URL)
                .header(AUTHORIZATION, currentAwareAccessToken)
                .param("meditation_id", meditationId.toString())
        )
                .andExpect(jsonPath("$.id").value(meditationId.intValue()))
                .andExpect(jsonPath("$.completed").value(false))
                .andExpect(status().isOk());
        verify(paymentService).provideSubscriptionForUser(eq(currentUser), isNull(String.class), isNull(String.class), eq(FREE_TRIAL));
        verify(notificationService).notifyTeachersByEmailAboutNewMeditation(eq(currentUser), eq(meditation),
                isNull(String.class), isNull(Long.class));
        verify(notificationService).notifyTeachersBySMSAboutNewMeditation(eq(currentUser)
        );
        verify(meditationService).find(eq(meditationId));
    }

    @Test
    public void testApplyFreeTrialWithAnswerToSlackChannel() throws Exception {
        Long userSubscriptionId = 1L;
        Long meditationId = 2L;
        String slackChannelId = "AAA";
        Long slackSessionId = 1L;
        UserSubscription userSubscription = new UserSubscription.Builder().id(userSubscriptionId).build();
        when(paymentService.provideSubscriptionForUser(eq(currentUser), isNull(String.class), isNull(String.class), eq(FREE_TRIAL))).thenReturn(userSubscription);
        Meditation meditation = new Meditation.Builder().id(meditationId).build();
        when(meditationService.find(eq(meditationId))).thenReturn(meditation);
        doNothing().when(notificationService).notifyTeachersByEmailAboutNewMeditation(eq(currentUser), eq(meditation),
                eq(slackChannelId), eq(slackSessionId));
        doNothing().when(notificationService).notifyTeachersBySMSAboutNewMeditation(eq(currentUser)
        );
        mockMvc.perform(post(API_V1_TRIAL_PAYMENT_URL)
                .header(AUTHORIZATION, currentAwareAccessToken)
                .param("meditation_id", meditationId.toString())
                .param("slack_channel_id", slackChannelId)
                .param("slack_session_id", slackSessionId.toString())
        )
                .andExpect(jsonPath("$.id").value(meditationId.intValue()))
                .andExpect(jsonPath("$.completed").value(false))
                .andExpect(status().isOk());
        verify(paymentService).provideSubscriptionForUser(eq(currentUser), isNull(String.class), isNull(String.class), eq(FREE_TRIAL));
        verify(notificationService).notifyTeachersByEmailAboutNewMeditation(eq(currentUser), eq(meditation),  eq(slackChannelId), eq(slackSessionId));
        verify(notificationService).notifyTeachersBySMSAboutNewMeditation(eq(currentUser));
        verify(meditationService).find(eq(meditationId));
    }
}