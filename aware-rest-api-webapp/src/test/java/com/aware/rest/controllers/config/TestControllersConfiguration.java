package com.aware.rest.controllers.config;

import com.aware.repositories.AwareUserRepository;
import com.aware.rest.slack.client.SlackBotNotifier;
import com.aware.rest.system.ServerVersionManager;
import com.aware.services.MeditationService;
import com.aware.services.UserService;
import com.aware.services.billing.PaymentService;
import com.aware.services.billing.stripe.StripeService;
import com.aware.services.config.AwareConfiguration;
import com.aware.services.notification.NotificationService;
import com.aware.services.security.JwtService;
import com.aware.services.utils.UrlShortener;
import com.aware.spring.mock.MocksReplacer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;

import java.util.Collection;

import static com.aware.spring.BeanUtils.beanNamesForClasses;
import static com.aware.utils.AwareConstants.AWARE_BASE_PACKAGE;
import static com.google.common.collect.Lists.newArrayList;
import static org.mockito.Mockito.mock;

/**
 * @author Oleg Artomov
 */
@ComponentScan(
        basePackages = AWARE_BASE_PACKAGE,
        useDefaultFilters = false,
        includeFilters = {
                @ComponentScan.Filter({
                        Service.class
                })
        }
)
public class TestControllersConfiguration {

    @Bean
    public AwareUserRepository awareUserRepository() {
        return mock(AwareUserRepository.class);
    }

    @Bean
    public MocksReplacer mockitoBeanFactory() {
        Collection<Class> mocks = newArrayList(ServerVersionManager.class, MeditationService.class,
                AwareConfiguration.class, JwtService.class, UrlShortener.class, NotificationService.class,
                PaymentService.class, UserService.class,  StripeService.class, SlackBotNotifier.class);
        return new MocksReplacer(beanNamesForClasses(mocks));
    }
}
