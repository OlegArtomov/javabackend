package com.aware.rest.controllers;

import com.aware.rest.system.ServerVersionInfo;
import com.aware.rest.system.ServerVersionManager;
import com.aware.rest.system.Version;
import org.junit.Test;

import javax.annotation.Resource;

import static com.aware.rest.controllers.ApiUrls.HOME_URL;
import static com.aware.utils.DateTimeUtils.now;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

/**
 * @author Oleg Artomov
 */
public class HomeControllerIT extends AbstractControllerIT {

    @Resource
    private ServerVersionManager serverVersionManager;

    @Test
    public void testHomeUrl() throws Exception {
        ServerVersionInfo serverVersionInfo = new ServerVersionInfo.Builder()
                .version(new Version("2.6.11"))
                .buildHash("hash")
                .buildBranch("master")
                .buildDateTime(now().toString())
                .build();
        when(serverVersionManager.getServerVersionInfo()).thenReturn(serverVersionInfo);
        mockMvc.perform(get(HOME_URL)).andExpect(content().contentType(APPLICATION_JSON_UTF8));
        verify(serverVersionManager, only()).getServerVersionInfo();
    }
}
