package com.aware.rest.controllers;

import com.aware.model.Meditation;
import com.aware.rest.slack.client.SlackBotNotifier;
import com.aware.services.MeditationService;
import com.aware.services.config.AwareConfiguration;
import org.junit.Test;

import javax.annotation.Resource;

import static com.aware.rest.controllers.ApiUrls.API_V1_MEDITATION_ACCEPT_URL;
import static com.aware.rest.controllers.ApiUrls.API_V1_MEDITATION_COMPLETE_URL;
import static com.aware.rest.controllers.ApiUrls.API_V1_MEDITATION_CREATE_URL;
import static com.aware.rest.controllers.ApiUrls.API_V1_MEDITATION_FEEDBACK_URL;
import static com.aware.rest.controllers.ApiUrls.API_V1_MEDITATION_GET_URL;
import static com.google.common.net.HttpHeaders.AUTHORIZATION;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Oleg Artomov
 */
public class MeditationControllerIT extends AbstractControllerIT {

    @Resource
    private AwareConfiguration awareConfiguration;

    @Resource
    private MeditationService meditationService;

    @Resource
    private SlackBotNotifier slackBotNotifier;

    @Test
    public void testCreateMeditationWithDefaultDuration() throws Exception {
        Integer defaultDuration = 15;
        Meditation createdMeditation = new Meditation.Builder().build();
        when(awareConfiguration.getMeditationDefaultDuration()).thenReturn(defaultDuration);
        when(meditationService.createForUser(eq(currentUser), eq(defaultDuration))).thenReturn(createdMeditation);
        mockMvc.perform(put(API_V1_MEDITATION_CREATE_URL)
                .header(AUTHORIZATION, currentAwareAccessToken))
                .andExpect(content().contentType(APPLICATION_JSON_UTF8_VALUE)
                );
        verify(meditationService).createForUser(eq(currentUser), eq(defaultDuration));
    }

    @Test
    public void testCreateMeditationWithCustomDuration() throws Exception {
        Integer meditationDuration = 20;
        Meditation createdMeditation = new Meditation.Builder().build();
        when(awareConfiguration.getMeditationDefaultDuration()).thenReturn(meditationDuration);
        when(meditationService.createForUser(eq(currentUser), eq(meditationDuration))).thenReturn(createdMeditation);
        mockMvc.perform(put(API_V1_MEDITATION_CREATE_URL)
                .header(AUTHORIZATION, currentAwareAccessToken)
                .param("duration", meditationDuration.toString())
        )
                .andExpect(content().contentType(APPLICATION_JSON_UTF8_VALUE)
                );
        verify(meditationService).createForUser(eq(currentUser), eq(meditationDuration));
    }

    @Test
    public void testGetMeditation() throws Exception {
        Long meditationId = 20L;
        Meditation meditation = new Meditation.Builder().id(meditationId).build();
        when(meditationService.find(eq(meditationId))).thenReturn(meditation);
        mockMvc.perform(get(API_V1_MEDITATION_GET_URL, meditationId)
                .header(AUTHORIZATION, currentAwareAccessToken)
        )
                .andExpect(content().contentType(APPLICATION_JSON_UTF8_VALUE)
                );
        verify(meditationService).find(eq(meditationId));
    }

    @Test
    public void testAcceptByTeacherWithoutAnswerToSlack() throws Exception {
        Long meditationId = 20L;
        Meditation meditation = new Meditation.Builder().id(meditationId).build();
        when(meditationService.acceptMeditation(eq(meditationId), eq(currentUser))).thenReturn(meditation);
        mockMvc.perform(post(API_V1_MEDITATION_ACCEPT_URL, meditationId)
                .header(AUTHORIZATION, currentAwareAccessToken)
        )
                .andExpect(content().contentType(APPLICATION_JSON_UTF8_VALUE)
                );
        verify(meditationService).acceptMeditation(eq(meditationId), eq(currentUser));
    }

    @Test
    public void testAcceptByTeacherWithAnswerToSlack() throws Exception {
        Long meditationId = 20L;
        String channelId = "AA";
        String slackSuccessAnswer = "Hello";
        Long slackSessionId = 1L;
        Meditation meditation = new Meditation.Builder().id(meditationId).build();
        when(meditationService.acceptMeditation(eq(meditationId), eq(currentUser))).thenReturn(meditation);
        when(awareConfiguration.getSlackSuccessAcceptedAnswer()).thenReturn(slackSuccessAnswer);
        doNothing().when(slackBotNotifier).notifyBot(eq(slackSessionId), eq(channelId), anyString());
        mockMvc.perform(post(API_V1_MEDITATION_ACCEPT_URL, meditationId)
                .header(AUTHORIZATION, currentAwareAccessToken)
                .param("slack_channel_id", channelId)
                .param("slack_session_id", slackSessionId.toString())
        )
                .andExpect(content().contentType(APPLICATION_JSON_UTF8_VALUE)
                );
        verify(meditationService).acceptMeditation(eq(meditationId), eq(currentUser));
        verify(slackBotNotifier).notifyBot(eq(slackSessionId), eq(channelId), anyString());
    }


    @Test
    public void testCompleteMeditation() throws Exception {
        Long meditationId = 20L;
        Integer duration = 20;
        doNothing().when(meditationService).complete(eq(meditationId), eq(duration));
        mockMvc.perform(post(API_V1_MEDITATION_COMPLETE_URL, meditationId)
                .header(AUTHORIZATION, currentAwareAccessToken)
                .param("duration", duration.toString())
        )
                .andExpect(content().string(""))
                .andExpect(status().isOk());
        verify(meditationService).complete(eq(meditationId), eq(duration));
    }

    @Test
    public void testFeedbackFromUserWhenNoFeedbackText() throws Exception {
        Long meditationId = 20L;
        Byte score = 5;
        doNothing().when(meditationService).saveFeedback(eq(meditationId), eq(currentUser), eq(score), isNull(String.class));
        mockMvc.perform(post(API_V1_MEDITATION_FEEDBACK_URL, meditationId)
                .header(AUTHORIZATION, currentAwareAccessToken)
                .param("score", score.toString())
        )
                .andExpect(content().string(""))
                .andExpect(status().isOk());
        verify(meditationService).saveFeedback(eq(meditationId), eq(currentUser), eq(score), isNull(String.class));
    }

    @Test
    public void testFeedbackFromUserWhenPresentFeedbackText() throws Exception {
        Long meditationId = 20L;
        Byte score = 5;
        String feedbackText = "Great";
        doNothing().when(meditationService).saveFeedback(eq(meditationId), eq(currentUser), eq(score), eq(feedbackText));
        mockMvc.perform(post(API_V1_MEDITATION_FEEDBACK_URL, meditationId)
                .header(AUTHORIZATION, currentAwareAccessToken)
                .param("score", score.toString())
                .param("feedbackText", feedbackText)
        )
                .andExpect(content().string(""))
                .andExpect(status().isOk());
        verify(meditationService).saveFeedback(eq(meditationId), eq(currentUser), eq(score), eq(feedbackText));
    }
}