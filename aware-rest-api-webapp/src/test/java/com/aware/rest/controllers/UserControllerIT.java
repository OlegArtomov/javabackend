package com.aware.rest.controllers;

import com.aware.model.AwareUser;
import com.aware.model.FacebookUserInfo;
import com.aware.services.UserService;
import com.aware.services.config.AwareConfiguration;
import com.aware.services.notification.NotificationService;
import com.aware.services.security.JwtService;
import org.junit.Test;

import javax.annotation.Resource;

import static com.aware.rest.controllers.ApiUrls.API_V1_USER_ME;
import static com.aware.rest.controllers.ApiUrls.API_V1_USER_REGISTER;
import static com.aware.rest.controllers.ApiUrls.API_V1_USER_REGISTER_FB;
import static com.aware.rest.controllers.ApiUrls.API_V1_USER_UPDATE_URL;
import static com.google.common.net.HttpHeaders.AUTHORIZATION;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 * @author Oleg Artomov
 */
public class UserControllerIT extends AbstractControllerIT {

    @Resource
    private UserService userService;

    @Resource
    private JwtService jwtService;

    @Resource
    private AwareConfiguration awareConfiguration;

    @Resource
    private NotificationService notificationService;

    @Test
    public void testLoginUsingFacebook() throws Exception {
        String facebookToken = "AA";
        String awareToken = "121";
        AwareUser awareUser = new AwareUser.Builder().id(1L).build();
        FacebookUserInfo facebookInfo = new FacebookUserInfo.Builder().user(awareUser).build();
        when(userService.loginUsingFacebook(eq(facebookToken), eq(true))).thenReturn(facebookInfo);
        doReturn(awareToken).when(jwtService).createForUser(eq(awareUser));
        mockMvc.perform(
                post(API_V1_USER_REGISTER_FB)
                .param("facebookToken", facebookToken)
                .param("isTeacher", "1"))
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.access_token").value(awareToken)
                );
        verify(jwtService).createForUser(eq(awareUser));
        verify(userService).loginUsingFacebook(eq(facebookToken), eq(true));
    }

    @Test
    public void testRegisterByEmail() throws Exception {
        String phoneNumber = "+38063";
        String email = "test@ukr.net";
        String name = "test";
        String surname = "TEST";
        String awareToken = "121";
        String smsFormat = "ewe";
        AwareUser awareUser = new AwareUser.Builder().id(1L).email(email).build();
        when(userService.registerByEmail(eq(phoneNumber), eq(email), eq(name), eq(surname))).thenReturn(awareUser);
        doReturn(awareToken).when(jwtService).createForUser(eq(awareUser));
        when(awareConfiguration.getSMSProfileCreated()).thenReturn(smsFormat);
        doNothing().when(notificationService).sendSmsToNumber(anyString(), anyString(), anyString());
        doNothing().when(notificationService).sendWelcomeEmail(eq(awareUser));
        //TODO: Fix me need validation of all fields in response
        mockMvc.perform(
                post(API_V1_USER_REGISTER)
                .param("phoneNumber", phoneNumber)
                .param("email", email)
                .param("name", name)
                .param("surname", surname)
        )
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.access_token").value(awareToken)
                );
        verify(userService).registerByEmail(eq(phoneNumber), eq(email), eq(name), eq(surname));
        verify(jwtService).createForUser(eq(awareUser));
        verify(notificationService).sendWelcomeEmail(eq(awareUser));
        verify(notificationService).sendSmsToNumber(anyString(), anyString(), anyString());
    }

    @Test
    public void testUpdateInfo() throws Exception {
        String phoneNumber = "+38063";
        String email = "test@ukr.net";
        String name = "test";
        String surname = "TEST";
        when(userService.updateUserInfo(eq(currentUser), eq(phoneNumber), eq(email), eq(name), eq(surname))).thenReturn(currentUser);
        //TODO: Fix me need validation of all fields in response
        mockMvc.perform(
                post(API_V1_USER_UPDATE_URL)
                .param("phoneNumber", phoneNumber)
                .param("email", email)
                .param("name", name)
                .param("surname", surname)
                .header(AUTHORIZATION, currentAwareAccessToken)
        )
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.access_token").doesNotExist()
                );
        verify(userService).updateUserInfo(eq(currentUser), eq(phoneNumber), eq(email), eq(name), eq(surname));
    }

    @Test
    public void testMe() throws Exception {
        //TODO: Fix me need validation of all fields in response
        mockMvc.perform(
                get(API_V1_USER_ME)
                .header(AUTHORIZATION, currentAwareAccessToken))
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.access_token").doesNotExist()
                );
    }
}