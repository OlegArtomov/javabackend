package com.aware.rest.interceptors;

import com.aware.exceptions.AuthorizationFailureException;
import com.aware.services.security.JwtService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.rules.ExpectedException.none;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * @author Oleg Artomov
 */
@RunWith(MockitoJUnitRunner.class)
public class SecurityInterceptorTest {

    @InjectMocks
    private SecurityInterceptor securityInterceptor;

    @Mock
    private JwtService jwtService;

    @Rule
    public ExpectedException expectedException = none();

    @Test
    public void testExtractUserIdFromTokenWHenUserIdIsValid() {
        String userIdAsString = "1";
        when(jwtService.parseJWT(anyString())).thenReturn(userIdAsString);
        assertThat(securityInterceptor.extractUserIdFromToken(""), equalTo(1L));
    }

    @Test
    public void testExtractUserIdFromTokenWhenUserIdIsEmpty() {
        expectedException.expect(AuthorizationFailureException.class);
        String userIdAsString = "  ";
        when(jwtService.parseJWT(anyString())).thenReturn(userIdAsString);
        assertThat(securityInterceptor.extractUserIdFromToken(""), equalTo(1L));
    }

    @Test
    public void testExtractUserIdFromTokenWhenUserIdIsNull() {
        expectedException.expect(AuthorizationFailureException.class);
        String userIdAsString = null;
        when(jwtService.parseJWT(anyString())).thenReturn(userIdAsString);
        assertThat(securityInterceptor.extractUserIdFromToken(""), equalTo(1L));
    }

    @Test
    public void testExtractUserIdFromTokenWhenUserIdIsNullString() {
        expectedException.expect(AuthorizationFailureException.class);
        String userIdAsString = "null";
        when(jwtService.parseJWT(anyString())).thenReturn(userIdAsString);
        assertThat(securityInterceptor.extractUserIdFromToken(""), equalTo(1L));
    }

}