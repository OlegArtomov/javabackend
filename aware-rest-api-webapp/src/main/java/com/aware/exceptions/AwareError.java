package com.aware.exceptions;

/**
 * @author Oleg Artomov
 */
public enum AwareError {

    GENERAL_ERROR,
    AUTHORIZATION_FAILURE,
    REQUEST_VALIDATION_ERROR;

    public String getErrorCode() {
        return name();
    }
}
