package com.aware.exceptions;

/**
 * @author Oleg Artomov
 */
public class AuthorizationFailureException extends RuntimeException {
    private static final long serialVersionUID = 4523830493929834461L;

    public AuthorizationFailureException() {
        super();
    }

    public AuthorizationFailureException(final Throwable cause) {
        super(cause);
    }
}
