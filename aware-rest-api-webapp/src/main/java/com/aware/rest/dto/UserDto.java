package com.aware.rest.dto;

/**
 * @author Oleg Artomov
 */
public class UserDto {

    private String firstName;

    private String lastName;

    private String accessToken;

    private boolean isTeacher;

    private String facebookUserId;

    private String userPic;

    public boolean isTeacher() {
        return isTeacher;
    }

    public void setTeacher(boolean teacher) {
        isTeacher = teacher;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getFacebookUserId() {
        return facebookUserId;
    }

    public void setFacebookUserId(String facebookUserId) {
        this.facebookUserId = facebookUserId;
    }

    public void setUserPic(String userPic) {
        this.userPic = userPic;
    }

    public String getUserPic() {
        return userPic;
    }
}
