package com.aware.rest.dto;

/**
 * @author Oleg Artomov
 */
public class MeditationDto {
    private Long id;

    private Long studentId;

    private Long teacherId;

    private String startDate;

    private String studentName;

    private String teacherName;

    private Integer duration;

    private String openTokSessionId;

    private String openTokStudentToken;

    private String openTokTeacherToken;

    private boolean completed;

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setOpenTokSessionId(String openTokSessionId) {
        this.openTokSessionId = openTokSessionId;
    }

    public String getOpenTokSessionId() {
        return openTokSessionId;
    }

    public void setOpenTokStudentToken(String openTokStudentToken) {
        this.openTokStudentToken = openTokStudentToken;
    }

    public String getOpenTokStudentToken() {
        return openTokStudentToken;
    }

    public void setOpenTokTeacherToken(String openTokTeacherToken) {
        this.openTokTeacherToken = openTokTeacherToken;
    }

    public String getOpenTokTeacherToken() {
        return openTokTeacherToken;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public boolean isCompleted() {
        return completed;
    }
}
