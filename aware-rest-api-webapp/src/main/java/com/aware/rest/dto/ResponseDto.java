package com.aware.rest.dto;

import com.aware.exceptions.AwareError;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.annotation.Nullable;

import static com.google.common.base.MoreObjects.toStringHelper;
import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;

/**
 * @author Oleg Artomov
 */
public final class ResponseDto<T> {
    private final @Nullable  T data;

    private final @Nullable  String errorCode;

    private final @Nullable String message;

    // Note: please leave this constructor private and use names static methods for construction.
    // Reason: the named methods make it easier to search code for success/error responses, as well as for empty/nonempty responses.
    private ResponseDto(@Nullable final T data, final @Nullable String errorCode, final @Nullable String message) {
        this.data = data;
        this.errorCode = errorCode;
        this.message = message;
    }

    public boolean isSuccess() {
        return errorCode == null;
    }

    public @Nullable T getData() {
        return data;
    }

    public @Nullable String getErrorCode() {
        return errorCode;
    }

    public @Nullable String getMessage() {
        return message;
    }


    public static ResponseDto<Void> failure(final String errorCode, final String message) {
        return new ResponseDto<>(null, errorCode, message);
    }

    public static ResponseDto<Void> failure(final AwareError error, final String message) {
        return failure(error.getErrorCode(), message);
    }

    public static ResponseDto<Void> failure(final String errorCode, final Exception exception) {
        return failure(
                errorCode,
                defaultIfEmpty(exception.getMessage(), exception.getClass().getSimpleName())
        );
    }

    public static ResponseDto<Void> failure(final AwareError error, final Exception exception) {
        return failure(error.getErrorCode(), exception);
    }


    // Equality

    @Override
    public boolean equals(final Object another) {
        if (this == another) {
            return true;
        }
        if (!(another instanceof ResponseDto)) {
            return false;
        }

        final ResponseDto<?> other = (ResponseDto<?>) another;
        return new EqualsBuilder()
                .append(isSuccess(), other.isSuccess())
                .append(getData(), other.getData())
                .append(getErrorCode(), other.getErrorCode())
                .append(getMessage(), other.getMessage())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(isSuccess())
                .append(getData())
                .append(getErrorCode())
                .append(getMessage())
                .toHashCode();
    }


    // toString

    @Override
    public String toString() {
        return toStringHelper(this)
                .add("success", isSuccess())
                .add("data", getData())
                .add("errorCode", getErrorCode())
                .add("message", getMessage())
                .toString();
    }
}

