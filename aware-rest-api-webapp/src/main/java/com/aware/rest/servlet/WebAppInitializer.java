package com.aware.rest.servlet;

import com.aware.rest.config.RestAppConfiguration;
import com.aware.rest.config.RestMvcConfiguration;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import static com.aware.rest.system.RestApiConstants.LOG4J_CONFIG_REST_API;
import static com.aware.utils.AwareConstants.AWARE_CONFIG_DIR_SYSTEM_PROPERTY_NAME;
import static java.io.File.separator;
import static org.apache.log4j.xml.DOMConfigurator.configure;
import static org.springframework.beans.factory.config.PlaceholderConfigurerSupport.DEFAULT_PLACEHOLDER_PREFIX;
import static org.springframework.beans.factory.config.PlaceholderConfigurerSupport.DEFAULT_PLACEHOLDER_SUFFIX;
import static org.springframework.web.util.ServletContextPropertyUtils.resolvePlaceholders;

/**
 * @author Oleg Artomov
 */
public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[]{RestAppConfiguration.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[]{RestMvcConfiguration.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        String path = DEFAULT_PLACEHOLDER_PREFIX +
                AWARE_CONFIG_DIR_SYSTEM_PROPERTY_NAME + DEFAULT_PLACEHOLDER_SUFFIX + separator + LOG4J_CONFIG_REST_API;
        configure(resolvePlaceholders(path, servletContext));
        super.onStartup(servletContext);
    }
}
