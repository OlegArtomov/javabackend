package com.aware.rest.converters;

import com.aware.model.AwareUser;
import com.aware.model.Meditation;
import com.aware.rest.dto.MeditationDto;
import org.springframework.stereotype.Service;

import java.util.Date;

import static com.aware.utils.DateTimeUtils.dateToIso8601UtcString;

/**
 * @author Oleg Artomov
 */
@Service
public class MeditationConverter {

    public MeditationDto convert(Meditation meditation) {
        MeditationDto meditationDto = new MeditationDto();
        meditationDto.setId(meditation.getId());
        meditationDto.setStudentId(extractId(meditation.getStudent()));
        meditationDto.setTeacherId(extractId(meditation.getTeacher()));
        meditationDto.setStudentName(extractUserName(meditation.getStudent()));
        meditationDto.setTeacherName(extractUserName(meditation.getTeacher()));
        meditationDto.setDuration(meditation.getDuration());
        meditationDto.setOpenTokSessionId(meditation.getOpenTokSessionId());
        meditationDto.setOpenTokStudentToken(meditation.getOpenTokStudentToken());
        meditationDto.setOpenTokTeacherToken(meditation.getOpenTokTeacherToken());
        meditationDto.setCompleted(meditation.isCompleted());
        Date startDate = meditation.getStartDate();
        if (startDate != null) {
            meditationDto.setStartDate(dateToIso8601UtcString(startDate));
        }
        return meditationDto;
    }

    private Long extractId(AwareUser user) {
        return user != null ? user.getId() : null;
    }

    public static String extractUserName(AwareUser user) {
        return user != null ? user.getUserName() : null;
    }
}
