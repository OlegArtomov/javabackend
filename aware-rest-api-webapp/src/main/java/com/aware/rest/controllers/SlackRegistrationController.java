package com.aware.rest.controllers;

import com.aware.rest.slack.client.SlackBotNotifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import static com.aware.rest.controllers.ApiUrls.API_V1_BOT_REGISTER;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author Oleg Artomov
 */
@RestController
public class SlackRegistrationController {

    @Resource
    private SlackBotNotifier slackBotNotifier;

    @RequestMapping(value = API_V1_BOT_REGISTER, method = POST)
    public ResponseEntity register(@RequestParam String code) {
        slackBotNotifier.acceptClientCode(code);
        return new ResponseEntity<>(OK);
    }
}
