package com.aware.rest.controllers;

import com.aware.model.AwareUser;
import com.aware.model.FacebookUserInfo;
import com.aware.rest.dto.UserDto;
import com.aware.services.UserService;
import com.aware.services.config.AwareConfiguration;
import com.aware.services.notification.NotificationService;
import com.aware.services.security.JwtService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;

import static com.aware.rest.controllers.ApiUrls.API_V1_USER_ME;
import static com.aware.rest.controllers.ApiUrls.API_V1_USER_REGISTER;
import static com.aware.rest.controllers.ApiUrls.API_V1_USER_REGISTER_FB;
import static com.aware.rest.controllers.ApiUrls.API_V1_USER_UPDATE_URL;
import static com.aware.rest.converters.MeditationConverter.extractUserName;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author Oleg Artomov
 */

@RestController
public class UserController {

    @Resource
    private UserService userService;

    @Resource
    private JwtService jwtService;

    @Resource
    private NotificationService notificationService;

    @Resource
    private AwareConfiguration awareConfiguration;

    @RequestMapping(value = API_V1_USER_REGISTER_FB, produces = APPLICATION_JSON_UTF8_VALUE, method = POST)
    public ResponseEntity<UserDto> loginUsingFacebook(
            @RequestParam(name = "facebookToken") String facebookToken,
            @RequestParam(name = "isTeacher") boolean isTeacher) {
        FacebookUserInfo facebookUserInfo = userService.loginUsingFacebook(facebookToken, isTeacher);
        String appAccessToken = jwtService.createForUser(facebookUserInfo.getUser());
        return new ResponseEntity<>(convert(facebookUserInfo.getUser(), appAccessToken), OK);
    }

    @RequestMapping(value = API_V1_USER_REGISTER, produces = APPLICATION_JSON_UTF8_VALUE, method = POST)
    public ResponseEntity<UserDto> registerByEmail(
            @RequestParam(name = "phoneNumber") String phoneNumber,
            @RequestParam(name = "email") String email,
            @RequestParam(name = "name") String name,
            @RequestParam(name = "surname") String surname) {
        AwareUser awareUser = userService.registerByEmail(phoneNumber, email, name, surname);
        String appAccessToken = jwtService.createForUser(awareUser);
        String body = String.format(awareConfiguration.getSMSProfileCreated(), extractUserName(awareUser));
        notificationService.sendSmsToNumber(awareConfiguration.getTwilioBotNumber(), body, awareUser.getPhoneNumber());
        notificationService.sendWelcomeEmail(awareUser);
        return new ResponseEntity<>(convert(awareUser, appAccessToken), OK);
    }

    @RequestMapping(value = API_V1_USER_UPDATE_URL, produces = APPLICATION_JSON_UTF8_VALUE, method = POST)
    public ResponseEntity<UserDto> updateUserInfo(
            @ApiIgnore AwareUser currentUser,
            @RequestParam(name = "phoneNumber") String phoneNumber,
            @RequestParam(name = "email") String email,
            @RequestParam(name = "name") String name,
            @RequestParam(name = "surname") String surname
    ) {
        currentUser = userService.updateUserInfo(currentUser, phoneNumber, email, name, surname);
        return new ResponseEntity<>(convert(currentUser, null), OK);
    }

    @RequestMapping(value = API_V1_USER_ME, produces = APPLICATION_JSON_UTF8_VALUE, method = GET)
    public ResponseEntity<UserDto> me(@ApiIgnore AwareUser currentUser) {
        return new ResponseEntity<>(convert(currentUser, null), OK);
    }

    private UserDto convert(AwareUser awareUser, String appAccessToken) {
        FacebookUserInfo facebookUserInfo = awareUser.getFacebookUserInfo();
        UserDto result = new UserDto();
        result.setAccessToken(appAccessToken);
        result.setFirstName(awareUser.getFirstName());
        result.setLastName(awareUser.getLastName());
        result.setTeacher(awareUser.isTeacher());
        if (facebookUserInfo != null) {
            result.setFacebookUserId(facebookUserInfo.getFacebookUserId());
            result.setUserPic(facebookUserInfo.getUserPicUrl());
        }
        return result;
    }
}
