package com.aware.rest.controllers;

/**
 * @author Oleg Artomov
 */
public final class ApiUrls {

    private ApiUrls() {

    }

    public static final String API_V1_USER_UPDATE_URL = "/api/v1/user/update";

    public static final String API_V1_USER_REGISTER_FB = "/api/v1/user/register/fb";

    public static final String API_V1_USER_REGISTER = "/api/v1/user/register";

    public static final String API_V1_USER_ME = "/api/v1/user/me";

    public static final String API_V1_MEDITATION_GET_URL = "/api/v1/meditation/{id}";

    public static final String API_V1_MEDITATION_CREATE_URL = "/api/v1/meditation/create";

    public static final String API_V1_MEDITATION_ACCEPT_URL = "/api/v1/meditation/accept/{id}";

    public static final String API_V1_MEDITATION_COMPLETE_URL = "/api/v1/meditation/complete/{id}";

    public static final String API_V1_MEDITATION_FEEDBACK_URL = "/api/v1/meditation/feedback/{id}";

    public static final String API_V1_TRIAL_PAYMENT_URL = "/api/v1/payment/trial";

    public static final String API_V1_SUBSCRIBE_PAYMENT_URL = "/api/v1/payment/subscribe";

    public static final String API_V1_BOT_REGISTER = "/api/v1/bot/register";

    public static final String HOME_URL = "/";

    public static final String[] SWAGGER_DOCUMENTATION_URLS = {"/swagger**", "/v2/api-docs/**", "/configuration/**"};
}
