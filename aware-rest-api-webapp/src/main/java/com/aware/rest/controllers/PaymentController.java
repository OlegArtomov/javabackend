package com.aware.rest.controllers;

import com.aware.model.AwareUser;
import com.aware.model.Meditation;
import com.aware.model.UserSubscription;
import com.aware.rest.converters.MeditationConverter;
import com.aware.rest.dto.MeditationDto;
import com.aware.services.MeditationService;
import com.aware.services.billing.PaymentService;
import com.aware.services.notification.NotificationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;

import static com.aware.model.SubscriptionType.FREE_TRIAL;
import static com.aware.model.SubscriptionType.REGULAR;
import static com.aware.rest.controllers.ApiUrls.API_V1_SUBSCRIBE_PAYMENT_URL;
import static com.aware.rest.controllers.ApiUrls.API_V1_TRIAL_PAYMENT_URL;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author Oleg Artomov
 */
@RestController
public class PaymentController {

    @Resource
    private PaymentService paymentService;

    @Resource
    private NotificationService notificationService;

    @Resource
    private MeditationService meditationService;

    @Resource
    private MeditationConverter meditationConverter;


    @RequestMapping(value = API_V1_TRIAL_PAYMENT_URL, method = POST, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<MeditationDto> applyFreeTrial(@ApiIgnore AwareUser currentUser,
                                                        @RequestParam(value = "stripe_token", required = false) String stripeToken,
                                                        @RequestParam(value = "customer_invoice_name", required = false) String customerInvoiceName,
                                                        @RequestParam("meditation_id") Long meditationId,
                                                        @RequestParam(value = "slack_channel_id", required = false) String slackChannelId,
                                                        @RequestParam(value = "slack_session_id", required = false) Long slackSessionId) {
        UserSubscription userSubscription = paymentService.provideSubscriptionForUser(currentUser, stripeToken, customerInvoiceName, FREE_TRIAL);
        Meditation meditation = meditationService.find(meditationId);
        meditationService.assignFreeTrialSubscriptionForMeditation(meditation, userSubscription);
        notificationService.notifyTeachersByEmailAboutNewMeditation(currentUser, meditation, slackChannelId, slackSessionId);
        notificationService.notifyTeachersBySMSAboutNewMeditation(currentUser);
        return new ResponseEntity<>(meditationConverter.convert(meditation), OK);
    }

    @RequestMapping(value = API_V1_SUBSCRIBE_PAYMENT_URL, method = POST, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity applySubscription(@ApiIgnore AwareUser currentUser,
                                            @RequestParam(value = "stripe_token", required = false) String stripeToken,
                                            @RequestParam(value = "customer_invoice_name", required = false) String customerInvoiceName) {
        paymentService.provideSubscriptionForUser(currentUser, stripeToken, customerInvoiceName, REGULAR);
        return new ResponseEntity<>(OK);
    }
}
