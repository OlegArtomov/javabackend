package com.aware.rest.controllers;

import com.aware.rest.system.ServerVersionInfo;
import com.aware.rest.system.ServerVersionManager;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import static com.aware.rest.controllers.ApiUrls.HOME_URL;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * @author Oleg Artomov
 */
@RestController
public class HomeController {

    @Resource
    private ServerVersionManager serverVersionManager;

    @RequestMapping(path = HOME_URL, produces = APPLICATION_JSON_UTF8_VALUE, method = GET)
    public ServerVersionInfo indexPage(){
        return serverVersionManager.getServerVersionInfo();
   }
}
