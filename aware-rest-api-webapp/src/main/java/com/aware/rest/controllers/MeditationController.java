package com.aware.rest.controllers;

import com.aware.model.AwareUser;
import com.aware.model.Meditation;
import com.aware.rest.converters.MeditationConverter;
import com.aware.rest.dto.MeditationDto;
import com.aware.rest.slack.client.SlackBotNotifier;
import com.aware.services.MeditationService;
import com.aware.services.config.AwareConfiguration;
import com.aware.services.notification.NotificationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;

import static com.aware.rest.controllers.ApiUrls.API_V1_MEDITATION_ACCEPT_URL;
import static com.aware.rest.controllers.ApiUrls.API_V1_MEDITATION_COMPLETE_URL;
import static com.aware.rest.controllers.ApiUrls.API_V1_MEDITATION_CREATE_URL;
import static com.aware.rest.controllers.ApiUrls.API_V1_MEDITATION_FEEDBACK_URL;
import static com.aware.rest.controllers.ApiUrls.API_V1_MEDITATION_GET_URL;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * @author Oleg Artomov
 */
@RestController
public class MeditationController {

    @Resource
    private MeditationService meditationService;

    @Resource
    private MeditationConverter meditationConverter;

    @Resource
    private AwareConfiguration awareConfiguration;

    @Resource
    private NotificationService notificationService;

    @Resource
    private SlackBotNotifier slackBotNotifier;

    @RequestMapping(value = API_V1_MEDITATION_CREATE_URL, method = PUT, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<MeditationDto> create(@ApiIgnore  AwareUser currentUser, @RequestParam(required = false)
    Integer duration) {
        Integer meditationDuration = duration != null ? duration : awareConfiguration.getMeditationDefaultDuration();
        Meditation meditation = meditationService.createForUser(currentUser, meditationDuration);
        return new ResponseEntity<>(meditationConverter.convert(meditation), CREATED);
    }

    @RequestMapping(value = API_V1_MEDITATION_GET_URL, method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<MeditationDto> get(@PathVariable Long id) {
        Meditation meditation = meditationService.find(id);
        return new ResponseEntity<>(meditationConverter.convert(meditation), OK);
    }

    @RequestMapping(value = API_V1_MEDITATION_ACCEPT_URL, method = POST, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<MeditationDto> acceptByTeacher(@PathVariable Long id,
                                                         @ApiIgnore AwareUser currentUser,
                                                         @RequestParam(value = "slack_channel_id", required = false) String slackChannelId,
                                                         @RequestParam(value = "slack_session_id", required = false) Long slackSessionId) {
        Meditation meditation = meditationService.acceptMeditation(id, currentUser);
        if (isNotEmpty(slackChannelId) && (slackSessionId != null)) {
            slackBotNotifier.notifyBot(slackSessionId, slackChannelId, awareConfiguration.getSlackSuccessAcceptedAnswer());
        }
        return new ResponseEntity<>(meditationConverter.convert(meditation), OK);
    }

    @RequestMapping(value = API_V1_MEDITATION_COMPLETE_URL, method = POST, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Void> complete(@PathVariable Long id,
                                         @RequestParam Integer duration) {
        meditationService.complete(id, duration);
        return new ResponseEntity<>(OK);
    }

    @RequestMapping(value = API_V1_MEDITATION_FEEDBACK_URL, method = POST, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Void> feedbackFromUser(@PathVariable Long id,
                                                 @RequestParam Byte score,
                                                 @RequestParam(required = false) String feedbackText,
                                                 @ApiIgnore AwareUser currentUser) {
        meditationService.saveFeedback(id, currentUser, score, feedbackText);
        notificationService.sendEmailAboutFeedback(currentUser, feedbackText, score);
        return new ResponseEntity<>(OK);
    }

}
