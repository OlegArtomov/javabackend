package com.aware.rest.system;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Version implements Comparable<Version> {

    private static final char VERSION_COMPONENTS_SEPARATOR = '.';

    private static final Splitter versionComponentsSplitter = Splitter.on(VERSION_COMPONENTS_SEPARATOR);


    private final List<String> versionComponents;


    public Version(final String versionString) {
        versionComponents = versionComponentsSplitter.splitToList(versionString);
    }


    public List<String> getVersionComponents() {
        return versionComponents;
    }


    @Override
    public boolean equals(final Object another) {
        if (this == another) {
            return true;
        }
        if (!(another instanceof Version)) {
            return false;
        }

        final Version other = (Version)another;
        return new EqualsBuilder()
            .append(getVersionComponents(), other.getVersionComponents())
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
            .append(getVersionComponents())
            .toHashCode();
    }

    @Override
    public String toString() {
        return Joiner.on(VERSION_COMPONENTS_SEPARATOR).join(getVersionComponents());
    }


    /**
     * Note that the ordering is NOT consistent with {@link #equals(Object)}!
     */
    @Override
    public int compareTo(final Version other) {
        final Iterator<String> thisVersionComponentsIter = getVersionComponents().iterator();
        final Iterator<String> otherVersionComponentsIter = other.getVersionComponents().iterator();

        while (thisVersionComponentsIter.hasNext() && otherVersionComponentsIter.hasNext()) {
            final int componentsCompareResult = compareVersionComponents(thisVersionComponentsIter.next(), otherVersionComponentsIter.next());
            if (componentsCompareResult != 0) {
                return componentsCompareResult;
            }
        }

        if (nonZeroVersionComponentExists(thisVersionComponentsIter)) {
            return 1;
        }
        if (nonZeroVersionComponentExists(otherVersionComponentsIter)) {
            return -1;
        }
        return 0;
    }

    private static int compareVersionComponents(final String thisComponent, final String otherComponent) {
        final Matcher thisComponentMatcher = versionComponentParseRegex.matcher(thisComponent);
        final Matcher otherComponentMatcher = versionComponentParseRegex.matcher(otherComponent);
        thisComponentMatcher.matches();
        otherComponentMatcher.matches();

        final String thisNumberStr = thisComponentMatcher.group(1);
        final String otherNumberStr = otherComponentMatcher.group(1);
        final BigInteger thisNumber = thisNumberStr.isEmpty() ? BigInteger.ZERO : new BigInteger(thisNumberStr);
        final BigInteger otherNumber = otherNumberStr.isEmpty() ? BigInteger.ZERO : new BigInteger(otherNumberStr);
        final int numbersCompareResult = thisNumber.compareTo(otherNumber);
        if (numbersCompareResult != 0) {
            return numbersCompareResult;
        }

        final String thisSuffix = thisComponentMatcher.group(2);
        final String otherSuffix = otherComponentMatcher.group(2);
        if (thisSuffix.isEmpty()) {
            return otherSuffix.isEmpty() ? 0 : 1;
        }
        if (otherSuffix.isEmpty()) {
            return -1;
        }
        return thisSuffix.compareTo(otherSuffix);
    }
    private static final Pattern versionComponentParseRegex = Pattern.compile("\\A0*(\\d*)(.*)\\z", Pattern.DOTALL);

    private static boolean nonZeroVersionComponentExists(final Iterator<String> versionComponentsIter) {
        while (versionComponentsIter.hasNext()) {
            if (!StringUtils.containsOnly(versionComponentsIter.next(), '0')) {
                return true;
            }
        }
        return false;
    }
}
