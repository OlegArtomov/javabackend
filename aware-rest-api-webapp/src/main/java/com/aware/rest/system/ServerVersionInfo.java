package com.aware.rest.system;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import static com.google.common.base.MoreObjects.toStringHelper;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.springframework.util.Assert.isTrue;
import static org.springframework.util.Assert.notNull;

public final class ServerVersionInfo {

    private final Version version;
    private final String buildHash;
    private final String buildDateTime;
    private final String buildBranch;

    public Version getVersion() {
        return version;
    }

    public String getBuildHash() {
        return buildHash;
    }

    public String getBuildDateTime() {
        return buildDateTime;
    }

    public String getBuildBranch() {
        return buildBranch;
    }


    @Override
    public String toString() {
        return toStringHelper(this)
                .add("version", getVersion())
                .add("buildHash", getBuildHash())
                .add("buildDateTime", getBuildDateTime())
                .add("buildBranch", getBuildBranch())
                .toString();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final ServerVersionInfo theObj = (ServerVersionInfo) obj;
        return new EqualsBuilder()
                .append(getVersion(), theObj.getVersion())
                .append(getBuildHash(), theObj.getBuildHash())
                .append(getBuildDateTime(), theObj.getBuildDateTime())
                .append(getBuildBranch(), theObj.getBuildBranch())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(getVersion())
                .append(getBuildHash())
                .append(getBuildDateTime())
                .append(getBuildBranch())
                .toHashCode();
    }

    public static class Builder {
        private Version version;
        private String buildHash;
        private String buildDateTime;
        private String buildBranch;


        public Builder version(final Version version) {
            this.version = version;
            return this;
        }

        public Builder buildHash(final String buildHash) {
            this.buildHash = buildHash;
            return this;
        }

        public Builder buildDateTime(final String buildDateTime) {
            this.buildDateTime = buildDateTime;
            return this;
        }

        public Builder buildBranch(final String buildBranch) {
            this.buildBranch = buildBranch;
            return this;
        }


        public ServerVersionInfo build() {
            return new ServerVersionInfo(this);
        }
    }
    private ServerVersionInfo(final Builder builder) {
        version = builder.version;
        buildHash = builder.buildHash;
        buildDateTime = builder.buildDateTime;
        buildBranch = builder.buildBranch;

        notNull(version);
        isTrue(isNotEmpty(buildHash));
        notNull(buildDateTime);
        isTrue(isNotEmpty(buildBranch));
    }
}
