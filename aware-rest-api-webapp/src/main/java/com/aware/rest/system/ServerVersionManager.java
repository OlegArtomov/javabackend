package com.aware.rest.system;

import com.aware.spring.resourceloaders.CombinedResourceLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Properties;

import static com.aware.utils.AwareConstants.SERVER_VERSION_INFO_PROPERTIES_FILE;
import static com.aware.utils.PropertiesUtils.getProperties;

@Service
public class ServerVersionManager {

    private final ServerVersionInfo serverVersionInfo;

    @Autowired
    public ServerVersionManager(CombinedResourceLoader combinedResourceLoader) throws IOException {
        final Properties serverVersionInfoProperties = getProperties(combinedResourceLoader, true, SERVER_VERSION_INFO_PROPERTIES_FILE);
        serverVersionInfo = new ServerVersionInfo.Builder()
                .version(new Version(serverVersionInfoProperties.getProperty("version")))
                .buildHash(serverVersionInfoProperties.getProperty("buildHash"))
                .buildDateTime(serverVersionInfoProperties.getProperty("buildDateTime"))
                .buildBranch(serverVersionInfoProperties.getProperty("buildBranch"))
                .build();
    }

    public ServerVersionInfo getServerVersionInfo() {
        return serverVersionInfo;
    }
}
