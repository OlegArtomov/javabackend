package com.aware.rest.system;

/**
 * @author Oleg Artomov
 */
public final class RestApiConstants {

    public static final String LOG4J_CONFIG_REST_API = "log4j.xml";

    private RestApiConstants(){

   }
}
