package com.aware.rest.exceptionhandling;

import com.aware.exceptions.AuthorizationFailureException;
import com.aware.exceptions.AwareRuntimeException;
import com.aware.rest.dto.ResponseDto;
import org.slf4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;

import static com.aware.exceptions.AwareError.AUTHORIZATION_FAILURE;
import static com.aware.exceptions.AwareError.GENERAL_ERROR;
import static com.aware.exceptions.AwareError.REQUEST_VALIDATION_ERROR;
import static com.aware.rest.dto.ResponseDto.failure;
import static com.aware.rest.interceptors.SecurityInterceptor.REQUEST_HANDLER_ATTRIBUTE_NAME;
import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.web.servlet.HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE;

/**
 * @author Oleg Artomov
 */
@ControllerAdvice
public class GlobalExceptionHandlerControllerAdvise extends ResponseEntityExceptionHandler {

    private static final Logger logger = getLogger(GlobalExceptionHandlerControllerAdvise.class);


    @ExceptionHandler({Exception.class})
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResponseDto<Void> handleException(final Exception exception, final HttpServletRequest request) {
        logger.error("Critical application error. Request handler: {}", getRequestHandlerAsString(request), exception);
        return failure(GENERAL_ERROR, exception);
    }


    @ExceptionHandler({AwareRuntimeException.class})
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResponseDto<Void> handleAwareRuntimeException(final AwareRuntimeException exception,
                                                         final HttpServletRequest request) {
        logger.error("Business application error. Request handler: {}", getRequestHandlerAsString(request), exception);
        return failure(exception.getAwareErrorCode().name(), exception);
    }

    @ExceptionHandler({AuthorizationFailureException.class})
    @ResponseStatus(UNAUTHORIZED)
    @ResponseBody
    public ResponseDto<Void> handleAuthException(final AuthorizationFailureException exception, final HttpServletRequest request) {
        logger.info("Auth failed. Request handler: {}", getRequestHandlerAsString(request),  exception);
        return failure(AUTHORIZATION_FAILURE, exception);
    }

    private String getRequestHandlerAsString(final HttpServletRequest request) {
        final @Nullable Object requestHandler = request.getAttribute(REQUEST_HANDLER_ATTRIBUTE_NAME);
        if (requestHandler != null) {
            return requestHandler.toString();
        }
        return defaultIfEmpty((String) request.getAttribute(PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE), "No handler");
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body,
                                                             HttpHeaders headers, HttpStatus status, WebRequest request) {
        headers.setContentType(APPLICATION_JSON_UTF8);
        String requestHandler = null;
        if (request instanceof NativeWebRequest){
            NativeWebRequest nativeWebRequest = (NativeWebRequest)request;
            requestHandler = getRequestHandlerAsString(nativeWebRequest.getNativeRequest(HttpServletRequest.class));
        }
        logger.error("Native spring exception. Request handler: {}", requestHandler, ex);
        return super.handleExceptionInternal(ex, failure(REQUEST_VALIDATION_ERROR, ex), headers, status, request);
    }
}
