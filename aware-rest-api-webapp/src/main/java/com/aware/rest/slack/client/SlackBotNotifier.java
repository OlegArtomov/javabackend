package com.aware.rest.slack.client;

import com.aware.slackbot.config.RedisConfiguration;
import com.aware.slackbot.format.RegisterBotRequest;
import com.aware.slackbot.format.SendToSlackRequest;
import org.slf4j.Logger;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author Oleg Artomov
 */
@Service
public class SlackBotNotifier {

    private static final Logger logger = getLogger(SlackBotNotifier.class);

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private RedisConfiguration redisConfiguration;

    public void notifyBot(Long sessionId, String channelId, String message) {
        if (isNotEmpty(message)) {
            logger.debug("Notify bot: slack channel {}", channelId);
            SendToSlackRequest sendToSlackRequest = new SendToSlackRequest.Builder()
                    .channelId(channelId)
                    .message(message)
                    .sessionId(sessionId)
                    .build();
            stringRedisTemplate.convertAndSend(redisConfiguration.getRedisSlackChannel(), sendToSlackRequest);
        } else {
            logger.warn("Skip empty message for channel: {}", channelId);
        }
    }

    public void acceptClientCode(String code) {
        if (isNotEmpty(code)) {
            logger.debug("Accept client code: {}", code);
            RegisterBotRequest registerBotRequest = new RegisterBotRequest.Builder().code(code).build();
            stringRedisTemplate.convertAndSend(redisConfiguration.getRedisRegisterBotChannel(), registerBotRequest);
        } else {
            logger.warn("Skip empty code");
        }
    }
}
