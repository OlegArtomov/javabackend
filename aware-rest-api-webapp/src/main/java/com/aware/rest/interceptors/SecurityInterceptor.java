package com.aware.rest.interceptors;

import com.aware.exceptions.AuthorizationFailureException;
import com.aware.model.AwareUser;
import com.aware.repositories.AwareUserRepository;
import com.aware.services.security.JwtService;
import io.jsonwebtoken.JwtException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.google.common.net.HttpHeaders.AUTHORIZATION;
import static org.springframework.util.StringUtils.isEmpty;
import static org.springframework.web.cors.CorsUtils.isPreFlightRequest;

/**
 * @author Oleg Artomov
 */
public class SecurityInterceptor extends HandlerInterceptorAdapter {

    public static final String CURRENT_USER_ATTRIBUTE_NAME = "CURRENT_USER";

    public static final String REQUEST_HANDLER_ATTRIBUTE_NAME = "requestHandler";

    @Resource
    private JwtService jwtService;

    @Resource
    private AwareUserRepository awareUserRepository;

    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler) {
        request.setAttribute(REQUEST_HANDLER_ATTRIBUTE_NAME, handler);

        //This is for CORS requests
        if (isPreFlightRequest(request)) {
            return true;
        }
        String token = request.getHeader(AUTHORIZATION);
        if (isEmpty(token)) {
            throw new AuthorizationFailureException();
        }
        //TODO: Very discussable moment
        AwareUser awareUser = awareUserRepository.findOne(extractUserIdFromToken(token));
        if (awareUser == null) {
            throw new AuthorizationFailureException();
        }
        request.setAttribute(CURRENT_USER_ATTRIBUTE_NAME, awareUser);
        return true;
    }

    protected Long extractUserIdFromToken(String token) {
        try {
            return Long.valueOf(jwtService.parseJWT(token));
        } catch (NumberFormatException | JwtException e) {
            throw new AuthorizationFailureException(e);
        }
    }
}
