package com.aware.rest.config;

import com.aware.config.AwareDaoConfiguration;
import com.aware.config.AwareProperties;
import com.aware.services.config.AwareServicesConfiguration;
import com.aware.slackbot.config.SlackBotSharedConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.stereotype.Service;

import static com.aware.utils.AwareConstants.AWARE_BASE_PACKAGE;

/**
 * @author Oleg Artomov
 */

@Configuration
@ComponentScan(
        basePackages = AWARE_BASE_PACKAGE,
        useDefaultFilters = false,
        includeFilters = {
                @ComponentScan.Filter({
                        Service.class
                })
        }
)
@Import({AwareServicesConfiguration.class, AwareDaoConfiguration.class,
        SlackBotSharedConfig.class, AwareProperties.class})
//TODO: Important: AwareProperties must be last
public class RestAppConfiguration {

    @Bean
    public StringRedisTemplate stringRedisTemplate(RedisConnectionFactory redisConnectionFactory, ObjectMapper redisRequestObjectMapper) {
        StringRedisTemplate template = new StringRedisTemplate(redisConnectionFactory);
        GenericJackson2JsonRedisSerializer serializer = new GenericJackson2JsonRedisSerializer(redisRequestObjectMapper);
        template.setValueSerializer(serializer);
        return template;
    }
}
