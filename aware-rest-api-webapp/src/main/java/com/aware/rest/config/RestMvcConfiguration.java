package com.aware.rest.config;

import com.aware.rest.argumentresolvers.CurrentUserArgumentResolver;
import com.aware.rest.interceptors.SecurityInterceptor;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.Resource;
import java.util.List;

import static com.aware.rest.controllers.ApiUrls.API_V1_BOT_REGISTER;
import static com.aware.rest.controllers.ApiUrls.API_V1_USER_REGISTER;
import static com.aware.rest.controllers.ApiUrls.API_V1_USER_REGISTER_FB;
import static com.aware.rest.controllers.ApiUrls.HOME_URL;
import static com.aware.rest.controllers.ApiUrls.SWAGGER_DOCUMENTATION_URLS;
import static com.aware.utils.AwareConstants.AWARE_BASE_PACKAGE;
import static com.fasterxml.jackson.databind.DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT;
import static com.fasterxml.jackson.databind.DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_INVALID_SUBTYPE;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_NUMBERS_FOR_ENUMS;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_READING_DUP_TREE_KEY;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL;
import static com.fasterxml.jackson.databind.DeserializationFeature.UNWRAP_ROOT_VALUE;
import static com.fasterxml.jackson.databind.DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS;
import static com.fasterxml.jackson.databind.DeserializationFeature.WRAP_EXCEPTIONS;
import static com.fasterxml.jackson.databind.PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.HEAD;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;

/**
 * @author Oleg Artomov
 */
@Configuration
@EnableWebMvc
@ComponentScan(
        basePackages = AWARE_BASE_PACKAGE,
        useDefaultFilters = false,
        includeFilters = @ComponentScan.Filter({
                Controller.class, ControllerAdvice.class
        })
)
@EnableSwagger2
public class RestMvcConfiguration extends WebMvcConfigurerAdapter {

    @Resource
    private ObjectMapper objectMapper;

    @Resource
    private SecurityInterceptor securityInterceptor;

    @Override
    public void configureMessageConverters(final List<HttpMessageConverter<?>> converters) {
        converters.add(new MappingJackson2HttpMessageConverter(objectMapper));
    }

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.favorPathExtension(false);
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new CurrentUserArgumentResolver());
    }

    @Override
    public void addInterceptors(final InterceptorRegistry registry) {
        registry
                .addInterceptor(securityInterceptor)
                .excludePathPatterns(HOME_URL, API_V1_USER_REGISTER_FB, API_V1_USER_REGISTER, API_V1_BOT_REGISTER)
                .excludePathPatterns(SWAGGER_DOCUMENTATION_URLS);
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedMethods(GET.name(), HEAD.name(), POST.name(), PUT.name());
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //OAR, Start Swagger handlers
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
        //OAR, end swagger handles
    }

    @Bean
    public FactoryBean<ObjectMapper> jsonObjectMapperFactory() {
        final Jackson2ObjectMapperFactoryBean jsonObjectMapperFactory = new Jackson2ObjectMapperFactoryBean();
        jsonObjectMapperFactory.setFeaturesToDisable(
                FAIL_ON_UNKNOWN_PROPERTIES,
                ACCEPT_SINGLE_VALUE_AS_ARRAY,
                UNWRAP_SINGLE_VALUE_ARRAYS,
                UNWRAP_ROOT_VALUE,
                ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,
                READ_UNKNOWN_ENUM_VALUES_AS_NULL
        );
        jsonObjectMapperFactory.setFeaturesToEnable(
                FAIL_ON_NULL_FOR_PRIMITIVES,
                FAIL_ON_NUMBERS_FOR_ENUMS,
                FAIL_ON_INVALID_SUBTYPE,
                FAIL_ON_READING_DUP_TREE_KEY,
                FAIL_ON_IGNORED_PROPERTIES,
                WRAP_EXCEPTIONS
        );
        jsonObjectMapperFactory.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        jsonObjectMapperFactory.setPropertyNamingStrategy(CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
        return jsonObjectMapperFactory;
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyResourceConfigurer() {
        // This enables resolution of `${...}` placeholders within bean definition property values and @Value annotations
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public SecurityInterceptor securityInterceptor() {
        return new SecurityInterceptor();
    }
}
