package com.aware.slack.listeners;

import com.aware.slack.events.SlackChannelJoined;

/**
 * @author Oleg Artomov
 */
public interface SlackChannelJoinedListener extends SlackEventListener<SlackChannelJoined>{
}
