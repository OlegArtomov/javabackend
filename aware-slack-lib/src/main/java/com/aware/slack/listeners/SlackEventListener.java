package com.aware.slack.listeners;

import com.aware.slack.SlackSession;
import com.aware.slack.events.SlackEvent;

public interface SlackEventListener<T extends SlackEvent>
{
    void onEvent(T event, SlackSession session);
}
