package com.aware.slack.listeners;

import com.aware.slack.events.SlackChannelCreated;

public interface SlackChannelCreatedListener extends SlackEventListener<SlackChannelCreated>
{
}
