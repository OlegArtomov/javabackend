
package com.aware.slack.listeners;

import com.aware.slack.events.PinAdded;


public interface PinAddedListener extends com.aware.slack.listeners.SlackEventListener<PinAdded> {
    
}
