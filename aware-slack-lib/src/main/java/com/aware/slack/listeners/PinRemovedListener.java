
package com.aware.slack.listeners;

import com.aware.slack.events.PinRemoved;


public interface PinRemovedListener extends SlackEventListener<PinRemoved>{
    
}
