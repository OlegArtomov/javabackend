package com.aware.slack.listeners;

import com.aware.slack.events.SlackGroupJoined;

public interface SlackGroupJoinedListener extends SlackEventListener<SlackGroupJoined>
{
}
