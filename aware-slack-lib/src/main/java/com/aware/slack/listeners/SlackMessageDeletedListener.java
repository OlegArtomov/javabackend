package com.aware.slack.listeners;

import com.aware.slack.events.SlackMessageDeleted;

public interface SlackMessageDeletedListener extends SlackEventListener<SlackMessageDeleted>
{
}
