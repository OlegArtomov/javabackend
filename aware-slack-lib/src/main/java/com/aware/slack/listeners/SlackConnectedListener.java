package com.aware.slack.listeners;

import com.aware.slack.events.SlackConnected;

public interface SlackConnectedListener extends SlackEventListener<SlackConnected>
{
}
