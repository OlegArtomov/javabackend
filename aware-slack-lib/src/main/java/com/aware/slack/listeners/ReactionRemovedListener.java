
package com.aware.slack.listeners;

import com.aware.slack.events.ReactionRemoved;


public interface ReactionRemovedListener extends SlackEventListener<ReactionRemoved>{
    
}
