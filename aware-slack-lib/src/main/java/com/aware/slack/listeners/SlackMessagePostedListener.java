package com.aware.slack.listeners;

import com.aware.slack.events.SlackMessagePosted;

public interface SlackMessagePostedListener extends SlackEventListener<SlackMessagePosted>
{
}
