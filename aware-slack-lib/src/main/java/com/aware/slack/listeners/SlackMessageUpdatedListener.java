package com.aware.slack.listeners;

import com.aware.slack.events.SlackMessageUpdated;

public interface SlackMessageUpdatedListener extends SlackEventListener<SlackMessageUpdated>
{
}
