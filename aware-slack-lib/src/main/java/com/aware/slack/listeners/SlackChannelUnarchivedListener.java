package com.aware.slack.listeners;

import com.aware.slack.events.SlackChannelUnarchived;

public interface SlackChannelUnarchivedListener extends SlackEventListener<SlackChannelUnarchived>
{
}
