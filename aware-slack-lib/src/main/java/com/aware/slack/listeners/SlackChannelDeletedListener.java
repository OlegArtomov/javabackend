package com.aware.slack.listeners;

import com.aware.slack.events.SlackChannelDeleted;

public interface SlackChannelDeletedListener extends SlackEventListener<SlackChannelDeleted>
{
}
