
package com.aware.slack.listeners;

import com.aware.slack.events.ReactionAdded;


public interface ReactionAddedListener extends SlackEventListener<ReactionAdded>{
    
}
