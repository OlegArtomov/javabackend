package com.aware.slack.listeners;

import com.aware.slack.events.SlackUserChange;

public interface SlackUserChangeListener extends SlackEventListener<SlackUserChange> {
}
