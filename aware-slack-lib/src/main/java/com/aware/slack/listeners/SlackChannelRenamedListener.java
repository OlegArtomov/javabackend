package com.aware.slack.listeners;

import com.aware.slack.events.SlackChannelRenamed;

public interface SlackChannelRenamedListener extends SlackEventListener<SlackChannelRenamed>
{
}
