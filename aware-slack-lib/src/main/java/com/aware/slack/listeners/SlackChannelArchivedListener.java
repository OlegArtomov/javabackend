package com.aware.slack.listeners;

import com.aware.slack.events.SlackChannelArchived;

public interface SlackChannelArchivedListener extends SlackEventListener<SlackChannelArchived>
{
}
