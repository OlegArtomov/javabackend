package com.aware.slack.events;

import com.aware.slack.SlackUser;

public interface SlackChannelUnarchived extends SlackChannelEvent
{
    SlackUser getUser();
}
