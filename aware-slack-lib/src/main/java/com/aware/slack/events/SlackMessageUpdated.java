package com.aware.slack.events;

import com.aware.slack.SlackChannel;

public interface SlackMessageUpdated extends SlackMessageEvent
{
    SlackChannel getChannel();
    String getMessageTimestamp();
    String getNewMessage();
}
