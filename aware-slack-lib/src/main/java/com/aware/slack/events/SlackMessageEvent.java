package com.aware.slack.events;

public interface SlackMessageEvent extends SlackEvent
{
    String getTimeStamp();
}
