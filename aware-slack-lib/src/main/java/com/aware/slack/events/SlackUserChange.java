package com.aware.slack.events;

import com.aware.slack.SlackUser;

public interface SlackUserChange extends SlackEvent {

    SlackUser getUser();
}
