package com.aware.slack.events;

import com.aware.slack.SlackChannel;
import com.aware.slack.SlackUser;


public interface ReactionAdded extends com.aware.slack.events.SlackEvent {
    
    String getEmojiName();
    SlackChannel getChannel();
    SlackUser getUser();
    String getMessageID();
    String getFileID();
    String getFileCommentID();

}
