package com.aware.slack.events;

import com.aware.slack.SlackChannel;

public interface SlackMessageDeleted extends SlackMessageEvent
{
    SlackChannel getChannel();
    String getMessageTimestamp();
}
