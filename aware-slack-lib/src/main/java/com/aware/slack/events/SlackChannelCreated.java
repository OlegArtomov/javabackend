package com.aware.slack.events;

import com.aware.slack.SlackUser;

public interface SlackChannelCreated extends SlackChannelEvent
{
    SlackUser getCreator();
}
