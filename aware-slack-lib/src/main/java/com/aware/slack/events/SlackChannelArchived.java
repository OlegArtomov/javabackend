package com.aware.slack.events;

import com.aware.slack.SlackUser;

public interface SlackChannelArchived extends SlackChannelEvent
{
    SlackUser getUser();
}
