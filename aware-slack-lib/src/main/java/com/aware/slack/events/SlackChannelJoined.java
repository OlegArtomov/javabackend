package com.aware.slack.events;

/**
 * @author Oleg Artomov
 */
public interface SlackChannelJoined extends SlackChannelEvent {
}
