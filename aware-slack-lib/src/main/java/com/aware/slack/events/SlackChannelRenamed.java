package com.aware.slack.events;

public interface SlackChannelRenamed extends SlackChannelEvent
{
    String getNewName();
}
