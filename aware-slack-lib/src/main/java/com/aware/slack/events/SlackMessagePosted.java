package com.aware.slack.events;

import java.util.Map;
import org.json.simple.JSONObject;
import com.aware.slack.SlackBot;
import com.aware.slack.SlackChannel;
import com.aware.slack.SlackFile;
import com.aware.slack.SlackUser;

public interface SlackMessagePosted extends SlackMessageEvent
{
    String getMessageContent();

    SlackUser getSender();

    @Deprecated
    SlackBot getBot();

    SlackChannel getChannel();
    
    SlackFile getSlackFile();
    
    JSONObject getJsonSource();
    
    String getTimestamp();
    
    Map<String, Integer> getReactions();
    
    int getTotalCountOfReactions();

}
