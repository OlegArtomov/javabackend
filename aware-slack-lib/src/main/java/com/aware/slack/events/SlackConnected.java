package com.aware.slack.events;

import com.aware.slack.SlackPersona;

public interface SlackConnected extends SlackEvent
{
    SlackPersona getConnectedPersona();
}
