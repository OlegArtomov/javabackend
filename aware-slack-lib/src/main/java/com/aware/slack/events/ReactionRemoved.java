package com.aware.slack.events;

import com.aware.slack.SlackChannel;
import com.aware.slack.SlackUser;

public interface ReactionRemoved extends SlackEvent {

    String getEmojiName();
    SlackChannel getChannel();
    SlackUser getUser();
    String getMessageID();
    String getFileID();
    String getFileCommentID();
  
}
