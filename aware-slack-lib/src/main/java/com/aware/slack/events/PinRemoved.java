package com.aware.slack.events;

import com.aware.slack.SlackChannel;
import com.aware.slack.SlackFile;
import com.aware.slack.SlackUser;

public interface PinRemoved extends com.aware.slack.events.SlackEvent {

    SlackUser getSender();

    SlackChannel getChannel();

    String getTimestamp();

    SlackFile getFile();

    String getMessage();

}
