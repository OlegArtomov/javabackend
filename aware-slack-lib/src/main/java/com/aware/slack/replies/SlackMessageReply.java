package com.aware.slack.replies;

public interface SlackMessageReply extends ParsedSlackReply
{
    long getReplyTo();
    String getTimestamp();

}
