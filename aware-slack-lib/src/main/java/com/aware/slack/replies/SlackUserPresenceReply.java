package com.aware.slack.replies;

public interface SlackUserPresenceReply extends ParsedSlackReply
{
    boolean isActive();
}
