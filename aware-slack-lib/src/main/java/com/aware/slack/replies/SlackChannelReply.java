package com.aware.slack.replies;

import com.aware.slack.SlackChannel;

public interface SlackChannelReply extends ParsedSlackReply
{
    SlackChannel getSlackChannel();
}
