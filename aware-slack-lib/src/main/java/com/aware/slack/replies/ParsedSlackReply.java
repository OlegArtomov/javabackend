package com.aware.slack.replies;

public interface ParsedSlackReply extends SlackReply
{
    boolean isOk();
    String getErrorMessage();
}
