package com.aware.slack.replies;

import org.json.simple.JSONObject;

public interface GenericSlackReply extends com.aware.slack.replies.SlackReply
{
    JSONObject getPlainAnswer();
}
