package com.aware.slack;

/**
 * @author Oleg Artomov
 */
public interface SlackConnectionErrorListener {
    void onErrorDuringAuthentication(SlackSession slackSession, String errorCode);
}
