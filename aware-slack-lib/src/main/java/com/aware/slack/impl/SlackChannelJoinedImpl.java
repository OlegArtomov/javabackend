package com.aware.slack.impl;

import com.aware.slack.SlackChannel;
import com.aware.slack.events.SlackChannelJoined;
import com.aware.slack.events.SlackEventType;

import static com.aware.slack.events.SlackEventType.SLACK_CHANNEL_JOINED;

/**
 * @author Oleg Artomov
 */
public class SlackChannelJoinedImpl implements SlackChannelJoined {

    private final SlackChannel channel;

    public SlackChannelJoinedImpl(SlackChannel channel) {
        this.channel = channel;
    }

    @Override
    public SlackChannel getSlackChannel() {
        return channel;
    }

    @Override
    public SlackEventType getEventType() {
        return SLACK_CHANNEL_JOINED;
    }
}
