package com.aware.slack.impl;

import com.aware.slack.ChannelHistoryModule;
import com.aware.slack.SlackSession;

public class ChannelHistoryModuleFactory {
    
    public static ChannelHistoryModule createChannelHistoryModule(SlackSession session){
        return new ChannelHistoryModuleImpl(session);
    }

}
