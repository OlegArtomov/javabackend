package com.aware.slack.impl;

import com.aware.slack.SlackUser;
import com.aware.slack.events.SlackEventType;
import com.aware.slack.events.SlackUserChange;

public class SlackUserChangeImpl implements SlackUserChange {

    private SlackUser slackUser;

    SlackUserChangeImpl(SlackUser slackUser) {
        this.slackUser = slackUser;
    }

    @Override
    public SlackUser getUser() {
        return slackUser;
    }

    @Override
    public SlackEventType getEventType() {
        return SlackEventType.SLACK_USER_CHANGE;
    }
}
