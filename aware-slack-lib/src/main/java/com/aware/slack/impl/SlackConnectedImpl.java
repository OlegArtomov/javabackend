package com.aware.slack.impl;

import com.aware.slack.SlackPersona;
import com.aware.slack.events.SlackConnected;
import com.aware.slack.events.SlackEventType;

class SlackConnectedImpl implements SlackConnected
{
    private SlackPersona slackConnectedPersona;

    SlackConnectedImpl(SlackPersona slackConnectedPersona)
    {
        this.slackConnectedPersona = slackConnectedPersona;
    }

    @Override
    public SlackPersona getConnectedPersona()
    {
        return slackConnectedPersona;
    }

    @Override
    public SlackEventType getEventType()
    {
        return SlackEventType.SLACK_CONNECTED;
    }
}
