package com.aware.slack.impl;

import com.aware.slack.SlackBot;

@Deprecated
class SlackBotImpl extends SlackPersonaImpl implements SlackBot
{
    SlackBotImpl(String id, String userName, String realName, String userMail, boolean deleted, boolean admin, boolean owner,
                 boolean primaryOwner, boolean restricted, boolean ultraRestricted, String firstName, String lastName)
    {
        super(id, userName, realName, userMail, deleted, admin, owner, primaryOwner, restricted, ultraRestricted,
                true,null,null,0, firstName, lastName, "");
    }

}
