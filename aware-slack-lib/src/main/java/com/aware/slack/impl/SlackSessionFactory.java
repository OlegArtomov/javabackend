package com.aware.slack.impl;

import com.aware.slack.SlackConnectionErrorListener;
import com.aware.slack.SlackSession;

import java.net.Proxy;

public class SlackSessionFactory
{
    public static SlackSession createWebSocketSlackSession(String authToken, Long sessionId, SlackConnectionErrorListener slackConnectionErrorListener)
    {
        return new SlackWebSocketSessionImpl(authToken, true, sessionId, slackConnectionErrorListener);
    }

    public static SlackSession createWebSocketSlackSession(final String authToken, Proxy.Type proxyType, String proxyAddress, int proxyPort)
    {
        return new SlackWebSocketSessionImpl(authToken, proxyType, proxyAddress, proxyPort, true);
    }

}
