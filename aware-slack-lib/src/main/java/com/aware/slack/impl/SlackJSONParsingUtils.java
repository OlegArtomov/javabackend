package com.aware.slack.impl;

import com.aware.slack.SlackTeam;
import com.aware.slack.SlackUser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Map;

class SlackJSONParsingUtils
{

    private SlackJSONParsingUtils()
    {
        // Helper class
    }

    static SlackUser buildSlackUser(JSONObject jsonUser)
    {
        String id = (String) jsonUser.get("id");
        String name = (String) jsonUser.get("name");
        String realName = (String) jsonUser.get("real_name");
        String tz = (String) jsonUser.get("tz");
        String tzLabel = (String) jsonUser.get("tz_label");
        Long tzOffset = ((Long) jsonUser.get("tz_offset"));
        Boolean deleted = ifNullFalse(jsonUser, "deleted");
        Boolean admin = ifNullFalse(jsonUser, "is_admin");
        Boolean owner = ifNullFalse(jsonUser, "is_owner");
        Boolean primaryOwner = ifNullFalse(jsonUser, "is_primary_owner");
        Boolean restricted = ifNullFalse(jsonUser, "is_restricted");
        Boolean ultraRestricted = ifNullFalse(jsonUser, "is_ultra_restricted");
        Boolean bot = ifNullTrue(jsonUser, "is_bot");
        JSONObject profileJSON = (JSONObject) jsonUser.get("profile");
        String email = "";
        String firstName= "";
        String lastName = "";
        String phone = "";
        if (profileJSON != null)
        {
            email = (String) profileJSON.get("email");
            firstName = (String) profileJSON.get("first_name");
            lastName =  (String) profileJSON.get("last_name");
            phone = (String) profileJSON.get("phone");
        }
        return new SlackUserImpl(id, name, realName, email, deleted, admin, owner, primaryOwner, restricted, ultraRestricted, bot, tz, tzLabel, tzOffset == null ? null : new Integer(tzOffset.intValue()),
                firstName, lastName, phone);
    }

    private static Boolean ifNullFalse(JSONObject jsonUser, String field)
    {
        Boolean deleted = (Boolean) jsonUser.get(field);
        if (deleted == null)
        {
            deleted = false;
        }
        return deleted;
    }

    private static Boolean ifNullTrue(JSONObject jsonUser, String field)
    {
        Boolean deleted = (Boolean) jsonUser.get(field);
        if (deleted == null)
        {
            deleted = true;
        }
        return deleted;
    }

    static SlackChannelImpl buildSlackChannel(JSONObject jsonChannel, Map<String, SlackUser> knownUsersById)
    {
        String id = (String) jsonChannel.get("id");
        String name = (String) jsonChannel.get("name");
        String topic = null; // TODO
        String purpose = null; // TODO
        SlackChannelImpl toReturn = new SlackChannelImpl(id, name, topic, purpose, false);
        JSONArray membersJson = (JSONArray) jsonChannel.get("members");
        if (membersJson != null)
        {
            for (Object jsonMembersObject : membersJson)
            {
                String memberId = (String) jsonMembersObject;
                SlackUser user = knownUsersById.get(memberId);
                toReturn.addUser(user);
            }
        }
        return toReturn;
    }

    static  SlackChannelImpl buildSlackImChannel(JSONObject jsonChannel, Map<String, SlackUser> knownUsersById)
    {
        String id = (String) jsonChannel.get("id");
        SlackChannelImpl toReturn = new SlackChannelImpl(id, null, null, null, true);
        String memberId = (String) jsonChannel.get("user");
        SlackUser user = knownUsersById.get(memberId);
        toReturn.addUser(user);
        return toReturn;
    }

    static  SlackTeam buildSlackTeam(JSONObject jsonTeam)
    {
        String id = (String) jsonTeam.get("id");
        String name = (String) jsonTeam.get("name");
        String domain = (String) jsonTeam.get("domain");
        return new SlackTeamImpl(id, name, domain);
    }
}
