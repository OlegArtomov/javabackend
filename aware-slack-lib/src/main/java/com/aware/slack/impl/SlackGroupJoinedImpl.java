package com.aware.slack.impl;

import com.aware.slack.SlackChannel;
import com.aware.slack.events.SlackEventType;
import com.aware.slack.events.SlackGroupJoined;

class SlackGroupJoinedImpl implements SlackGroupJoined
{
    private SlackChannel slackChannel;

    SlackGroupJoinedImpl(SlackChannel slackChannel)
    {
        this.slackChannel = slackChannel;
    }

    @Override
    public SlackChannel getSlackChannel()
    {
        return slackChannel;
    }

    void setSlackChannel(SlackChannel slackChannel)
    {
        this.slackChannel = slackChannel;
    }

    @Override
    public SlackEventType getEventType()
    {
        return SlackEventType.SLACK_GROUP_JOINED;
    }

}
