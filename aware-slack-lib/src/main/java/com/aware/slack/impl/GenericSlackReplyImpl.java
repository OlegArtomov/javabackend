package com.aware.slack.impl;

import org.json.simple.JSONObject;
import com.aware.slack.replies.GenericSlackReply;

class GenericSlackReplyImpl implements GenericSlackReply
{
    private JSONObject obj;

    public GenericSlackReplyImpl(JSONObject obj)
    {
        this.obj = obj;
    }

    @Override
    public JSONObject getPlainAnswer()
    {
        return obj;
    }

}
