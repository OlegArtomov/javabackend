package com.aware.slack;

public interface SlackTeam
{
    String getId();
    String getName();
    String getDomain();
}
