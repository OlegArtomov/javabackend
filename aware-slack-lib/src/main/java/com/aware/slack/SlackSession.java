package com.aware.slack;

import com.aware.slack.impl.SlackChatConfiguration;
import com.aware.slack.listeners.PinAddedListener;
import com.aware.slack.listeners.PinRemovedListener;
import com.aware.slack.listeners.ReactionAddedListener;
import com.aware.slack.listeners.ReactionRemovedListener;
import com.aware.slack.listeners.SlackChannelArchivedListener;
import com.aware.slack.listeners.SlackChannelCreatedListener;
import com.aware.slack.listeners.SlackChannelDeletedListener;
import com.aware.slack.listeners.SlackChannelJoinedListener;
import com.aware.slack.listeners.SlackChannelRenamedListener;
import com.aware.slack.listeners.SlackChannelUnarchivedListener;
import com.aware.slack.listeners.SlackConnectedListener;
import com.aware.slack.listeners.SlackGroupJoinedListener;
import com.aware.slack.listeners.SlackMessageDeletedListener;
import com.aware.slack.listeners.SlackMessagePostedListener;
import com.aware.slack.listeners.SlackMessageUpdatedListener;
import com.aware.slack.listeners.SlackUserChangeListener;
import com.aware.slack.replies.GenericSlackReply;
import com.aware.slack.replies.ParsedSlackReply;
import com.aware.slack.replies.SlackChannelReply;
import com.aware.slack.replies.SlackMessageReply;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

public interface SlackSession {

    Collection<com.aware.slack.SlackChannel> getChannels();

    Collection<com.aware.slack.SlackUser> getUsers();

    Collection<com.aware.slack.SlackBot> getBots();

    com.aware.slack.SlackChannel findChannelByName(String channelName);

    com.aware.slack.SlackChannel findChannelById(String channelId);

    com.aware.slack.SlackUser findUserById(String userId);

    com.aware.slack.SlackUser findUserByUserName(String userName);

    com.aware.slack.SlackUser findUserByEmail(String userMail);

    com.aware.slack.SlackPersona sessionPersona();

    Long getSessionId();

    @Deprecated
    com.aware.slack.SlackBot findBotById(String botId);
    

    com.aware.slack.SlackMessageHandle<ParsedSlackReply> inviteUser(String email, String firstName, boolean setActive);

    void connect() throws IOException;

    void disconnect() throws IOException;

    com.aware.slack.SlackMessageHandle<SlackMessageReply> deleteMessage(String timeStamp, com.aware.slack.SlackChannel channel);

    com.aware.slack.SlackMessageHandle<SlackMessageReply> sendMessage(com.aware.slack.SlackChannel channel, String message, com.aware.slack.SlackAttachment attachment, SlackChatConfiguration chatConfiguration, boolean unfurl);

    com.aware.slack.SlackMessageHandle<SlackMessageReply> sendMessage(com.aware.slack.SlackChannel channel, String message, com.aware.slack.SlackAttachment attachment, SlackChatConfiguration chatConfiguration);

    com.aware.slack.SlackMessageHandle<SlackMessageReply> sendMessage(com.aware.slack.SlackChannel channel, String message, com.aware.slack.SlackAttachment attachment, boolean unfurl);

    com.aware.slack.SlackMessageHandle<SlackMessageReply> sendMessage(com.aware.slack.SlackChannel channel, String message, com.aware.slack.SlackAttachment attachment);

    com.aware.slack.SlackMessageHandle<SlackMessageReply> sendMessage(com.aware.slack.SlackChannel channel, String message, boolean unfurl);

    com.aware.slack.SlackMessageHandle<SlackMessageReply> sendMessage(com.aware.slack.SlackChannel channel, String message);

    com.aware.slack.SlackMessageHandle<SlackMessageReply> sendMessageToUser(com.aware.slack.SlackUser user, String message, com.aware.slack.SlackAttachment attachment);
    
    com.aware.slack.SlackMessageHandle<SlackMessageReply> sendMessageToUser(String userName, String message, com.aware.slack.SlackAttachment attachment);
    
    com.aware.slack.SlackMessageHandle<SlackMessageReply> updateMessage(String timeStamp, com.aware.slack.SlackChannel channel, String message);

    com.aware.slack.SlackMessageHandle<SlackMessageReply> sendMessageOverWebSocket(com.aware.slack.SlackChannel channel, String message);

    com.aware.slack.SlackMessageHandle<SlackMessageReply> addReactionToMessage(com.aware.slack.SlackChannel channel, String messageTimeStamp, String emojiCode);

    com.aware.slack.SlackMessageHandle<SlackChannelReply> joinChannel(String channelName);

    com.aware.slack.SlackMessageHandle<SlackChannelReply> leaveChannel(com.aware.slack.SlackChannel channel);
    
    com.aware.slack.SlackMessageHandle<SlackChannelReply> inviteToChannel(com.aware.slack.SlackChannel channel, com.aware.slack.SlackUser user);
    
    com.aware.slack.SlackMessageHandle<ParsedSlackReply> archiveChannel(com.aware.slack.SlackChannel channel);

    com.aware.slack.SlackMessageHandle<SlackChannelReply> openDirectMessageChannel(com.aware.slack.SlackUser user);

    com.aware.slack.SlackMessageHandle<SlackChannelReply> openMultipartyDirectMessageChannel(com.aware.slack.SlackUser... users);

    com.aware.slack.SlackPersona.SlackPresence getPresence(com.aware.slack.SlackPersona persona);

    SlackUser getInfoById(String userId);

    com.aware.slack.SlackMessageHandle<GenericSlackReply> postGenericSlackCommand(Map<String, String> params, String command);

    void addchannelArchivedListener(SlackChannelArchivedListener listener);

    void removeChannelArchivedListener(SlackChannelArchivedListener listener);

    void addchannelCreatedListener(SlackChannelCreatedListener listener);

    void removeChannelCreatedListener(SlackChannelCreatedListener listener);

    void addchannelDeletedListener(SlackChannelDeletedListener listener);

    void addchannelJoinedListener(SlackChannelJoinedListener listener);

    void removeChannelDeletedListener(SlackChannelDeletedListener listener);

    void addChannelRenamedListener(SlackChannelRenamedListener listener);

    void removeChannelRenamedListener(SlackChannelRenamedListener listener);

    void addChannelUnarchivedListener(SlackChannelUnarchivedListener listener);

    void removeChannelUnarchivedListener(SlackChannelUnarchivedListener listener);

    void addMessageDeletedListener(SlackMessageDeletedListener listener);

    void removeMessageDeletedListener(SlackMessageDeletedListener listener);

    void addMessagePostedListener(SlackMessagePostedListener listener);

    void removeMessagePostedListener(SlackMessagePostedListener listener);

    void addMessageUpdatedListener(SlackMessageUpdatedListener listener);

    void removeMessageUpdatedListener(SlackMessageUpdatedListener listener);

    void addGroupJoinedListener(SlackGroupJoinedListener listener);

    void removeGroupJoinedListener(SlackGroupJoinedListener listener);


    /*
     * Subscribe to events related to the actions to the slack
     * server. At this time a set of status information is exchanged that
     * is useful to implementing bots.
     * 
     * For example, the current user that is connecting.
     * knowing your own user id will help you stop answering your own
     * questions.
     */
    void addSlackConnectedListener(SlackConnectedListener listner);
    
    void removeSlackConnectedListener(SlackConnectedListener listener);

    /**
     * 
     * @return true if actions is open
     */
    boolean isConnected();
    
    void addReactionAddedListener(ReactionAddedListener listener);
    
    void removeReactionAddedListener(ReactionAddedListener listener);
    
    void addReactionRemovedListener(ReactionRemovedListener listener);
    
    void removeReactionRemovedListener(ReactionRemovedListener listener);

    void addSlackUserChangeListener(SlackUserChangeListener listener);

    void removeSlackUserChangeListener(SlackUserChangeListener listener);

    void addPinAddedListener(PinAddedListener listener);

    void removePinAddedListener(PinAddedListener listener);

    void addPinRemovedListener(PinRemovedListener listener);
  
    void removePinRemovedListener(PinRemovedListener listener);

    void registerUser(SlackUser user);

    String SLACK_API_HTTPS_ROOT = "https://slack.com/api/";
}
