package com.aware.slack;

/**
 * @author Oleg Artomov
 */
public final class SlackErrorCode {

    private SlackErrorCode() {

    }

    public static final String ACCOUNT_INACTIVE_ERROR_CODE = "account_inactive";
}
