package com.aware.slack;

import com.aware.slack.impl.SlackChannelImpl;

/**
 * @author Oleg Artomov
 */
public final class ChannelUtils {

    private ChannelUtils() {
        // Namespace
    }

    public static SlackChannel fromChannelId(String channelId){
        return new SlackChannelImpl(channelId, null, null, null,
                channelId.startsWith("D"));
    }
}
