package com.aware;

import com.google.common.collect.Maps;
import com.stripe.exception.StripeException;
import com.stripe.model.Token;
import com.stripe.net.RequestOptions;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Oleg Artomov
 */
public class StripeTokenGenerator {


    public static void main(String[] args) throws StripeException{
        RequestOptions requestOptions = new RequestOptions.RequestOptionsBuilder()
                .setApiKey("sk_test_rVapqWKvc2lPsap6SW53hHSL")
                .build();
        Map<String, Object> cardMap = new HashMap<>();
        cardMap.put("number", "4242424242424242");
        cardMap.put("exp_month", 12);
        cardMap.put("exp_year", 2020);
        Map<String, Object> tokenParams = Maps.newHashMap();
        tokenParams.put("card", cardMap);
        Token token = Token.create(tokenParams, requestOptions);
        System.out.println(token.getId());
    }
}
