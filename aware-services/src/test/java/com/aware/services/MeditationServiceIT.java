package com.aware.services;

import com.aware.model.AwareUser;
import com.aware.model.FacebookUserInfo;
import com.aware.model.Meditation;
import com.aware.repositories.MeditationRepository;
import com.aware.services.config.AwareConfiguration;
import org.junit.Test;

import javax.annotation.Resource;

import static java.lang.Boolean.TRUE;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Oleg Artomov
 */
public class MeditationServiceIT extends AbstractServiceIT {

    @Resource
    private MeditationService meditationService;

    @Resource
    private UserService userService;

    @Resource
    private MeditationRepository meditationRepository;

    @Resource
    private AwareConfiguration awareConfiguration;

    @Test
    public void testExceptionWhenCreateNewMeditationForTeacher() {
        prepareResponseForLogin(STUDENT_FACEBOOK_TOKEN, STUDENT_FACEBOOK_USER_ID, STUDENT_FACEBOOK_USER_ID);
        FacebookUserInfo facebookUserInfo = userService.loginUsingFacebook(STUDENT_FACEBOOK_TOKEN, true);
        meditationService.createForUser(facebookUserInfo.getUser(), awareConfiguration.getMeditationDefaultDuration());
    }

    @Test
    public void testCreateNewMeditationForStudent() {
        AwareUser user = generateStudent();
        Meditation meditation = meditationService.createForUser(user, awareConfiguration.getMeditationDefaultDuration());
        assertThat(meditation, notNullValue());
        assertThat(meditation.getStudent(), equalTo(user));
        assertThat(meditation.isCompleted(), equalTo(false));
    }

    @Test
    public void testCompleteMeditation() {
        AwareUser student = generateStudent();
        Meditation meditation = meditationService.createForUser(student, awareConfiguration.getMeditationDefaultDuration());
        AwareUser teacher = generateTeacher();
        meditationService.acceptMeditation(meditation.getId(), teacher);
        meditationService.complete(meditation.getId(), 10);
        meditation = meditationRepository.findOne(meditation.getId());
        assertThat(meditation.isCompleted(), equalTo(TRUE));
    }

    @Test
    public void testAcceptMeditation() throws InterruptedException {
        AwareUser student = generateStudent();
        AwareUser teacher = generateTeacher();
        Meditation meditation = meditationService.createForUser(student, awareConfiguration.getMeditationDefaultDuration());
        meditationService.acceptMeditation(meditation.getId(), teacher);
        meditation = meditationRepository.findOne(meditation.getId());
        assertThat(meditation.getTeacher(), equalTo(teacher));
    }

}