package com.aware.services.billing;

import com.aware.model.AwareUser;
import com.aware.model.Subscription;
import com.aware.model.UserSubscription;
import com.aware.repositories.AwareUserRepository;
import com.aware.repositories.SubscriptionRepository;
import com.aware.repositories.UserSubscriptionRepository;
import com.aware.services.billing.stripe.StripeService;
import com.aware.services.config.AwareConfiguration;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.aware.model.SubscriptionType.FREE_TRIAL;
import static com.aware.model.SubscriptionType.REGULAR;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.rules.ExpectedException.none;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Oleg Artomov
 */
@RunWith(MockitoJUnitRunner.class)
public class PaymentServiceTest {

    @InjectMocks
    private PaymentService paymentService;

    @Mock
    private UserSubscriptionRepository userSubscriptionRepository;

    @Mock
    private SubscriptionRepository subscriptionRepository;

    @Mock
    private AwareUserRepository awareUserRepository;

    @Mock
    private StripeService stripeService;

    @Mock
    private AwareConfiguration awareConfiguration;

    private Subscription freeTrialSubscription;

    private Subscription regularSubscription;

    @Rule
    public ExpectedException expectedException = none();

    @Before
    public void beforeEachTest(){
        freeTrialSubscription = new Subscription.Builder().type(FREE_TRIAL).id(1L).build();
        regularSubscription = new Subscription.Builder().type(REGULAR).id(2L).build();
        when(subscriptionRepository.findByType(eq(FREE_TRIAL))).thenReturn(freeTrialSubscription);
        when(subscriptionRepository.findByType(eq(REGULAR))).thenReturn(regularSubscription);
        paymentService.afterConstruction();
    }

    @Test
    public void testProvideFreeTrialForUserWhenUserIsNotRegisteredAndNoStripeToken(){
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Stripe token must be not empty");
        AwareUser awareUser = new AwareUser.Builder().email("aa@ukr.net").build();
        paymentService.provideSubscriptionForUser(awareUser, null, null, FREE_TRIAL);
    }

    @Test
    public void testProvideFreeTrialForUserWhenUserIsRegisteredAndNoStripeToken(){
        String stripeCustomerId = "12";
        AwareUser awareUser = new AwareUser.Builder().email("aa@ukr.net").stripeCustomerId(stripeCustomerId).build();
        String stripeToken = null;
        String customerInvoiceName = null;
        double price = 5;
        when(awareConfiguration.getMeditationTrialPrice()).thenReturn(price);
        paymentService.provideSubscriptionForUser(awareUser, stripeToken, customerInvoiceName, FREE_TRIAL);
        verify(stripeService, only()).billCustomer(eq(stripeCustomerId), eq(price));
        verify(awareUserRepository, only()).save(eq(awareUser));
        verify(userSubscriptionRepository, only()).save(notNull(UserSubscription.class));
        assertThat(awareUser.getCurrentSubscription(), notNullValue());
        assertThat(awareUser.getCurrentSubscription().getSubscription(), equalTo(freeTrialSubscription));
    }

    @Test
    public void testProvideFreeTrialForUserWhenUserIsNotRegisteredAndPresentStripeToken(){
        AwareUser awareUser = new AwareUser.Builder().email("aa@ukr.net").build();
        String stripeToken = "AA";
        String customerInvoiceName = "John Bull";
        String stripeCustomerId = "12";
        double price = 5;
        when(stripeService.createNewCustomer(eq(stripeToken))).thenReturn(stripeCustomerId);
        when(awareConfiguration.getMeditationTrialPrice()).thenReturn(price);
        paymentService.provideSubscriptionForUser(awareUser, stripeToken, customerInvoiceName, FREE_TRIAL);
        verify(stripeService).createNewCustomer(eq(stripeToken));
        verify(stripeService).billCustomer(eq(stripeCustomerId), eq(price));
        verify(awareUserRepository, only()).save(eq(awareUser));
        verify(userSubscriptionRepository, only()).save(notNull(UserSubscription.class));
        assertThat(awareUser.getCurrentSubscription(), notNullValue());
        assertThat(awareUser.getCurrentSubscription().getSubscription(), equalTo(freeTrialSubscription));
    }

    @Test
    public void testProvideSubscriptionUserWhenUserIsNotRegisteredAndPresentStripeToken(){
        AwareUser awareUser = new AwareUser.Builder().email("aa@ukr.net").build();
        String stripeToken = "AA";
        String customerInvoiceName = "John Bull";
        String stripeCustomerId = "12";
        String stripePlanId = "12";
        when(stripeService.createNewCustomer(eq(stripeToken))).thenReturn(stripeCustomerId);
        when(awareConfiguration.getStripeSubscriptionPlanId()).thenReturn(stripePlanId);
        paymentService.provideSubscriptionForUser(awareUser, stripeToken, customerInvoiceName, REGULAR);
        verify(stripeService).createNewCustomer(eq(stripeToken));
        verify(stripeService).subscribeCustomer(eq(stripeCustomerId), eq(stripePlanId));
        verify(awareUserRepository, only()).save(eq(awareUser));
        verify(userSubscriptionRepository, only()).save(notNull(UserSubscription.class));
        assertThat(awareUser.getCurrentSubscription(), notNullValue());
        assertThat(awareUser.getCurrentSubscription().getSubscription(), equalTo(regularSubscription));
        assertThat(awareUser.getStripeCustomerId(), equalTo(stripeCustomerId));
        assertThat(awareUser.getInvoiceName(), equalTo(customerInvoiceName));
    }

}