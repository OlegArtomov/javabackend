package com.aware.services;

import com.aware.exceptions.AwareRuntimeException;
import com.aware.model.AwareUser;
import com.aware.model.FacebookUserInfo;
import com.aware.repositories.AwareUserRepository;
import com.aware.repositories.FacebookUserInfoRepository;
import org.junit.Test;

import javax.annotation.Resource;

import static java.lang.Boolean.FALSE;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.text.IsEmptyString.isEmptyString;
import static org.junit.Assert.assertThat;

/**
 * @author Oleg Artomov
 */

public class UserServiceIT extends AbstractServiceIT{


    @Resource
    private UserService userService;

    @Resource
    private FacebookUserInfoRepository facebookUserInfoRepository;

    @Resource
    private AwareUserRepository awareUserRepository;

    @Test
    public void testLoginUsingFacebookForNewUser() {
        final boolean isTeacher = false;
        long countBeforeLogin = facebookUserInfoRepository.count();
        prepareResponseForLogin(STUDENT_FACEBOOK_TOKEN, STUDENT_FACEBOOK_USER_ID, STUDENT_FACEBOOK_USER_ID);
        FacebookUserInfo facebookUserInfo = userService.loginUsingFacebook(STUDENT_FACEBOOK_TOKEN, isTeacher);
        assertThat(facebookUserInfo.getFacebookUserId(), not(isEmptyString()));
        assertThat(facebookUserInfo.getUser().getFirstName(), not(isEmptyString()));
        assertThat(facebookUserInfo.getUser().getLastName(), not(isEmptyString()));
        long countAfterLogin = facebookUserInfoRepository.count();
        assertThat(countAfterLogin - countBeforeLogin, equalTo(1L));
    }

    @Test
    public void testLoginUsingFacebookForExistingUserWhenFacebookIdIsChangedBuEmailIsTheSame() {
        final boolean isTeacher = false;
        prepareResponseForLogin(STUDENT_FACEBOOK_TOKEN, STUDENT_FACEBOOK_USER_ID, STUDENT_FACEBOOK_USER_ID);
        userService.loginUsingFacebook(STUDENT_FACEBOOK_TOKEN, isTeacher);
        String newFacebookUserId = "1511891801";
        prepareResponseForLogin(STUDENT_FACEBOOK_TOKEN, newFacebookUserId, STUDENT_FACEBOOK_USER_ID);
        userService.loginUsingFacebook(STUDENT_FACEBOOK_TOKEN, isTeacher);
        assertThat(awareUserRepository.count(), equalTo(1L));
        assertThat(facebookUserInfoRepository.count(), equalTo(1L));
    }

    @Test
    public void testLoginUsingFacebookForExistingUser() {
        final boolean isTeacher = false;
        prepareResponseForLogin(STUDENT_FACEBOOK_TOKEN, STUDENT_FACEBOOK_USER_ID, STUDENT_FACEBOOK_USER_ID);
        FacebookUserInfo facebookUserInfoAfterLogin1 = userService.loginUsingFacebook(STUDENT_FACEBOOK_TOKEN, isTeacher);
        prepareResponseForLogin(STUDENT_FACEBOOK_TOKEN, STUDENT_FACEBOOK_USER_ID, STUDENT_FACEBOOK_USER_ID);
        FacebookUserInfo facebookUserInfoAfterLogin2 = userService.loginUsingFacebook(STUDENT_FACEBOOK_TOKEN, isTeacher);
        assertThat(facebookUserInfoAfterLogin1, equalTo(facebookUserInfoAfterLogin2));
        assertThat(facebookUserInfoRepository.count(), equalTo(1L));
    }

    @Test
    public void testLoginUsingFacebookForStudentThenSameTokenTeacher() {
        prepareResponseForLogin(STUDENT_FACEBOOK_TOKEN, STUDENT_FACEBOOK_USER_ID, STUDENT_FACEBOOK_USER_ID);
        FacebookUserInfo facebookUserInfo = userService.loginUsingFacebook(STUDENT_FACEBOOK_TOKEN, false);
        assertThat(facebookUserInfo.getUser().isTeacher(), equalTo(FALSE));
        prepareResponseForLogin(STUDENT_FACEBOOK_TOKEN, STUDENT_FACEBOOK_USER_ID, STUDENT_FACEBOOK_USER_ID);
        facebookUserInfo = userService.loginUsingFacebook(STUDENT_FACEBOOK_TOKEN, true);
        assertThat(facebookUserInfo.getUser().isTeacher(), equalTo(true));
    }

    @Test
    public void testUpdateUserInfoWhenNoUserWithThisEmail(){
        String phoneNumber = "+380575644545";
        String name = "teacherName";
        String surname = "teacherSurname";
        String email = "test@gmail.com";
        AwareUser student = generateStudent();
        userService.updateUserInfo(student, phoneNumber, email, name, surname);
    }

    @Test
    public void testUpdateUserInfoWhenPresentUserWithThisEmail(){
        expectedException.expect(AwareRuntimeException.class);
        AwareUser teacher = generateTeacher();
        AwareUser student = generateStudent();
        String phoneNumber = "+380575644545";
        String name = "teacherName";
        String surname = "teacherSurname";
        userService.updateUserInfo(teacher, phoneNumber, student.getEmail(), name, surname);
    }

    @Test
    public void testUpdateUserInfoWithInvalidPhone(){
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Phone number 1 must have valid format");
        AwareUser teacher = generateTeacher();
        String phoneNumber = "1";
        String name = "teacherName";
        String surname = "teacherSurname";
        String email = "test@gmail.com";
        userService.updateUserInfo(teacher, phoneNumber, email, name, surname);
    }
}
