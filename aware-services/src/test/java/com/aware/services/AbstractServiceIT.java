package com.aware.services;

import com.aware.config.AwareDaoConfiguration;
import com.aware.model.AwareUser;
import com.aware.model.FacebookUserInfo;
import com.aware.repositories.AwareUserRepository;
import com.aware.services.config.AwareServicesConfiguration;
import com.aware.services.config.TestServiceConfiguration;
import org.junit.After;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import javax.annotation.Resource;

import static com.aware.AwareTestConstants.DEFAULTS_TEST_PROPERTIES;
import static com.aware.AwareTestConstants.TRUNCATE_ALL_DATA_SCRIPT;
import static com.aware.spring.BeanUtils.resetMocksInApplicationContext;
import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.junit.rules.ExpectedException.none;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.client.MockRestServiceServer.createServer;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

/**
 * @author Oleg Artomov
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AwareServicesConfiguration.class, AwareDaoConfiguration.class,
        TestServiceConfiguration.class})
@Sql(scripts = {TRUNCATE_ALL_DATA_SCRIPT})
@TestPropertySource(locations = DEFAULTS_TEST_PROPERTIES)
public abstract class AbstractServiceIT {

    @Resource
    private ApplicationContext applicationContext;

    @Resource
    private UserService userService;

    @Resource
    private AwareUserRepository awareUserRepository;

    @Rule
    public ExpectedException expectedException = none();

    protected final String STUDENT_FACEBOOK_TOKEN = "facebookToken";

    protected final String STUDENT_FACEBOOK_USER_ID = "1511891800";

    protected final String TEACHER_FACEBOOK_TOKEN = "facebookToken1";

    protected final String TEACHER_FACEBOOK_USER_ID = "1511891801";

    @After
    public void afterEach(){
        resetMocksInApplicationContext(applicationContext);
    }

    protected void prepareResponseForLogin(String facebookToken, String facebookUserId, String userEmail){
        FacebookTemplate template = new FacebookTemplate(facebookToken);
        String graphUrl = template.getBaseGraphApiUrl();
        MockRestServiceServer mockServer = createServer(template.getRestTemplate());
        mockServer.expect(requestTo(startsWith(graphUrl + "me"))).
                andRespond(withSuccess("{\"id\":\"" + facebookUserId + "\",\"name\":\"Oleg Artyomov\",\"email\":\"" + userEmail + "@gmail.com\",\"__debug__\":{}}", APPLICATION_JSON));
        when(userService.getFacebookTemplate(eq(facebookToken))).thenReturn(template);
    }

    protected AwareUser generateTeacher() {
        return generateUserWithFacebookToken(TEACHER_FACEBOOK_TOKEN, TEACHER_FACEBOOK_USER_ID, true);
    }

    protected AwareUser generateStudent() {
        return generateUserWithFacebookToken(STUDENT_FACEBOOK_TOKEN, STUDENT_FACEBOOK_USER_ID, false);
    }

    private AwareUser generateUserWithFacebookToken(String student_facebook_token,
                                                    String student_facebook_user_id,
                                                    boolean isTeacher) {
        prepareResponseForLogin(student_facebook_token, student_facebook_user_id, student_facebook_user_id);
        FacebookUserInfo facebookUserInfo = userService.loginUsingFacebook(student_facebook_token, isTeacher);
        AwareUser user = facebookUserInfo.getUser();
        return awareUserRepository.findOne(user.getId());
    }
}
