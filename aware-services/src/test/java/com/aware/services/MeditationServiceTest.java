package com.aware.services;

import com.aware.exceptions.AwareRuntimeException;
import com.aware.model.AwareUser;
import com.aware.model.Meditation;
import com.aware.repositories.MeditationRepository;
import com.aware.services.config.AwareConfiguration;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static com.aware.utils.DateTimeUtils.now;
import static com.aware.utils.DateTimeUtils.resetClock;
import static com.aware.utils.DateTimeUtils.setClock;
import static java.time.Clock.fixed;
import static java.time.Instant.ofEpochMilli;
import static java.time.ZoneId.systemDefault;
import static org.apache.commons.lang3.time.DateUtils.addMilliseconds;
import static org.apache.commons.lang3.time.DateUtils.addSeconds;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.rules.ExpectedException.none;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Oleg Artomov
 */
@RunWith(MockitoJUnitRunner.class)
public class MeditationServiceTest {

    @InjectMocks
    private MeditationService meditationService;

    @Mock
    private MeditationRepository meditationRepository;

    @Mock
    private AwareConfiguration awareConfiguration;

    @Rule
    public ExpectedException expectedException = none();

    @Test
    public void testAcceptMeditationWhenTimeIsExpired() {
        expectedException.expect(AwareRuntimeException.class);
        final Long meditationId = 1L;
        final Integer meditationTimeToLive = 124;
        final Date meditationCreatedDate = addSeconds(now(), -1 * (meditationTimeToLive + 1));
        Meditation meditation = new Meditation.Builder().createdDate(meditationCreatedDate).build();
        when(awareConfiguration.getMeditationTimeToLiveInSeconds()).thenReturn(meditationTimeToLive);
        when(meditationRepository.findOne(eq(meditationId))).thenReturn(meditation);
        meditationService.acceptMeditation(meditationId, new AwareUser.Builder().email("aa@ukr.net").teacher(true).build());
    }

    @Test
    public void testAcceptMeditationWhenMeditationIsAlreadyAccepted() {
        expectedException.expect(AwareRuntimeException.class);
        final Long meditationId = 1L;
        final AwareUser alreadyAcceptedTeacher = new AwareUser.Builder().email("teacher@ukr.net").teacher(true).build();
        Meditation meditation = new Meditation.Builder().teacher(alreadyAcceptedTeacher).build();
        when(meditationRepository.findOne(eq(meditationId))).thenReturn(meditation);
        meditationService.acceptMeditation(meditationId, new AwareUser.Builder().email("aa@ukr.net").teacher(true).build());
    }

    @Test
    public void testAcceptMeditationByStudent() {
        expectedException.expect(IllegalArgumentException.class);
        final Long meditationId = 1L;
        meditationService.acceptMeditation(meditationId, new AwareUser.Builder().email("aa@ukr.net").teacher(false).build());
    }

    @Test
    public void testAcceptMeditationWithValidData() {
        final Long meditationId = 1L;
        final Integer meditationTimeToLive = 86400;
        final Date meditationCreatedDate = now();
        final Date currentDate = addMilliseconds(meditationCreatedDate, 1000);
        Meditation meditation = new Meditation.Builder().createdDate(meditationCreatedDate).build();
        when(awareConfiguration.getMeditationTimeToLiveInSeconds()).thenReturn(meditationTimeToLive);
        when(meditationRepository.findOne(eq(meditationId))).thenReturn(meditation);
        setClock(fixed(ofEpochMilli(currentDate.getTime()), systemDefault()));
        AwareUser teacher = new AwareUser.Builder().email("aa@ukr.net").teacher(true).id(1L).build();
        meditationService.acceptMeditation(meditationId, teacher);
        assertThat(meditation.getTeacher(), equalTo(teacher));
        assertThat(meditation.getStartDate(), equalTo(currentDate));
        verify(meditationRepository).save(meditation);
    }

    @After
    public void afterEachTest(){
        resetClock();
    }
}