package com.aware.services;

import com.aware.model.AwareUser;
import com.aware.services.security.JwtService;
import io.jsonwebtoken.JwtException;
import org.junit.Test;

import javax.annotation.Resource;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Oleg Artomov
 */
public class JwtServiceIT extends AbstractServiceIT {

    @Resource
    private JwtService jwtService;

    @Test
    public void testCreateForUser() throws NoSuchAlgorithmException, IOException {
        Long userId = 1L;
        AwareUser user = new AwareUser.Builder().id(userId).build();
        String token = jwtService.createForUser(user);
        Long userIdAfterParsing = Long.valueOf(jwtService.parseJWT(token));
        assertThat(userId, equalTo(userIdAfterParsing));
    }

    @Test
    public void testParseJWTForBadString(){
        expectedException.expect(JwtException.class);
        jwtService.parseJWT("12");
    }
}
