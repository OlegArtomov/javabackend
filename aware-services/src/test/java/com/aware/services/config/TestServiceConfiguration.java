package com.aware.services.config;

import com.aware.config.AwareProperties;
import com.aware.services.UserService;
import com.aware.spring.SpyDefinder;
import com.aware.spring.resourceloaders.ClasspathResourceLoader;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import liquibase.exception.LiquibaseException;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;

import static com.aware.AwareTestConstants.LIQUEBASE_CHANGELOG_XML;
import static com.aware.AwareTestConstants.SET_MYSQL_SYNTAX_SCRIPT;
import static com.aware.spring.BeanUtils.beanNameForClass;
import static com.aware.utils.AwareConstants.AWARE_BASE_PACKAGE;
import static com.google.common.collect.Lists.newArrayList;
import static org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType.HSQL;

/**
 * @author Oleg Artomov
 */
@Configuration
@ComponentScan(
        basePackages = AWARE_BASE_PACKAGE,
        useDefaultFilters = false,
        includeFilters = {
                @ComponentScan.Filter({
                        Service.class
                })
        }
)
@Import(AwareProperties.class)
public class TestServiceConfiguration {

    @Bean
    @Primary
    public DataSource dataSource() throws LiquibaseException {
        EmbeddedDatabase embeddedDatabase = new EmbeddedDatabaseBuilder().setType(HSQL).
                addScripts(SET_MYSQL_SYNTAX_SCRIPT).build();
        HikariConfig config = new HikariConfig();
        config.setDataSource(embeddedDatabase);
        return new HikariDataSource(config);
    }

    @Bean
    @Primary
    public SpringLiquibase dbUpdater() throws LiquibaseException {
        SpringLiquibase result = new SpringLiquibase();
        result.setDataSource(dataSource());
        result.setChangeLog(LIQUEBASE_CHANGELOG_XML);
        result.setResourceLoader(new ClasspathResourceLoader());
        return result;
    }

    @Bean
    public Database database(){
        return Database.HSQL;
    }

    @Bean
    public SpyDefinder spyDefinder() {
        return new SpyDefinder(newArrayList(beanNameForClass(UserService.class)));
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
