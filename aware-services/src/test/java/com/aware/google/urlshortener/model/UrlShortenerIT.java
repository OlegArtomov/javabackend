package com.aware.google.urlshortener.model;

import com.aware.services.AbstractServiceIT;
import com.aware.services.utils.UrlShortener;
import org.junit.Test;

import javax.annotation.Resource;

import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

/**
 * @author Oleg Artomov
 */
public class UrlShortenerIT extends AbstractServiceIT {

    @Resource
    private UrlShortener urlShortener;

    @Test
    public void testMakeShort() throws Exception {
        final String url = "http://itc.ua";
        String shortUrl = urlShortener.makeShort(url);
        assertThat(shortUrl, not(url.equals(shortUrl)));
        assertThat(shortUrl, startsWith("http://goo.gl"));
    }
}