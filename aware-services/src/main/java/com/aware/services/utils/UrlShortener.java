package com.aware.services.utils;

import com.aware.services.config.AwareConfiguration;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.net.URI;
import java.util.Map;

import static com.google.common.collect.ImmutableMap.of;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

/**
 * @author Oleg Artomov
 */
@Service
public class UrlShortener {

    private static final Logger logger = getLogger(UrlShortener.class);

    @Resource
    private AwareConfiguration awareConfiguration;

    private URI googleApiUri;

    public String makeShort(String fullUrl) {
        StopWatch stopWatch = new StopWatch("UrlShortener");
        stopWatch.start();
        try {
            return new RestTemplate().postForObject(googleApiUri, of("longUrl", fullUrl), Map.class).get("id").toString();
        } finally {
            stopWatch.stop();
            logger.debug(stopWatch.shortSummary());
        }
    }

    @PostConstruct
    private void afterConstruction() {
        googleApiUri = fromHttpUrl("https://www.googleapis.com/urlshortener/v1/url").
                queryParam("key", awareConfiguration.getGoogleApiKey()).build().encode().toUri();
    }
}
