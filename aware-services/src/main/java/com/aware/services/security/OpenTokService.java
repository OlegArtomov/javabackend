package com.aware.services.security;

import com.aware.exceptions.AwareRuntimeException;
import com.aware.services.config.AwareConfiguration;
import com.opentok.OpenTok;
import com.opentok.Session;
import com.opentok.TokenOptions;
import com.opentok.exception.OpenTokException;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import static com.aware.exceptions.AwareErrorCode.ERROR_GENERATION_OPEN_TOK;
import static java.lang.System.currentTimeMillis;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * @author Oleg Artomov
 */
@Service
public class OpenTokService {

    @Resource
    private AwareConfiguration awareConfiguration;


    public Triple<String, String, String> generateOpenTokInfo(){
        OpenTok opentok = new OpenTok(awareConfiguration.getOpenTokApiKey(),
                awareConfiguration.getOpenTokApiSecret());
        try {
            Session session = opentok.createSession();
            String sessionId = session.getSessionId();
            TokenOptions tokenOptions = buildTokenOptions();
            String studentToken = session.generateToken(tokenOptions);
            String teacherToken = session.generateToken(tokenOptions);
            return Triple.of(sessionId, studentToken, teacherToken);
        }
        catch (final OpenTokException openTokException){
            throw new AwareRuntimeException(openTokException, ERROR_GENERATION_OPEN_TOK);
        }
    }

    private TokenOptions buildTokenOptions() {
        TokenOptions.Builder builder = new TokenOptions.Builder();
        Integer meditationTimeToLiveInSeconds = awareConfiguration.getMeditationTimeToLiveInSeconds();
        if (meditationTimeToLiveInSeconds >= 0 ){
            double expireTimeInSeconds = MILLISECONDS.toSeconds(currentTimeMillis()) + meditationTimeToLiveInSeconds;
            builder.expireTime(expireTimeInSeconds);
        }
        return builder.build();
    }
}
