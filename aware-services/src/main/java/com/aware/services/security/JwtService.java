package com.aware.services.security;

import com.aware.model.AwareUser;
import com.aware.services.config.AwareConfiguration;
import com.aware.spring.resourceloaders.CombinedResourceLoader;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.Key;
import java.util.Date;

import static com.aware.utils.AwareConstants.SECRET_KEY_FILE;
import static com.aware.utils.DateTimeUtils.now;
import static com.google.common.io.ByteStreams.toByteArray;
import static io.jsonwebtoken.SignatureAlgorithm.HS256;
import static org.springframework.util.Assert.isTrue;

/**
 * @author Oleg Artomov
 */
@Service
public class JwtService {

    @Resource
    private CombinedResourceLoader combinedResourceLoader;

    @Resource
    private AwareConfiguration awareConfiguration;

    private  Key signingKey;

    private final SignatureAlgorithm signatureAlgorithm = HS256;

    public String createForUser(AwareUser user){
        return createJWT(user.getId().toString(), "", "");
    }

    private String createJWT(String userId, String issuer, String subject) {
        Date now = now();
        long nowMillis = now.getTime();

        //Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().setId(userId)
                .setIssuedAt(now)
                .setSubject(subject)
                .setIssuer(issuer)
                .signWith(signatureAlgorithm, signingKey);

        //if it has been specified, let's add the expiration
        long ttl = awareConfiguration.getTokenTimeToLiveInMillis();
        if (ttl >= 0) {
            long expMillis = nowMillis + ttl;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        //Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

    public String parseJWT(String jwt) {
//This line will throw an exception if it is not a signed JWS (as expected)
        Claims claims = Jwts.parser()
                .setSigningKey(signingKey)
                .parseClaimsJws(jwt).getBody();
        return claims.getId();
    }

    @PostConstruct
    private void afterConstruction() {
        org.springframework.core.io.Resource loadedResource = combinedResourceLoader.getResource(SECRET_KEY_FILE);
        isTrue(loadedResource.exists(), "File with name " + SECRET_KEY_FILE + " is not found");
        try {
            signingKey = new SecretKeySpec(toByteArray(loadedResource.getInputStream()), signatureAlgorithm.getJcaName());
            //OAR, This line is called, because construction of OBJECT_MAPPER in DefaultJwtBuilder costs near 50 ms
            //So, during startup of application we init it.
            createJWT("1", "", "");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
