package com.aware.services;

import com.aware.exceptions.AwareRuntimeException;
import com.aware.model.AwareUser;
import com.aware.model.Meditation;
import com.aware.model.UserSubscription;
import com.aware.repositories.MeditationRepository;
import com.aware.services.config.AwareConfiguration;
import com.aware.services.security.OpenTokService;
import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

import static com.aware.exceptions.AwareErrorCode.INVALID_USER;
import static com.aware.exceptions.AwareErrorCode.MEDITATION_IS_ALREADY_ACCEPTED;
import static com.aware.exceptions.AwareErrorCode.MEDITATION_IS_ALREADY_EXPIRED;
import static com.aware.services.UserService.checkUserIsTeacher;
import static com.aware.utils.Assert.isFalse;
import static com.aware.utils.DateTimeUtils.now;
import static org.apache.commons.lang3.time.DateUtils.addSeconds;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.util.Assert.isTrue;

/**
 * @author Oleg Artomov
 */
@Service
@Transactional
public class MeditationService {

    private static final Logger logger = getLogger(MeditationService.class);

    @Resource
    private MeditationRepository meditationRepository;

    @Resource
    private OpenTokService openTokService;

    @Resource
    private AwareConfiguration awareConfiguration;

    public Meditation createForUser(AwareUser user, Integer duration) {
        logger.debug("Create meditation. User {}, duration {}", user.getId(), duration);
        Meditation result = new Meditation();
        result.setDuration(duration);
        result.setStudent(user);
        UserSubscription userSubscription = user.getCurrentSubscription();
        if (userSubscription != null) {
            logger.debug("Create meditation for user {} using subscription {}", user.getId(), userSubscription.getId());
            result.setUserSubscription(userSubscription);
        }
        Triple<String, String, String> openTok = openTokService.generateOpenTokInfo();
        result.setOpenTokSessionId(openTok.getLeft());
        result.setOpenTokStudentToken(openTok.getMiddle());
        result.setOpenTokTeacherToken(openTok.getRight());
        return meditationRepository.save(result);
    }

    public void assignFreeTrialSubscriptionForMeditation(Meditation meditation, UserSubscription userSubscription) {
        meditation.setUserSubscription(userSubscription);
        meditationRepository.save(meditation);
    }

    public Meditation acceptMeditation(Long id, AwareUser user) {
        try {
            logger.debug("Start accept meditation {} by user {}", id, user.getId());
            checkUserIsTeacher(user);
            Meditation meditation = meditationRepository.findOne(id);
            if (meditation.getTeacher() != null) {
                throw new AwareRuntimeException(MEDITATION_IS_ALREADY_ACCEPTED);
            }
            Date startDate = now();
            Date endOfMeditation = addSeconds(meditation.getCreationDate(), awareConfiguration.getMeditationTimeToLiveInSeconds());
            if (startDate.after(endOfMeditation)) {
                logger.debug("End of meditation {}, current date {}", endOfMeditation, startDate);
                throw new AwareRuntimeException(MEDITATION_IS_ALREADY_EXPIRED);
            }
            //TODO: Fix me. Date is very discussable moment
            meditation.setStartDate(startDate);
            meditation.setTeacher(user);
            return meditationRepository.save(meditation);
        } catch (ObjectOptimisticLockingFailureException e) {
            throw new AwareRuntimeException(e, MEDITATION_IS_ALREADY_ACCEPTED);
        }
    }

    public void complete(Long id, Integer duration) {
        Meditation meditation = meditationRepository.findOne(id);
        isFalse(meditation.isCompleted(), "Meditation is already completed");
        meditation.setCompleted(true);
        meditation.setDuration(duration);
        meditationRepository.save(meditation);
    }

    public void saveFeedback(Long id, AwareUser currentUser, Byte score, String feedbackText) {
        Meditation meditation = meditationRepository.findOne(id);
        isTrue(meditation.isCompleted(), "Meditation must be completed");
        if (currentUser.equals(meditation.getStudent())) {
            logger.debug("Save feedback from student {} about meditation {}", currentUser.getId(), id);
            meditation.setStudentScore(score);
            meditation.setStudentFeedback(feedbackText);
            meditationRepository.save(meditation);
        } else {
            if (currentUser.equals(meditation.getTeacher())) {
                logger.debug("Save feedback from teacher {} about meditation {}", currentUser.getId(), id);
                meditation.setTeacherScore(score);
                meditation.setTeacherFeedback(feedbackText);
                meditationRepository.save(meditation);
            } else {
                logger.warn("Unknown user {} want to save feedback about meditation {}", currentUser.getId(), id);
                throw new AwareRuntimeException(INVALID_USER);
            }
        }
    }

    public Meditation find(Long id) {
        return meditationRepository.findOne(id);
    }
}
