package com.aware.services.notification.mixpanel;

import com.aware.exceptions.AwareRuntimeException;
import com.aware.model.AwareUser;
import com.aware.services.config.AwareConfiguration;
import com.mixpanel.mixpanelapi.MessageBuilder;
import com.mixpanel.mixpanelapi.MixpanelAPI;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;

import static com.aware.exceptions.AwareErrorCode.ERROR_REGISTRATION_IN_MIXPANEL;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author Oleg Artomov
 */
@Service
public class MixPanelService {

    private static final Logger logger = getLogger(MixPanelService.class);

    @Resource
    private AwareConfiguration awareConfiguration;

    @Async
    public void registerNewUserFromSlack(AwareUser awareUser) {
        logger.debug("Register user {} in MixPanel", awareUser.getEmail());
        try {
            MessageBuilder messageBuilder = new MessageBuilder(awareConfiguration.getMixPanelProjectToken());
            String email = awareUser.getEmail();
            JSONObject props = new JSONObject()
                    .put("$email", email)
                    .put("type", "student")
                    .put("$browser", "slack");
            JSONObject update = messageBuilder.set(email, props);
            MixpanelAPI mixpanel = new MixpanelAPI();
            mixpanel.sendMessage(update);
            logger.debug("Registration user {} in MixPanel is successful", awareUser.getEmail());
        } catch (JSONException | IOException e) {
            throw new AwareRuntimeException(e, ERROR_REGISTRATION_IN_MIXPANEL);
        }
    }
}
