package com.aware.services.notification.sms;

import com.aware.services.config.AwareConfiguration;
import com.google.common.collect.ImmutableList;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author Oleg Artomov
 */
@Service
public class SMSSender {

    private static final Logger logger = getLogger(SMSSender.class);

    @Resource
    private AwareConfiguration awareConfiguration;

    public boolean send(String fromPhoneNumber, String smsText, String targetPhoneNumber) {
        TwilioRestClient client = new TwilioRestClient(awareConfiguration.getTwilioSID(),
                awareConfiguration.getTwilioToken());
        List<NameValuePair> smsParams = new ImmutableList.Builder<NameValuePair>()
                .add(new BasicNameValuePair("Body", smsText))
                .add(new BasicNameValuePair("To", targetPhoneNumber))
                .add(new BasicNameValuePair("From", fromPhoneNumber))
                .build();
        MessageFactory messageFactory = client.getAccount().getMessageFactory();
        try {
            messageFactory.create(smsParams);
            logger.debug("SMS with text \n {} \n has been sent to number {} from {}", smsText, targetPhoneNumber, fromPhoneNumber);
            return true;
        } catch (TwilioRestException e) {
            logger.error("Error during send SMS to number: {}", targetPhoneNumber, e);
        }
        return false;
    }
}
