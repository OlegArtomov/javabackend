package com.aware.services.notification.mail;

/**
 * @author Oleg Artomov
 */
public final class MailTemplates {


    private MailTemplates() {

    }

    public static final String NEW_MEDITATION_TEMPLATE_ID = "tem_f55LExkMAYmPLXwj48g2wU";

    public static final String SLACK_USER_REGISTRATION_TEMPLATE_ID = "tem_2aCpRHjdA3FeRDhxWU8ZE7";

    public static final String MAIL_NOTIFICATION_FEEDBACK_TEMPLATE_ID = "tem_miSCYFtLQUzLjCMGoNcQe7";
}
