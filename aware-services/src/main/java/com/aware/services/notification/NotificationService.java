package com.aware.services.notification;

import com.aware.model.AwareUser;
import com.aware.model.Meditation;
import com.aware.repositories.AwareUserRepository;
import com.aware.services.config.AwareConfiguration;
import com.aware.services.notification.mail.MailService;
import com.aware.services.notification.sms.SMSSender;
import com.aware.services.security.JwtService;
import com.aware.services.utils.UrlShortener;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.aware.services.notification.mail.MailTemplates.MAIL_NOTIFICATION_FEEDBACK_TEMPLATE_ID;
import static com.aware.services.notification.mail.MailTemplates.NEW_MEDITATION_TEMPLATE_ID;
import static com.aware.services.notification.mail.MailTemplates.SLACK_USER_REGISTRATION_TEMPLATE_ID;
import static com.aware.utils.AwareConstants.DEFAULT_FEEDBACK_TEXT;
import static com.aware.utils.AwareConstants.SLACK_CHANNEL_NO_ANSWER;
import static com.aware.utils.AwareConstants.SLACK_SESSION_NO_ANSWER;
import static com.google.common.collect.Iterables.getFirst;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author Oleg Artomov
 */
@Service
public class NotificationService {

    private static final Logger logger = getLogger(NotificationService.class);

    @Resource
    private AwareUserRepository awareUserRepository;

    @Resource
    private AwareConfiguration awareConfiguration;

    @Resource
    private JwtService jwtService;

    @Resource
    private SMSSender smsSender;

    @Resource
    private MailService mailService;

    @Resource
    private UrlShortener urlShortener;

    @Async
    public void notifyTeachersByEmailAboutNewMeditation(AwareUser user, Meditation meditation,
                                                        @Nullable String slackChannelForAnswer,
                                                        @Nullable Long slackSessionId) {
        List<AwareUser> teachers = awareUserRepository.findTeachersWithNotEmptyEmailExcludeUser(user.getId());
        if (isNotEmpty(teachers)) {
            String userName = meditation.getStudent().getUserName();
            for (AwareUser currentTeacher : teachers) {
                final String email = currentTeacher.getEmail();
                logger.debug("Send email about new meditation to user: {}", email);
                Map<String, Object> model =
                        buildModelForNotificationTeachers(userName, currentTeacher, meditation, slackChannelForAnswer,
                                slackSessionId);
                mailService.send(currentTeacher.getEmail(), emptyList(), NEW_MEDITATION_TEMPLATE_ID, model);
            }
        }
    }

    private Map<String, Object> buildModelForNotificationTeachers(String userName, AwareUser teacher,
                                                                  Meditation meditation, @Nullable String slackChannelForAnswer,
                                                                  @Nullable Long slackSessionId) {
        return new ImmutableMap.Builder<String, Object>()
                .put("teacherName", teacher.getUserName())
                .put("userName", userName)
                .put("durationInMinutes", meditation.getDuration())
                .put("acceptMeditationUrl", buildAcceptUrl(teacher, meditation.getId(), slackChannelForAnswer, slackSessionId)).build();
    }

    private String buildAcceptUrl(AwareUser currentTeacher, Long meditationId,
                                  @Nullable String slackChannelForAnswer,
                                  @Nullable Long slackSessionId) {
        String teacherToken = jwtService.createForUser(currentTeacher);
        String slackChannelInUrl = defaultIfEmpty(slackChannelForAnswer, SLACK_CHANNEL_NO_ANSWER);
        Long slackSessionInUrl = defaultIfNull(slackSessionId, SLACK_SESSION_NO_ANSWER);
        return urlShortener.makeShort(String.format(awareConfiguration.getAcceptMeditationUrl(), teacherToken,
                meditationId, slackChannelInUrl, slackSessionInUrl));
    }

    @Async
    public void notifyTeachersBySMSAboutNewMeditation(AwareUser user) {
        List<AwareUser> teachers = awareUserRepository.findTeachersWithNotEmptyPhoneNumberExcludeUser(user.getId());
        if (isNotEmpty(teachers)) {
            for (AwareUser currentTeacher : teachers) {
                String smsText = awareConfiguration.getSMSNewMeditationText();
                boolean resultOfSend = smsSender.send(awareConfiguration.getTwilioTeacherNumber(), smsText,
                        currentTeacher.getPhoneNumber());
                logger.debug("SMS about new meditation has been sent to user: {}. Successful: {}",
                        currentTeacher.getId(), resultOfSend);
            }
        }
    }

    @Async
    public void sendSmsToNumber(String fromPhoneNumber, String text, String phoneNumber) {
        smsSender.send(fromPhoneNumber, text, phoneNumber);
    }

    @Async
    public void sendWelcomeEmail(AwareUser awareUser) {
        logger.debug("Send welcome email to user: {}", awareUser.getId());
        mailService.send(awareUser.getEmail(), emptyList(), SLACK_USER_REGISTRATION_TEMPLATE_ID, emptyMap());
    }

    @Async
    public void sendEmailAboutFeedback(AwareUser awareUser, @Nullable String feedbackText, Byte score) {
        logger.debug("Send email about feedback from user : {}", awareUser.getId());
        Collection<String> admins = awareConfiguration.getAdmins();
        mailService.send(getFirst(admins, null), admins, MAIL_NOTIFICATION_FEEDBACK_TEMPLATE_ID,
                buildModelMapForFeedbackEmail(awareUser, feedbackText, score));
    }

    private Map<String, Object> buildModelMapForFeedbackEmail(AwareUser awareUser, @Nullable String feedbackText, Byte score) {
        return new ImmutableMap.Builder<String, Object>()
                .put("userName", awareUser.getUserName())
                .put("feedbackText", defaultIfNull(feedbackText, DEFAULT_FEEDBACK_TEXT))
                .put("score", score)
                .build();
    }

}
