package com.aware.services.notification.mail;

import com.aware.exceptions.AwareRuntimeException;
import com.aware.services.config.AwareConfiguration;
import com.google.common.collect.ImmutableMap;
import com.sendwithus.SendWithUs;
import com.sendwithus.exception.SendWithUsException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Map;

import static com.aware.exceptions.AwareErrorCode.ERROR_GENERATION_EMAIL;
import static com.google.common.collect.ImmutableMap.of;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

/**
 * @author Oleg Artomov
 */
@Service
public class MailService {

    @Resource
    private AwareConfiguration awareConfiguration;

    public void send(String recipient, Collection<String> cc, String templateId, Map<String, Object> model) {
        SendWithUs sendWithUsAPI = new SendWithUs(awareConfiguration.getSendWithUsApiKey());
        try {
            ImmutableMap<String, Object> receivers = of("address", recipient);
            Map<String, Object>[] ccReceivers = null;
            if (isNotEmpty(cc)) {
                ccReceivers = cc.stream().map(currentCC -> of("address", currentCC)).toArray(Map[]::new);
            }
            sendWithUsAPI.send(templateId, receivers, null, model, ccReceivers);
        } catch (SendWithUsException e) {
            throw new AwareRuntimeException(e, ERROR_GENERATION_EMAIL);
        }
    }
}
