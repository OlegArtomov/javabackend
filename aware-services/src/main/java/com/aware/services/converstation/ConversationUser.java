package com.aware.services.converstation;

/**
 * @author Oleg Artomov
 */
public class ConversationUser {

    private final String userId;

    private final String userEmail;

    private final String firstName;

    private final String lastName;

    private final String phone;

    public ConversationUser(String userId, String userEmail, String firstName, String lastName, String phone) {
        this.userId = userId;
        this.userEmail = userEmail;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;

    }

    public String getPhone() {
        return phone;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
