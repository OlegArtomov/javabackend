package com.aware.services.converstation;

/**
 * @author Oleg Artomov
 */
public class ConversationEvent {

    private final String sourceId;

    public ConversationEvent(String source) {
        this.sourceId = source;
    }

    public String getSourceId() {
        return sourceId;
    }
}
