package com.aware.services;

import com.aware.exceptions.AwareRuntimeException;
import com.aware.model.AwareUser;
import com.aware.model.FacebookUserInfo;
import com.aware.repositories.AwareUserRepository;
import com.aware.repositories.FacebookUserInfoRepository;
import com.aware.services.config.AwareConfiguration;
import com.aware.services.notification.NotificationService;
import com.aware.services.notification.mixpanel.MixPanelService;
import com.aware.services.converstation.ConversationUser;
import org.slf4j.Logger;
import org.springframework.social.facebook.api.User;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import static com.aware.exceptions.AwareErrorCode.EMAIL_IS_ALREADY_USED_BY_OTHER_USER;
import static com.aware.utils.PhoneNumberUtils.normalize;
import static com.aware.utils.PhoneNumberUtils.validatePhoneNumber;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.util.Assert.isTrue;
import static org.springframework.util.StringUtils.isEmpty;

/**
 * @author Oleg Artomov
 */
@Service
public class UserService {

    private static final Logger logger = getLogger(UserService.class);

    @Resource
    private FacebookUserInfoRepository facebookUserInfoRepository;

    @Resource
    private AwareUserRepository awareUserRepository;

    @Resource
    private AwareConfiguration awareConfiguration;

    @Resource
    private NotificationService notificationService;

    @Resource
    private MixPanelService mixPanelService;

    @Transactional
    public FacebookUserInfo loginUsingFacebook(String facebookAccessToken, boolean isTeacher) {
        FacebookTemplate facebookTemplate = getFacebookTemplate(facebookAccessToken);
        User userProfile = facebookTemplate.userOperations().getUserProfile();
        String facebookUserId = userProfile.getId();
        isTrue(isNotEmpty(facebookUserId), "Facebook user id is empty");
        logger.debug("Login user with facebook id: {}", facebookUserId);
        AwareUser awareUser = findUser(userProfile);
        if (awareUser == null) {
            awareUser = buildNewUser(isTeacher, userProfile);
            return registerFacebookUser(userProfile, awareUser);
        } else {
            if (awareUser.isTeacher() != isTeacher) {
                logger.warn("User with id {} has new isTeacher value: {}", awareUser.getId(), isTeacher);
                awareUser.setTeacher(isTeacher);
                awareUserRepository.save(awareUser);
            }
            return registerFacebookUser(userProfile, awareUser);
        }
    }

    private AwareUser findUser(User userProfile) {
        FacebookUserInfo facebookUserInfo = facebookUserInfoRepository.findByFacebookId(userProfile.getId());
        if (facebookUserInfo != null) {
            return facebookUserInfo.getUser();
        } else {
            if (!isEmpty(userProfile.getEmail())) {
                return awareUserRepository.findByEmail(userProfile.getEmail());
            }
        }
        return null;
    }

    public AwareUser loginUsingSlack(ConversationUser conversationUser, boolean isTeacher) {
        String userEmail = conversationUser.getUserEmail();
        isTrue(isNotEmpty(userEmail), "User email must be not null");
        AwareUser awareUser = awareUserRepository.findByEmail(userEmail);
        return awareUser != null ? awareUser : buildNewUserForSlack(isTeacher, conversationUser);
    }

    private AwareUser buildNewUserForSlack(boolean isTeacher, ConversationUser conversationUser) {
        AwareUser awareUser = new AwareUser();
        awareUser.setTeacher(isTeacher);
        awareUser.setEmail(conversationUser.getUserEmail());
        awareUser.setFirstName(conversationUser.getFirstName());
        awareUser.setLastName(conversationUser.getLastName());
        awareUser.setPhoneNumber(normalize(conversationUser.getPhone()));
        awareUserRepository.save(awareUser);
        notificationService.sendWelcomeEmail(awareUser);
        mixPanelService.registerNewUserFromSlack(awareUser);
        return awareUser;
    }

    protected FacebookTemplate getFacebookTemplate(String facebookAccessToken) {
        return new FacebookTemplate(facebookAccessToken);
    }

    private AwareUser buildNewUser(boolean isTeacher, User facebookUserProfile) {
        AwareUser awareUser = new AwareUser();
        awareUser.setTeacher(isTeacher);
        assignInfoFromFacebookProfile(facebookUserProfile, awareUser);
        return awareUserRepository.save(awareUser);
    }

    private void assignInfoFromFacebookProfile(User facebookUserProfile, AwareUser awareUser) {
        awareUser.setEmail(facebookUserProfile.getEmail());
        awareUser.setFirstName(facebookUserProfile.getFirstName());
        awareUser.setLastName(facebookUserProfile.getLastName());
    }

    private FacebookUserInfo registerFacebookUser(User userProfile, AwareUser awareUser) {
        FacebookUserInfo facebookUserInfo = awareUser.getFacebookUserInfo();
        boolean needSaveFacebookInfo = true;
        if (facebookUserInfo == null) {
            facebookUserInfo = new FacebookUserInfo();
        } else {
            //OAR:  It is possible situation, when facebookId is updated after change name or surname
            needSaveFacebookInfo = !facebookUserInfo.getFacebookUserId().equals(userProfile.getId());
        }
        if (needSaveFacebookInfo) {
            logger.debug("Update facebook info for user: {}", awareUser.getId());
            facebookUserInfo.setFacebookUserId(userProfile.getId());
            facebookUserInfo.setUser(awareUser);
            facebookUserInfo.setUserPicUrl(String.format(awareConfiguration.getFacebookUserPicUrl(),
                    userProfile.getId()));
            assignInfoFromFacebookProfile(userProfile, awareUser);
            awareUserRepository.save(awareUser);
            return facebookUserInfoRepository.save(facebookUserInfo);
        }
        return facebookUserInfo;
    }

    @Transactional
    public AwareUser updateUserInfo(AwareUser currentUser, String phoneNumber, String email, String name,
                                    String surname) {
        isTrue(isNotEmpty(email), "Email is empty");
        isTrue(validatePhoneNumber(phoneNumber), String.format("Phone number %1$s must have valid format", phoneNumber));
        AwareUser userByEmail = awareUserRepository.findByEmail(email);
        if (userByEmail != null && !currentUser.equals(userByEmail)) {
            throw new AwareRuntimeException(EMAIL_IS_ALREADY_USED_BY_OTHER_USER);
        }
        currentUser.setPhoneNumber(phoneNumber);
        currentUser.setFirstName(name);
        currentUser.setLastName(surname);
        currentUser.setEmail(email);
        return awareUserRepository.save(currentUser);
    }


    @Transactional
    public AwareUser registerByEmail(String phoneNumber, String email, String name,
                                     String surname) {
        isTrue(isNotEmpty(email), "Email is empty");
        isTrue(validatePhoneNumber(phoneNumber), String.format("Phone number %1$s must have valid format", phoneNumber));
        AwareUser userByEmail = awareUserRepository.findByEmail(email);
        if (userByEmail != null) {
            throw new AwareRuntimeException(EMAIL_IS_ALREADY_USED_BY_OTHER_USER);
        }
        AwareUser userForUpdate = new AwareUser();
        userForUpdate.setPhoneNumber(phoneNumber);
        userForUpdate.setFirstName(name);
        userForUpdate.setLastName(surname);
        userForUpdate.setEmail(email);
        return awareUserRepository.save(userForUpdate);
    }

    public static void checkUserIsTeacher(AwareUser user) {
        isTrue(user.isTeacher(), "User must be teacher");
    }
}
