package com.aware.services.config;

import com.aware.spring.resourceloaders.BaseDirResourceLoader;
import com.aware.spring.resourceloaders.ClasspathResourceLoader;
import com.aware.spring.resourceloaders.CombinedResourceLoader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * @author Oleg Artomov
 */
@Configuration
@Import({AwareAsyncConfigurer.class})
public class AwareServicesConfiguration {

    @Bean
    public CombinedResourceLoader resourceLoader(AwareConfiguration awareConfiguration) {
        String awareConfigDir = awareConfiguration.getAwareConfigDir();
        if (isNotEmpty(awareConfigDir)) {
            return new CombinedResourceLoader(
                    new BaseDirResourceLoader(awareConfigDir),
                    new ClasspathResourceLoader()
            );
        }
        return new CombinedResourceLoader(new ClasspathResourceLoader());
    }
}
