package com.aware.services.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Collection;

import static com.aware.utils.AwareConstants.AWARE_CONFIG_DIR_SYSTEM_PROPERTY_NAME;

/**
 * @author Oleg Artomov
 */
@Service
public class AwareConfiguration {

    @Value("${" + AWARE_CONFIG_DIR_SYSTEM_PROPERTY_NAME + ":#{null}}")
    private String awareConfigDir;

    @Value("${token.ttl.ms}")
    private Long tokenTimeToLiveInMillis;

    @Value("${openTok.apiKey}")
    private int openTokApiKey;

    @Value("${openTok.apiSecret}")
    private String openTokApiSecret;

    @Value("${meditation.ttl.seconds}")
    private Integer meditationTimeToLiveInSeconds;

    @Value("${slack.command.welcome}")
    private String slackWelcomeCommand;

    @Value("#{T(com.aware.utils.StringUtils).splitWithCommaSeparatorAndTrim('${slack.command.awareStart}')}")
    private Collection<String> slackStartDialog;

    @Value("${stripe.api.key}")
    private String stripeApiKey;

    @Value("${slack.successPayment.answer}")
    private String slackSuccessPaymentAnswer;

    @Value("${slack.acceptMeditationPageUrl}")
    private String acceptMeditationUrl;

    @Value("${slack.command.createMeditation.answer}")
    private String slackCreateMeditationCommandAnswer;

    @Value("${slack.command.unknown.answer}")
    private String slackUnknownCommand;

    @Value("${sms.twilio.sid}")
    private String twilioSID;

    @Value("${sms.twilio.token}")
    private String twilioToken;

    @Value("${sms.twilio.botNumber}")
    private String twilioBotNumber;

    @Value("${sms.twilio.teacherNumber}")
    private String twilioTeacherNumber;

    @Value("${sms.newMeditation}")
    private String smsNewMeditationText;

    @Value("${slack.paymentPageLink}")
    private String paymentPageUrl;

    @Value("${meditation.defaultDuration}")
    private Integer meditationDefaultDuration;

    @Value("#{T(com.aware.utils.StringUtils).splitWithCommaSeparatorAndTrim('${slack.positiveAnswer}')}")
    private Collection<String> slackPositiveAnswers;

    @Value("#{T(com.aware.utils.StringUtils).splitWithCommaSeparatorAndTrim('${slack.negativeAnswer}')}")
    private Collection<String> slackNegativeAnswer;

    @Value("${slack.successAccepted.answer}")
    private String slackSuccessAcceptedAnswer;

    @Value("${facebook.userpic.url}")
    private String facebookUserPicUrl;

    @Value("${meditation.trial.amount}")
    private Double meditationTrialPrice;

    @Value("${sms.initialProfileCreated}")
    private String smsProfileCreated;

    @Value("${google.apiKey}")
    private String googleApiKey;

    @Value("${stripe.subscription.plan.id}")
    private String stripeSubscriptionPlanId;

    @Value("#{T(com.aware.utils.StringUtils).splitWithCommaSeparatorAndTrim('${mail.admins}')}")
    private Collection<String> admins;

    @Value("${mixPanel.project.token}")
    private String mixPanelToken;

    @Value("${sendWithUs.apiKey}")
    private String sendWithUsApiKey;

    @Value("${slack.app.clientId}")
    private String slackAppClientId;

    @Value("${slack.app.clientSecret}")
    private String slackAppClientSecret;

    public String getSendWithUsApiKey() {
        return sendWithUsApiKey;
    }

    public String getStripeSubscriptionPlanId() {
        return stripeSubscriptionPlanId;
    }

    public String getTwilioTeacherNumber() {
        return twilioTeacherNumber;
    }

    public Double getMeditationTrialPrice() {
        return meditationTrialPrice;
    }

    public String getSlackWelcomeCommand() {
        return slackWelcomeCommand;
    }

    public Collection<String> getSlackPositiveAnswers() {
        return slackPositiveAnswers;
    }

    public Collection<String> getSlackNegativeAnswer() {
        return slackNegativeAnswer;
    }

    public Integer getMeditationDefaultDuration() {
        return meditationDefaultDuration;
    }

    public String getSlackCreateMeditationCommandAnswer() {
        return slackCreateMeditationCommandAnswer;
    }

    public String getSlackUnknownCommand() {
        return slackUnknownCommand;
    }

    public Integer getMeditationTimeToLiveInSeconds() {
        return meditationTimeToLiveInSeconds;
    }

    public int getOpenTokApiKey() {
        return openTokApiKey;
    }

    public String getOpenTokApiSecret() {
        return openTokApiSecret;
    }

    public Long getTokenTimeToLiveInMillis() {
        return tokenTimeToLiveInMillis;
    }

    public String getAwareConfigDir() {
        return awareConfigDir;
    }

    public String getTwilioSID() {
        return twilioSID;
    }

    public String getTwilioToken() {
        return twilioToken;
    }

    public String getTwilioBotNumber() {
        return twilioBotNumber;
    }

    public String getSMSNewMeditationText() {
        return smsNewMeditationText;
    }

    public String getPaymentPageUrl() {
        return paymentPageUrl;
    }

    public Collection<String> getSlackStartDialog() {
        return slackStartDialog;
    }

    public String getStripeApiKey() {
        return stripeApiKey;
    }

    public String getSlackSuccessPaymentAnswer() {
        return slackSuccessPaymentAnswer;
    }

    public String getAcceptMeditationUrl() {
        return acceptMeditationUrl;
    }

    public String getSlackSuccessAcceptedAnswer() {
        return slackSuccessAcceptedAnswer;
    }

    public String getFacebookUserPicUrl() {
        return facebookUserPicUrl;
    }

    public String getSMSProfileCreated() {
        return smsProfileCreated;
    }

    public String getGoogleApiKey() {
        return googleApiKey;
    }

    public Collection<String> getAdmins() {
        return admins;
    }

    public String getMixPanelProjectToken() {
        return mixPanelToken;
    }

    public String getSlackAppClientId() {
        return slackAppClientId;
    }

    public String getSlackAppClientSecret() {
        return slackAppClientSecret;
    }
}
