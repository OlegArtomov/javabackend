package com.aware.services.billing;

import com.aware.model.AwareUser;
import com.aware.model.Subscription;
import com.aware.model.SubscriptionType;
import com.aware.model.UserSubscription;
import com.aware.repositories.AwareUserRepository;
import com.aware.repositories.SubscriptionRepository;
import com.aware.repositories.UserSubscriptionRepository;
import com.aware.services.billing.stripe.StripeService;
import com.aware.services.config.AwareConfiguration;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import static com.aware.model.SubscriptionType.FREE_TRIAL;
import static com.aware.model.SubscriptionType.REGULAR;
import static com.aware.utils.Assert.isFalse;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author Oleg Artomov
 */
@Service
public class PaymentService {

    private static final Logger logger = getLogger(PaymentService.class);

    @Resource
    private UserSubscriptionRepository userSubscriptionRepository;

    @Resource
    private SubscriptionRepository subscriptionRepository;

    @Resource
    private AwareUserRepository awareUserRepository;

    @Resource
    private StripeService stripeService;

    @Resource
    private AwareConfiguration awareConfiguration;

    private Subscription freeTrialSubscription;

    private Subscription regularSubscription;

    public UserSubscription provideSubscriptionForUser(AwareUser user, @Nullable String stripeToken,
                                                       @Nullable String customerInvoiceName, SubscriptionType subscriptionType) {
        logger.debug("Start provide subscription {} for user: {}", subscriptionType, user.getId());
        checkStripeCustomer(user, stripeToken, customerInvoiceName);
        Subscription operationSubscription = null;
        switch (subscriptionType) {
            case FREE_TRIAL:
                operationSubscription = freeTrialSubscription;
                stripeService.billCustomer(user.getStripeCustomerId(), awareConfiguration.getMeditationTrialPrice());
                break;
            case REGULAR:
                operationSubscription = regularSubscription;
                stripeService.subscribeCustomer(user.getStripeCustomerId(), awareConfiguration.getStripeSubscriptionPlanId());
                break;
        }
        UserSubscription userSubscription = createUserSubscription(user, operationSubscription);
        logger.debug("Charge for user {} is successful", user.getId());
        return userSubscription;
    }

    private void checkStripeCustomer(AwareUser user, @Nullable String stripeToken, @Nullable String customerInvoiceName) {
        if (isEmpty(user.getStripeCustomerId())) {
            isFalse(isEmpty(stripeToken), "Stripe token must be not empty");
            isFalse(isEmpty(customerInvoiceName), "User name in invoice must be not empty");
            String stripeCustomerId = stripeService.createNewCustomer(stripeToken);
            user.setStripeCustomerId(stripeCustomerId);
            user.setInvoiceName(customerInvoiceName);
        }
    }

    private UserSubscription createUserSubscription(AwareUser user, Subscription subscription) {
        UserSubscription userSubscription = new UserSubscription();
        userSubscription.setUser(user);
        userSubscription.setSubscription(subscription);
        userSubscriptionRepository.save(userSubscription);
        user.setCurrentSubscription(userSubscription);
        awareUserRepository.save(user);
        return userSubscription;
    }

    @PostConstruct
    public void afterConstruction() {
        freeTrialSubscription = subscriptionRepository.findByType(FREE_TRIAL);
        regularSubscription = subscriptionRepository.findByType(REGULAR);
    }
}
