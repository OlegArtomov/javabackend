package com.aware.services.billing.stripe;

import com.aware.exceptions.AwareRuntimeException;
import com.aware.services.config.AwareConfiguration;
import com.google.common.collect.ImmutableMap;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.model.Customer;
import com.stripe.net.RequestOptions;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import static com.aware.exceptions.AwareErrorCode.ERROR_PAYMENT_IN_STRIPE;
import static com.google.common.collect.ImmutableMap.of;
import static com.stripe.model.Customer.create;
import static com.stripe.model.Customer.retrieve;
import static org.apache.commons.lang3.BooleanUtils.isTrue;
import static org.slf4j.LoggerFactory.getLogger;


/**
 * @author Oleg Artomov
 */
@Service
public class StripeService {

    private static final Logger logger = getLogger(StripeService.class);

    private RequestOptions requestOptions;

    @Resource
    private AwareConfiguration awareConfiguration;

    public String createNewCustomer(String sourceToken) {
        try {
            Customer customer = create(of("source", sourceToken), requestOptions);
            return customer.getId();
        } catch (StripeException e) {
            throw new AwareRuntimeException(e, ERROR_PAYMENT_IN_STRIPE);
        }
    }

    public void billCustomer(String stripeCustomerId, Double price) {
        try {
            ImmutableMap<String, Object> dataForCharge = new ImmutableMap.Builder<String, Object>()
                    .put("amount", Double.valueOf(price * 100).longValue())
                    .put("currency", "usd")
                    .put("customer", stripeCustomerId)
                    .build();
            Charge result = Charge.create(dataForCharge, requestOptions);
            if (!isTrue(result.getPaid())) {
                logger.warn("Error during payment for customerId {}. Response: {}", stripeCustomerId, result);
                throw new AwareRuntimeException(ERROR_PAYMENT_IN_STRIPE);
            }
        } catch (StripeException e) {
            throw new AwareRuntimeException(e, ERROR_PAYMENT_IN_STRIPE);
        }
    }

    public void subscribeCustomer(String stripeCustomerId, String planId) {
        try {
            Customer customer = retrieve(stripeCustomerId, requestOptions);
            customer.createSubscription(of("plan", planId), requestOptions);
        } catch (StripeException e) {
            throw new AwareRuntimeException(e, ERROR_PAYMENT_IN_STRIPE);
        }
    }

    @PostConstruct
    private void afterConstruction() {
        requestOptions = new RequestOptions.RequestOptionsBuilder()
                .setApiKey(awareConfiguration.getStripeApiKey()).build();
    }
}
