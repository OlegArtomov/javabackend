package com.aware.exceptions;

/**
 * @author Oleg Artomov
 */
public class AwareRuntimeException extends RuntimeException {

    private final AwareErrorCode awareErrorCode;

    public AwareErrorCode getAwareErrorCode() {
        return awareErrorCode;
    }

    public AwareRuntimeException(final Throwable cause, final AwareErrorCode awareErrorCode) {
        super(cause);
        this.awareErrorCode = awareErrorCode;
    }

    public AwareRuntimeException(final AwareErrorCode awareErrorCode){
        this.awareErrorCode = awareErrorCode;
    }
}
