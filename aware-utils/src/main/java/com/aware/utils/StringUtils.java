package com.aware.utils;

import com.google.common.base.Splitter;

import java.util.List;

/**
 * @author Oleg Artomov
 */
public final class StringUtils {

    private static final char COMMA = ',';
    private static final Splitter commaSplitter = Splitter.on(COMMA).trimResults();

    private StringUtils() {
        // Namespace
    }

    public static List<String> splitWithCommaSeparatorAndTrim(final String value) {
        return commaSplitter.splitToList(value);
    }

}
