package com.aware.utils;

/**
 * @author Oleg Artomov
 */
public final class Assert {

    private Assert(){
        // Namespace
    }

    public static void isFalse(boolean expression, String message) {
        if (expression) {
            throw new IllegalArgumentException(message);
        }
    }
}
