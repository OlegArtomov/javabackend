package com.aware.utils;

/**
 * @author Oleg Artomov
 */
public final class AwareConstants {

    private AwareConstants() {

    }

    public static final String AWARE_BASE_PACKAGE = "com.aware";

    public static final String AWARE_CONFIG_DIR_SYSTEM_PROPERTY_NAME = "aware.config.dir";

    public static final String AWARE_DEFAULTS_PROPERTIES_FILE = "awareDefaults.properties";

    public static final String AWARE_PROPERTIES_FILE = "aware.properties";

    public static final String HIBERNATE_DEFAULT_PROPERTIES_FILE = "hibernateDefaults.properties";

    public static final String AWARE_HIBERNATE_PROPERTIES_FILE = "awareHibernate.properties";

    public static final String HIKARI_DEFAULT_PROPERTIES_FILE = "hikariDefaults.properties";

    public static final String AWARE_HIKARI_PROPERTIES_FILE = "awareHikari.properties";

    public static final String SERVER_VERSION_INFO_PROPERTIES_FILE = "serverVersionInfo.properties";

    public static final String SECRET_KEY_FILE = "secretKey";

    public static final String SLACK_CHANNEL_NO_ANSWER = "NO_CHANNEL";

    public static final Long SLACK_SESSION_NO_ANSWER = 0L;

    public static final String DEFAULT_FEEDBACK_TEXT = "No feedback from user";
}
