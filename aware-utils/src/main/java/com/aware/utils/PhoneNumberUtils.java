package com.aware.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.replaceEach;

/**
 * @author Oleg Artomov
 */
public final class PhoneNumberUtils {

    private static final Pattern PHONE_NUMBER_REGEX = Pattern.compile("^\\+(?:[0-9]●?){6,14}[0-9]$");

    private static final String[] SEARCH_LIST = new String[]{" ", "(", ")", "-"};

    private static final String[] REPLACEMENT_LIST = new String[]{"", "", "", ""};

    private PhoneNumberUtils() {

    }

    public static boolean validatePhoneNumber(String phoneNumber) {
        if (isEmpty(phoneNumber)) {
            return false;
        }
        Matcher matcher = PHONE_NUMBER_REGEX.matcher(phoneNumber);
        return matcher.matches();
    }

    public static String normalize(String phoneNumber) {
        if (isEmpty(phoneNumber)) {
            return phoneNumber;
        }
        return replaceEach(phoneNumber, SEARCH_LIST, REPLACEMENT_LIST);
    }
}
