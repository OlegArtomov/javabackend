package com.aware.utils;

import org.slf4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.Properties;

import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.util.Assert.isTrue;

/**
 * @author Oleg Artomov
 */
public final class PropertiesUtils {

    private static final Logger logger = getLogger(PropertiesUtils.class);

    private PropertiesUtils() {
        // Namespace
    }

    public static Properties loadProperties(ResourceLoader resourceLoader, String defaultFileName, String customFileName) {
        try {
            Properties result = getProperties(resourceLoader, true, defaultFileName);
            Properties externalProperties = getProperties(resourceLoader, false, customFileName);
            if (externalProperties != null) {
                for (String currentPropertyName : externalProperties.stringPropertyNames()) {
                    result.setProperty(currentPropertyName, externalProperties.getProperty(currentPropertyName));
                }
            }
            return result;
        } catch (IOException e) {
            logger.error("Error during load properties", e);
            throw new RuntimeException(e);
        }
    }

    public static @Nullable
    Properties getProperties(ResourceLoader resourceLoader, boolean required, String fileName) throws IOException {
        Resource propertiesFromFile = resourceLoader.getResource(fileName);
        if (required) {
            isTrue(propertiesFromFile.exists(), "Properties file " + fileName + " is not found");
        }
        if (propertiesFromFile.exists()) {
            Properties result = new Properties();
            result.load(propertiesFromFile.getInputStream());
            return result;
        }
        return null;
    }
}
