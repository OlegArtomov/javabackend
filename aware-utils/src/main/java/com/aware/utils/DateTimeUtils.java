package com.aware.utils;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static java.time.Clock.systemDefaultZone;
import static java.time.ZoneOffset.UTC;

public final class DateTimeUtils {

    public static final DateTimeFormatter ISO_DATETIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZZ");

    private static Clock clock = systemDefaultZone();

    private DateTimeUtils() {
        // Namespace
    }

    public static Date iso8601StringToUtcDate(final String dateAsString) {
        return Date.from(LocalDateTime.parse(dateAsString, ISO_DATETIME_FORMATTER).toInstant(UTC));
    }

    public static String dateToIso8601UtcString(final Date date) {
        return ISO_DATETIME_FORMATTER.withZone(UTC).format(date.toInstant());
    }

    public static Date now() {
        return new Date(clock.millis());
    }

    public static void setClock(Clock customClock){
        clock = customClock;
    }

    public static void resetClock(){
        clock = systemDefaultZone();
    }

}
