package com.aware.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import static com.aware.utils.AwareConstants.AWARE_CONFIG_DIR_SYSTEM_PROPERTY_NAME;
import static com.aware.utils.AwareConstants.AWARE_DEFAULTS_PROPERTIES_FILE;
import static com.aware.utils.AwareConstants.AWARE_PROPERTIES_FILE;
import static org.springframework.util.ResourceUtils.CLASSPATH_URL_PREFIX;
import static org.springframework.util.ResourceUtils.FILE_URL_PREFIX;

/**
 * @author Oleg Artomov
 */
@PropertySources(value = {
        //OAR, The property sources will be added in Environment in reverse order !!!!
        @PropertySource(value = CLASSPATH_URL_PREFIX + AWARE_DEFAULTS_PROPERTIES_FILE),
        @PropertySource(value = FILE_URL_PREFIX + "${" + AWARE_CONFIG_DIR_SYSTEM_PROPERTY_NAME
                + "}/" + AWARE_PROPERTIES_FILE, ignoreResourceNotFound = true)

})
public class AwareProperties {

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
