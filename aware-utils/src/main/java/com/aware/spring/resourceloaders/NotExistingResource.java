package com.aware.spring.resourceloaders;

import org.springframework.core.io.Resource;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;

/**
 * @author Oleg Artomov
 */
public final class NotExistingResource implements Resource {

    private static NotExistingResource instance = new NotExistingResource();

    private NotExistingResource() {
        // Singleton
    }

    public static Resource getInstance() {
        return NotExistingResource.instance;
    }

    @Override
    public boolean exists() {
        return false;
    }

    @Override
    public boolean isReadable() {
        return false;
    }

    @Override
    public boolean isOpen() {
        return false;
    }

    @Override
    public URL getURL() throws IOException {
        throw new IOException("Cannot get URL of a non-existing resource");
    }

    @Override
    public URI getURI() throws IOException {
        throw new IOException("Cannot get URI of a non-existing resource");
    }

    @Override
    public File getFile() throws IOException {
        throw new IOException("Cannot get a non-existing resource as a File");
    }

    @Override
    public long contentLength() throws IOException {
        throw new IOException("Cannot get content length of a non-existing resource");
    }

    @Override
    public long lastModified() throws IOException {
        throw new IOException("Cannot get last modified timestamp of a non-existing resource");
    }

    @Override
    public Resource createRelative(final String relativePath) throws IOException {
        throw new IOException("Cannot resolve a relative resource path based on a non-existing resource");
    }

    @Override public @Nullable String getFilename() {
        return null;
    }

    @Override
    public String getDescription() {
        return toString();
    }

    @Override
    public InputStream getInputStream() throws IOException {
        throw new IOException("Cannot get input stream of a non-existing resource");
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }

}
