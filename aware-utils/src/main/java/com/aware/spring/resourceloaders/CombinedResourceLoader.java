package com.aware.spring.resourceloaders;

import org.slf4j.Logger;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.slf4j.LoggerFactory.getLogger;

public class CombinedResourceLoader extends DefaultResourceLoader {

    private static final Logger logger = getLogger(CombinedResourceLoader.class);

    private final ResourceLoader[] resourceLoaders;

    public CombinedResourceLoader(final ResourceLoader... resourceLoaders) {
        if (isEmpty(resourceLoaders)) {
            throw new IllegalArgumentException(String.format("%s requires at least one child resource loader", CombinedResourceLoader.class.getSimpleName()));
        }

        this.resourceLoaders = resourceLoaders;
    }

    @Override
    public Resource getResource(final String location) {
        for (final ResourceLoader loader : resourceLoaders) {
            final Resource resource = loader.getResource(location);
            if (resource.exists()) {
                logger.debug("Resource `{}` was found by loader `{}`", location, loader.getClass().getCanonicalName());
                return resource;
            }
        }

        logger.debug("Resource `{}` was not found", location);
        return NotExistingResource.getInstance();
    }

}
