package com.aware.spring.resourceloaders;

import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.FileSystemResourceLoader;
import org.springframework.core.io.Resource;

import java.io.File;

public class BaseDirResourceLoader extends FileSystemResourceLoader {

    private final String baseDir;

    public BaseDirResourceLoader(final String baseDir) {
        this.baseDir = baseDir;
    }

    @Override
    protected Resource getResourceByPath(final String path) {
        return new FileSystemResource(new File(baseDir, path));
    }

}
