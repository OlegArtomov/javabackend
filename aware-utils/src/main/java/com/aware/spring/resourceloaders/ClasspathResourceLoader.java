package com.aware.spring.resourceloaders;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;

/**
 * @author Oleg Artomov
 */
public class ClasspathResourceLoader extends DefaultResourceLoader {
    public Resource getResource(String location) {
        return new ClassPathResource(location);
    }
}
