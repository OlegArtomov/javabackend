package com.aware.utils;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * @author Oleg Artomov
 */
public class StringUtilsTest {

    @Test
    public void testSplitWithCommaSeparatorAndTrim(){
        String s = "s, 1, 2";
        List<String> strings = StringUtils.splitWithCommaSeparatorAndTrim(s);
        assertThat(strings.get(0), equalTo("s"));
        assertThat(strings.get(1), equalTo("1"));
    }

    @Test
    public void testSplitWithCommaSeparatorAndTrimWHenStringIsEmpty(){
        String s = "";
        List<String> strings = StringUtils.splitWithCommaSeparatorAndTrim(s);
        assertThat(strings, hasSize(1));
    }
}