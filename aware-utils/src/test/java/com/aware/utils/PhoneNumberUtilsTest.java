package com.aware.utils;

import org.junit.Test;

import static com.aware.utils.PhoneNumberUtils.normalize;
import static com.aware.utils.PhoneNumberUtils.validatePhoneNumber;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Oleg Artomov
 */
public class PhoneNumberUtilsTest {

    @Test
    public void testValidatePhoneNumber() throws Exception {
        assertFalse(validatePhoneNumber("+1"));
        assertTrue(validatePhoneNumber("+380676222808"));
        assertFalse(validatePhoneNumber(null));
        assertFalse(validatePhoneNumber("AAAA"));
    }

    @Test
    public void testNormalizePhoneNumber(){
        assertThat(normalize("+38 (063) 517-68-30"), equalTo("+380635176830"));
    }
}