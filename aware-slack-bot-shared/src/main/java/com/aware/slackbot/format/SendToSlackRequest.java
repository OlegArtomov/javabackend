package com.aware.slackbot.format;

/**
 * @author Oleg Artomov
 */
public class SendToSlackRequest {
    private Long sessionId;

    private String channelId;

    private String message;

    public Long getSessionId() {
        return sessionId;
    }

    public String getChannelId() {
        return channelId;
    }

    public String getMessage() {
        return message;
    }

    public static class Builder{
        private Long sessionId;

        private String channelId;

        private String message;

        public Builder sessionId(Long sessionId){
            this.sessionId = sessionId;
            return this;
        }

        public Builder channelId(String channelId){
            this.channelId = channelId;
            return this;
        }


        public Builder message(String message){
            this.message = message;
            return this;
        }

        public SendToSlackRequest build(){
            SendToSlackRequest result = new SendToSlackRequest();
            result.sessionId = sessionId;
            result.message = message;
            result.channelId = channelId;
            return result;
        }
    }
}
