package com.aware.slackbot.format;

/**
 * @author Oleg Artomov
 */
public class RegisterBotRequest {
    private String code;

    public String getCode() {
        return code;
    }

    public static class Builder {
        private String code;

        public Builder code(String value) {
            this.code = value;
            return this;
        }

        public RegisterBotRequest build() {
            RegisterBotRequest request = new RegisterBotRequest();
            request.code = code;
            return request;
        }
    }
}
