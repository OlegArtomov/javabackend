package com.aware.slackbot.config;

import org.springframework.beans.factory.annotation.Value;

/**
 * @author Oleg Artomov
 */
public class RedisConfiguration {
    @Value("${redis.hostName}")
    private String redisHostName;

    @Value("${redis.port}")
    private int redisPort;

    @Value("${redis.slackChannel}")
    private String redisSlackChannel;

    @Value("${redis.registerBotChannel}")
    private String redisRegisterBotChannel;

    public String getRedisHostName() {
        return redisHostName;
    }

    public int getRedisPort() {
        return redisPort;
    }

    public String getRedisSlackChannel() {
        return redisSlackChannel;
    }

    public String getRedisRegisterBotChannel() {
        return redisRegisterBotChannel;
    }
}
