package com.aware.slackbot.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

import static com.fasterxml.jackson.databind.DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT;
import static com.fasterxml.jackson.databind.DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_INVALID_SUBTYPE;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_NUMBERS_FOR_ENUMS;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_READING_DUP_TREE_KEY;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL;
import static com.fasterxml.jackson.databind.DeserializationFeature.UNWRAP_ROOT_VALUE;
import static com.fasterxml.jackson.databind.DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS;
import static com.fasterxml.jackson.databind.DeserializationFeature.WRAP_EXCEPTIONS;
import static com.fasterxml.jackson.databind.PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES;

/**
 * @author Oleg Artomov
 */
@PropertySources(value = {
        @PropertySource(value = "classpath:slackBotSharedDefaults.properties")
})
@Configuration
public class SlackBotSharedConfig {

    @Bean
    public ObjectMapper redisRequestObjectMapper() {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(
                FAIL_ON_UNKNOWN_PROPERTIES,
                ACCEPT_SINGLE_VALUE_AS_ARRAY,
                UNWRAP_SINGLE_VALUE_ARRAYS,
                UNWRAP_ROOT_VALUE,
                ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,
                READ_UNKNOWN_ENUM_VALUES_AS_NULL
        );
        objectMapper.enable(
                FAIL_ON_NULL_FOR_PRIMITIVES,
                FAIL_ON_NUMBERS_FOR_ENUMS,
                FAIL_ON_INVALID_SUBTYPE,
                FAIL_ON_READING_DUP_TREE_KEY,
                FAIL_ON_IGNORED_PROPERTIES,
                WRAP_EXCEPTIONS
        );
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.setPropertyNamingStrategy(CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
        return objectMapper;
    }

    @Bean
    public JedisConnectionFactory jedisConnectionFactory(RedisConfiguration awareConfiguration) {
        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        jedisConnectionFactory.setUsePool(true);
        jedisConnectionFactory.setHostName(awareConfiguration.getRedisHostName());
        jedisConnectionFactory.setPort(awareConfiguration.getRedisPort());
        return jedisConnectionFactory;
    }

    @Bean
    public RedisConfiguration redisConfiguration() {
        return new RedisConfiguration();
    }
}
