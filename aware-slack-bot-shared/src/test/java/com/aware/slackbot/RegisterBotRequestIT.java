package com.aware.slackbot;

import com.aware.slackbot.config.SlackBotSharedConfig;
import com.aware.slackbot.format.RegisterBotRequest;
import com.aware.slackbot.format.SendToSlackRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.io.IOException;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Oleg Artomov
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SlackBotSharedConfig.class, RegisterBotTestConfiguration.class})
public class RegisterBotRequestIT {

    @Resource
    private ObjectMapper objectMapper;

    @Test
    public void testForRegisterBotRequest() throws IOException {
        String value = "{\"code\":\"212\"}";
        RegisterBotRequest request = objectMapper.readValue(value, RegisterBotRequest.class);
        assertThat(request.getCode(), equalTo("212"));
    }

    @Test
    public void testForSendToSlackBotRequest() throws IOException {
        String value = "{\"session_id\":1, \"channel_id\":\"212\", \"message\":\"Test\"}";
        SendToSlackRequest request = objectMapper.readValue(value, SendToSlackRequest.class);
        assertThat(request.getSessionId(), equalTo(1L));
        assertThat(request.getChannelId(), equalTo("212"));
        assertThat(request.getMessage(), equalTo("Test"));
    }
}
