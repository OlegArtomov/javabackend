package com.aware.slackbot.service.flow;

import com.aware.services.converstation.ConversationEvent;
import com.aware.services.converstation.ConversationUser;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;

import static com.aware.slackbot.service.flow.SlackStepsConstants.FIFTH_STEP_ID;
import static com.aware.slackbot.service.flow.SlackStepsConstants.FIRST_STEP_ID;
import static com.aware.slackbot.service.flow.SlackStepsConstants.SECOND_STEP_ID;
import static com.aware.slackbot.service.flow.SlackStepsConstants.SIXTH_STEP_ID;
import static com.aware.slackbot.service.flow.SlackStepsConstants.THIRD_STEP_ID;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.emptyList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

/**
 * @author Oleg Artomov
 */
public class ConversationStateTest {

    private ConversationUser slackUser;

    private ConversationEvent slackMessagePosted;

    private ConversationState conversationState;

    private static final String POSITIVE_ANSWER = "YES";

    private static final String NEGATIVE_ANSWER = "NO";

    private static final Collection<String> POSITIVE_ANSWERS = newArrayList(POSITIVE_ANSWER);

    private static final Collection<String> NEGATIVE_ANSWERS = newArrayList(NEGATIVE_ANSWER);


    @Before
    public void beforeEachTest() {
        slackUser = new ConversationUser("11", null, null, null, null);
        slackMessagePosted = new ConversationEvent("11");
        StepListener stepListener = new StepListener() {
            @Override
            public void onMakeCurrent(ConversationEvent event, ConversationUser slackUser) {
                conversationState.finish(slackUser);
            }

            @Override
            public void onAcceptAnswer(ConversationEvent event, ConversationUser slackUser, Integer duration) {

            }
        };

        StepListener meditationListener = new StepListener() {
            @Override
            public void onMakeCurrent(ConversationEvent event, ConversationUser slackUser) {

            }

            @Override
            public void onAcceptAnswer(ConversationEvent event, ConversationUser slackUser, Integer duration) {
                conversationState.setDurationForUser(slackUser, duration);
            }
        };
        FlowStep sixth = new SimpleStep(null, null, emptyList(), emptyList(), SIXTH_STEP_ID, stepListener);
        FlowStep fifth = new SimpleStep(null, null, emptyList(), emptyList(), FIFTH_STEP_ID, stepListener);
        FlowStep thirdStep = new SimpleStep(sixth, fifth, POSITIVE_ANSWERS, NEGATIVE_ANSWERS, THIRD_STEP_ID, null);
        FlowStep secondStep = new CustomFlowStep(thirdStep, null, SECOND_STEP_ID, meditationListener);
        FlowStep firstStep = new SimpleStep(secondStep, fifth, POSITIVE_ANSWERS, NEGATIVE_ANSWERS, FIRST_STEP_ID, null);
        conversationState = new ConversationState(firstStep);
    }

    @Test
    public void testSimpleExit() {
        conversationState.start(slackUser);
        multipleAnswer(slackUser, NEGATIVE_ANSWER);
        assertFalse(conversationState.isConversationStarted(slackUser));
    }

    @Test
    public void testValidateSecondStep() {
        conversationState.start(slackUser);
        multipleAnswer(slackUser, POSITIVE_ANSWER, "20");
        validateCurrentStep(slackUser, THIRD_STEP_ID);
        assertThat(conversationState.conversationState(slackUser).getDuration(), equalTo(20));
    }

    @Test
    public void testValidateWhenAnswerOnSecondStepIsNotNumber() {
        conversationState.start(slackUser);
        multipleAnswer(slackUser, POSITIVE_ANSWER, "dsada");
        validateCurrentStep(slackUser, SECOND_STEP_ID);
    }

    private void validateCurrentStep(ConversationUser slackUser, String stepId) {
        FlowStep slackStep = conversationState.currentStep(slackUser);
        if (slackStep != null) {
            assertThat(slackStep.getStepId(), equalTo(stepId));
        } else {
            fail("Current step is null");
        }
    }

    private void multipleAnswer(ConversationUser slackUser, String... answers) {
        for (String currentAnswer : answers) {
            conversationState.answer(slackMessagePosted, slackUser, currentAnswer);
        }
    }

}