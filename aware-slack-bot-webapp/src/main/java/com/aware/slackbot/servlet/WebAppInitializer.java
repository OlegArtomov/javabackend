package com.aware.slackbot.servlet;

import com.aware.slackbot.config.SlackAppConfig;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import static com.aware.slackbot.utils.SlackBotConstants.LOG4J_CONFIG_SLACK_BOT;
import static com.aware.utils.AwareConstants.AWARE_CONFIG_DIR_SYSTEM_PROPERTY_NAME;
import static java.io.File.separator;
import static org.apache.log4j.xml.DOMConfigurator.configure;
import static org.springframework.beans.factory.config.PlaceholderConfigurerSupport.DEFAULT_PLACEHOLDER_PREFIX;
import static org.springframework.beans.factory.config.PlaceholderConfigurerSupport.DEFAULT_PLACEHOLDER_SUFFIX;
import static org.springframework.web.util.ServletContextPropertyUtils.resolvePlaceholders;

/**
 * @author Oleg Artomov
 */
public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[]{SlackAppConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return null;
    }

    @Override
    protected String[] getServletMappings() {
        return null;
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        String path = DEFAULT_PLACEHOLDER_PREFIX +
                AWARE_CONFIG_DIR_SYSTEM_PROPERTY_NAME + DEFAULT_PLACEHOLDER_SUFFIX + separator + LOG4J_CONFIG_SLACK_BOT;
        configure(resolvePlaceholders(path, servletContext));
        super.onStartup(servletContext);
    }
}
