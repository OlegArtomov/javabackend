package com.aware.slackbot.config;

import com.aware.config.AwareDaoConfiguration;
import com.aware.config.AwareProperties;
import com.aware.services.config.AwareServicesConfiguration;
import com.aware.slackbot.service.core.RedisMessageListener;
import com.aware.slackbot.format.RegisterBotRequest;
import com.aware.slackbot.format.SendToSlackRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.stereotype.Service;

import static com.aware.slackbot.service.core.RedisMessageListener.REGISTER_BOT_METHOD_NAME;
import static com.aware.slackbot.service.core.RedisMessageListener.SEND_FROM_SLACK_METHOD_NAME;
import static com.aware.utils.AwareConstants.AWARE_BASE_PACKAGE;

/**
 * @author Oleg Artomov
 */
@Configuration
@ComponentScan(
        basePackages = AWARE_BASE_PACKAGE,
        useDefaultFilters = false,
        includeFilters = {
                @ComponentScan.Filter({
                        Service.class
                })
        }
)
@Import({AwareServicesConfiguration.class, AwareDaoConfiguration.class,
        SlackBotSharedConfig.class, AwareProperties.class})
//TODO: Important: AwareProperties must be last
public class SlackAppConfig {

    @Bean
    public MessageListenerAdapter sendToSlackListenerAdaptor(RedisMessageListener receiver,
                                                             ObjectMapper redisRequestObjectMapper) {
        MessageListenerAdapter listenerAdapter = new MessageListenerAdapter(receiver, SEND_FROM_SLACK_METHOD_NAME);
        Jackson2JsonRedisSerializer<SendToSlackRequest> serializer = new Jackson2JsonRedisSerializer<>(SendToSlackRequest.class);
        serializer.setObjectMapper(redisRequestObjectMapper);
        listenerAdapter.setSerializer(serializer);
        return listenerAdapter;
    }

    @Bean
    public MessageListenerAdapter registerBotListenerAdaptor(RedisMessageListener receiver,
                                                             ObjectMapper redisRequestObjectMapper) {
        MessageListenerAdapter listenerAdapter = new MessageListenerAdapter(receiver, REGISTER_BOT_METHOD_NAME);
        Jackson2JsonRedisSerializer<RegisterBotRequest> serializer = new Jackson2JsonRedisSerializer<>(RegisterBotRequest.class);
        serializer.setObjectMapper(redisRequestObjectMapper);
        listenerAdapter.setSerializer(serializer);
        return listenerAdapter;
    }

    @Bean
    public RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory,
                                                   MessageListenerAdapter sendToSlackListenerAdaptor,
                                                   MessageListenerAdapter registerBotListenerAdaptor,
                                                   RedisConfiguration redisConfiguration) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.addMessageListener(sendToSlackListenerAdaptor, new PatternTopic(redisConfiguration.getRedisSlackChannel()));
        container.addMessageListener(registerBotListenerAdaptor, new PatternTopic(redisConfiguration.getRedisRegisterBotChannel()));
        return container;
    }
}
