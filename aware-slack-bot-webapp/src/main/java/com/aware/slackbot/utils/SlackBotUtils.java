package com.aware.slackbot.utils;

import com.aware.services.converstation.ConversationEvent;
import com.aware.services.converstation.ConversationUser;
import com.aware.slack.SlackChannel;
import com.aware.slack.SlackUser;
import com.aware.slack.events.SlackMessagePosted;

/**
 * @author Oleg Artomov
 */
public final class SlackBotUtils {
    private SlackBotUtils() {

    }

    public static ConversationUser fromSlackUser(SlackUser slackUser) {
        return new ConversationUser(slackUser.getId(), slackUser.getUserMail(), slackUser.getFirstName(), slackUser.getLastName(), slackUser.getPhone());
    }

    public static ConversationEvent fromSlackEvent(SlackMessagePosted event) {
        return new ConversationEvent(event.getChannel().getId());
    }

    public static ConversationEvent fromSlackChannel(SlackChannel slackChannel) {
        return new ConversationEvent(slackChannel.getId());
    }
}
