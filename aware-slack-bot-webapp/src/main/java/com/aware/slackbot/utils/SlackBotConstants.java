package com.aware.slackbot.utils;

/**
 * @author Oleg Artomov
 */
public final class SlackBotConstants {

    private SlackBotConstants() {

    }

    public static final String LOG4J_CONFIG_SLACK_BOT = "log4jSlackBot.xml";

    public static final String STANDARD_SLACKBOT_NAME = "slackbot";
}
