package com.aware.slackbot.service.flow;

import com.aware.model.AwareUser;
import com.aware.services.UserService;
import com.aware.services.converstation.ConversationUser;
import com.aware.slack.SlackSession;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import static com.aware.slack.ChannelUtils.fromChannelId;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author Oleg Artomov
 */
@Service
public class SlackService implements BotService {

    private static final Logger logger = getLogger(SlackService.class);

    @Resource
    private UserService userService;

    public void sendAnswerFromSlack(SlackSession slackSession, String channelId, String resultMessage) {
        if (isNotEmpty(resultMessage)) {
            logger.debug("Result message {} is sent to channel {}", resultMessage, channelId);
            slackSession.sendMessage(fromChannelId(channelId), resultMessage);
        } else {
            logger.warn("Skip empty message for channel: {}", channelId);
        }
    }

    @Override
    public void reply(SlackSession slackSession, String sourceId, String message) {
        sendAnswerFromSlack(slackSession, sourceId, message);
    }

    @Override
    public AwareUser findUser(ConversationUser conversationUser) {
        return userService.loginUsingSlack(conversationUser, false);
    }
}
