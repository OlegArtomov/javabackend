package com.aware.slackbot.service.flow;

import com.aware.model.AwareUser;
import com.aware.model.Meditation;
import com.aware.model.UserSubscription;
import com.aware.services.MeditationService;
import com.aware.services.config.AwareConfiguration;
import com.aware.services.notification.NotificationService;
import com.aware.services.security.JwtService;
import com.aware.services.converstation.ConversationEvent;
import com.aware.services.converstation.ConversationUser;
import com.aware.services.utils.UrlShortener;
import com.aware.slack.SlackSession;
import org.slf4j.Logger;
import org.springframework.core.env.Environment;

import static com.aware.model.SubscriptionType.REGULAR;
import static com.aware.slackbot.service.flow.SlackStepsConstants.FIFTH_STEP_ID;
import static com.aware.slackbot.service.flow.SlackStepsConstants.FIRST_STEP_ID;
import static com.aware.slackbot.service.flow.SlackStepsConstants.SECOND_STEP_ID;
import static com.aware.slackbot.service.flow.SlackStepsConstants.SIXTH_STEP_ID;
import static com.aware.slackbot.service.flow.SlackStepsConstants.THIRD_STEP_ID;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.util.StringUtils.isEmpty;

/**
 * @author Oleg Artomov
 */
public class SlackFlowService {
    private static final Logger logger = getLogger(SlackFlowService.class);

    private final AwareConfiguration awareConfiguration;

    private final MeditationService meditationService;

    private final JwtService jwtService;

    private final Environment environment;

    private final UrlShortener urlShortener;

    private final NotificationService notificationService;

    private final SlackSession slackSession;

    private final BotService botService;

    private ConversationState conversationState;

    public SlackFlowService(AwareConfiguration awareConfiguration,
                            MeditationService meditationService, JwtService jwtService, Environment environment,
                            UrlShortener urlShortener, NotificationService notificationService,
                            BotService botService, SlackSession slackSession) {
        this.awareConfiguration = awareConfiguration;
        this.meditationService = meditationService;
        this.jwtService = jwtService;
        this.environment = environment;
        this.urlShortener = urlShortener;
        this.notificationService = notificationService;
        this.botService = botService;
        this.slackSession = slackSession;
        afterConstruction();
    }

    public void processMessage(String originalMessage, ConversationUser conversationUser, ConversationEvent conversationEvent) {
        if (!isEmpty(originalMessage))
            logger.debug("Original message: {}", originalMessage);
        if (checkIsStartDialog(originalMessage, conversationUser)) {
            conversationState.start(conversationUser);
            botService.reply(slackSession, conversationEvent.getSourceId(), getMessageForCurrentState(conversationUser));
        } else {
            if (conversationState.isConversationStarted(conversationUser)) {
                boolean isMovedToNextState = conversationState.answer(conversationEvent, conversationUser, originalMessage);
                if (isMovedToNextState) {
                    if (conversationState.isConversationStarted(conversationUser)) {
                        botService.reply(slackSession, conversationEvent.getSourceId(), getMessageForCurrentState(conversationUser));
                    } else {
                        logger.debug("Conversation is finished for user: {}. No answer will be send in response", conversationUser.getUserEmail());
                    }
                } else {
                    String message = awareConfiguration.getSlackUnknownCommand() + getMessageForCurrentState(conversationUser);
                    botService.reply(slackSession, conversationEvent.getSourceId(), message);
                }
            }
        }
    }

    private boolean checkIsStartDialog(String originalMessage, ConversationUser conversationUser) {
        boolean isStartMessage = awareConfiguration.getSlackStartDialog().stream().anyMatch(message -> message.equalsIgnoreCase(originalMessage));
        return isStartMessage || !conversationState.isConversationStarted(conversationUser);
    }

    private String getMessageForCurrentState(ConversationUser conversationUser) {
        FlowStep step = conversationState.currentStep(conversationUser);
        if (step != null) {
            return environment.getProperty("slack.flow.step" + step.getStepId() + ".message");
        }
        return null;
    }

    private String createMeditationForUser(ConversationUser conversationUser, Integer duration, String channelId) {
        AwareUser awareUser = botService.findUser(conversationUser);
        String accessToken = jwtService.createForUser(awareUser);
        Meditation meditation = meditationService.createForUser(awareUser, duration);
        return urlShortener.makeShort(String.format(awareConfiguration.getPaymentPageUrl(),
                accessToken, meditation.getId(), channelId, slackSession.getSessionId()));
    }

    private void afterConstruction() {
        StepListener sixthListener = new StepListener() {
            @Override
            public void onMakeCurrent(ConversationEvent event, ConversationUser conversationUser) {
                StateHolder stateHolder = conversationState.conversationState(conversationUser);
                if (stateHolder != null) {
                    String url = createMeditationForUser(conversationUser,
                            stateHolder.getDuration(),
                            event.getSourceId());
                    String resultMessage = String.format(awareConfiguration.getSlackCreateMeditationCommandAnswer(),
                            url);
                    botService.reply(slackSession, event.getSourceId(), resultMessage);
                }
                conversationState.finish(conversationUser);
            }

            @Override
            public void onAcceptAnswer(ConversationEvent event, ConversationUser conversationUser, Integer duration) {

            }
        };

        StepListener fifthListener = new StepListener() {
            @Override
            public void onMakeCurrent(ConversationEvent event, ConversationUser conversationUser) {
                botService.reply(slackSession, event.getSourceId(), getMessageForCurrentState(conversationUser));
                conversationState.finish(conversationUser);
            }

            @Override
            public void onAcceptAnswer(ConversationEvent event, ConversationUser conversationUser, Integer duration) {

            }
        };

        StepListener meditationListener = new StepListener() {
            @Override
            public void onMakeCurrent(ConversationEvent event, ConversationUser conversationUser) {

            }

            @Override
            public void onAcceptAnswer(ConversationEvent event, ConversationUser conversationUser, Integer duration) {
                conversationState.setDurationForUser(conversationUser, duration);
                AwareUser awareUser = botService.findUser(conversationUser);
                if (awareUser != null) {
                    UserSubscription userSubscription = awareUser.getCurrentSubscription();
                    if ((userSubscription != null) && (REGULAR.equals(userSubscription.getSubscription().getType()))) {
                        logger.debug("Start create meditation for subscribed user: {}", awareUser.getId());
                        Meditation meditation = meditationService.createForUser(awareUser, duration);
                        notificationService.notifyTeachersByEmailAboutNewMeditation(awareUser, meditation,
                                event.getSourceId(), slackSession.getSessionId());
                        notificationService.notifyTeachersBySMSAboutNewMeditation(awareUser);
                        botService.reply(slackSession, event.getSourceId(), awareConfiguration.getSlackSuccessPaymentAnswer());
                        conversationState.finish(conversationUser);
                    }
                }
            }
        };
        FlowStep sixth = new SimpleStep(null, null, null, null, SIXTH_STEP_ID, sixthListener);
        FlowStep fifth = new SimpleStep(null, null, null, null, FIFTH_STEP_ID, fifthListener);
        FlowStep thirdStep = new SimpleStep(sixth, fifth, awareConfiguration.getSlackPositiveAnswers(),
                awareConfiguration.getSlackNegativeAnswer(), THIRD_STEP_ID, null);
        FlowStep secondStep = new CustomFlowStep(thirdStep, null, SECOND_STEP_ID, meditationListener);
        FlowStep firstStep = new SimpleStep(secondStep, fifth, awareConfiguration.getSlackPositiveAnswers(),
                awareConfiguration.getSlackNegativeAnswer(), FIRST_STEP_ID, null);
        conversationState = new ConversationState(firstStep);
    }

    public SlackSession getSlackSession() {
        return slackSession;
    }
}
