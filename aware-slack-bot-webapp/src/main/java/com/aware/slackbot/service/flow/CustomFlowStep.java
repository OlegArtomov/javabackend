package com.aware.slackbot.service.flow;

import com.aware.services.converstation.ConversationEvent;
import com.aware.services.converstation.ConversationUser;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author Oleg Artomov
 */
public class CustomFlowStep extends FlowStep {

    private static final Logger logger = getLogger(CustomFlowStep.class);

    public CustomFlowStep(FlowStep positiveStep, FlowStep negativeStep,
                          String stepId, StepListener stepListener) {
        super(positiveStep, negativeStep, stepId, stepListener);
    }

    @Override
    public Pair<Boolean, FlowStep> accept(ConversationEvent event, ConversationUser conversationUser, String answer) {
        try {
            int duration = Integer.parseInt(answer);
            if (getStepListener() != null) {
                logger.debug("Accept answer with duration: {}", duration);
                getStepListener().onAcceptAnswer(event, conversationUser, duration);
            }
            return Pair.of(true, getPositiveStep());
        } catch (NumberFormatException e) {
            logger.error("Error during parsing}", e);
        }

        return MISSED_NEXT_STEP;
    }
}
