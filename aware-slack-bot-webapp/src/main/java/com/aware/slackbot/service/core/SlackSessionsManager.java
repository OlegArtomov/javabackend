package com.aware.slackbot.service.core;

import com.aware.model.SlackBotInfo;
import com.aware.repositories.SlackBotInfoRepository;
import com.aware.services.MeditationService;
import com.aware.services.config.AwareConfiguration;
import com.aware.services.converstation.ConversationEvent;
import com.aware.services.converstation.ConversationUser;
import com.aware.services.notification.NotificationService;
import com.aware.services.security.JwtService;
import com.aware.services.utils.UrlShortener;
import com.aware.slack.SlackMessageHandle;
import com.aware.slack.SlackSession;
import com.aware.slack.SlackUser;
import com.aware.slack.replies.SlackChannelReply;
import com.aware.slackbot.service.events.AwareSlackChannelJoinedListener;
import com.aware.slackbot.service.events.AwareSlackMessageListener;
import com.aware.slackbot.service.flow.SlackFlowService;
import com.aware.slackbot.service.flow.SlackService;
import org.slf4j.Logger;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static com.aware.slack.SlackErrorCode.ACCOUNT_INACTIVE_ERROR_CODE;
import static com.aware.slack.SlackSession.SLACK_API_HTTPS_ROOT;
import static com.aware.slack.impl.SlackSessionFactory.createWebSocketSlackSession;
import static com.aware.slackbot.utils.SlackBotConstants.STANDARD_SLACKBOT_NAME;
import static com.aware.slackbot.utils.SlackBotUtils.fromSlackChannel;
import static com.aware.slackbot.utils.SlackBotUtils.fromSlackUser;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

/**
 * @author Oleg Artomov
 */
@Service
public class SlackSessionsManager {

    private static final Logger logger = getLogger(SlackSessionsManager.class);

    @Resource
    private SlackBotInfoRepository slackBotInfoRepository;

    @Resource
    private AwareConfiguration awareConfiguration;

    @Resource
    private SlackService slackService;

    @Resource
    private MeditationService meditationService;

    @Resource
    private JwtService jwtService;

    @Resource
    private Environment environment;

    @Resource
    private UrlShortener urlShortener;

    @Resource
    private NotificationService notificationService;

    private Map<Long, SlackSession> sessions = new HashMap<>();

    @PostConstruct
    private void afterConstruction() {
        logger.debug("Start register bots");
        slackBotInfoRepository.findAll().forEach(this::registerNewBot);
        logger.debug("Bots count: {}", sessions.size());
    }

    private
    @Nullable
    SlackFlowService registerNewBot(SlackBotInfo slackBotInfo) {
        try {
            SlackSession session = createWebSocketSlackSession(slackBotInfo.getBotToken(), slackBotInfo.getId(),
                    (slackSession, errorCode) -> {
                        if (ACCOUNT_INACTIVE_ERROR_CODE.equals(errorCode)) {
                            unregisterSession(slackSession);
                        } else
                            logger.warn("We skip error code: {}", errorCode);
                    });
            SlackFlowService slackFlowService = new SlackFlowService(awareConfiguration,
                    meditationService, jwtService, environment, urlShortener,
                    notificationService, slackService, session);
            session.addMessagePostedListener(new AwareSlackMessageListener(slackFlowService,
                    slackBotInfo.getBotId()));
            session.addchannelJoinedListener(new AwareSlackChannelJoinedListener(slackService, awareConfiguration));
            session.connect();
            logger.info("Slack session created. Id {}, token {}", slackBotInfo.getId(), slackBotInfo.getBotToken());
            registerSession(slackBotInfo.getId(), session);
            return slackFlowService;
        } catch (IOException e) {
            logger.error("Error during connect to slack. Id {}", slackBotInfo.getId(), e);
            return null;
        }
    }

    public SlackSession getSlackSessionById(Long sessionId) {
        return sessions.get(sessionId);
    }

    public void registerSession(Long id, SlackSession slackSession) {
        sessions.put(id, slackSession);
        logger.debug("Sessions count: {}", sessions.size());
    }

    public void unregisterSession(SlackSession slackSession) {
        logger.debug("Unregister session: {}", slackSession.getSessionId());
        sessions.remove(slackSession.getSessionId());
        try {
            slackSession.disconnect();
            slackBotInfoRepository.delete(slackSession.getSessionId());
        } catch (IOException e) {
            logger.error("Error during disconnect session {}", slackSession.getSessionId(), e);
        }
        logger.debug("Sessions count: {}", sessions.size());
    }

    public void acceptClientCode(String code) {
        logger.debug("Accept client code {}", code);
        URI uri = fromHttpUrl(SLACK_API_HTTPS_ROOT + "oauth.access")
                .queryParam("client_id", awareConfiguration.getSlackAppClientId())
                .queryParam("client_secret", awareConfiguration.getSlackAppClientSecret())
                .queryParam("code", code)
                .build().encode().toUri();
        Map map = new RestTemplate().getForEntity(uri, Map.class).getBody();
        String teamId = (String) map.get("team_id");
        String teamName = (String) map.get("team_name");
        String accessToken = (String) map.get("access_token");
        String botAccessToken = null;
        String botId = null;
        Object bot = map.get("bot");
        if ((bot != null) && (bot instanceof Map)) {
            botAccessToken = (String) ((Map) bot).get("bot_access_token");
            botId = (String) ((Map) bot).get("bot_user_id");
        } else {
            logger.warn("Token is unavailable in response");
        }
        if (isNotEmpty(teamId) && isNotEmpty(botAccessToken)) {
            SlackBotInfo botInfo = slackBotInfoRepository.findByTeamId(teamId);
            if (botInfo == null) {
                SlackBotInfo slackBotInfo = new SlackBotInfo();
                slackBotInfo.setBotToken(botAccessToken);
                slackBotInfo.setTeamId(teamId);
                slackBotInfo.setTeamName(teamName);
                slackBotInfo.setAccessToken(accessToken);
                slackBotInfo.setBotId(botId);
                slackBotInfoRepository.save(slackBotInfo);
                SlackFlowService slackFlowService = registerNewBot(slackBotInfo);
                if (slackFlowService != null) {
                    notifyAllUsersInTeam(slackFlowService);
                }
            } else {
                logger.warn("Bot is already registered for team: {}", teamId);
            }
        }
    }

    private boolean isRealSlackUser(SlackUser slackUser) {
        return !STANDARD_SLACKBOT_NAME.equals(slackUser.getRealName()) && !slackUser.isDeleted() && !slackUser.isBot();
    }

    private void notifyAllUsersInTeam(SlackFlowService slackFlowService) {
        logger.debug("Start conversation with all users in team");
        SlackSession slackSession = slackFlowService.getSlackSession();
        Collection<SlackUser> users = slackSession.getUsers();
        users.stream().filter(this::isRealSlackUser).forEach(user -> {
            logger.debug("Notify user {} about join bot and start conversation.", user.getRealName());
            SlackMessageHandle<SlackChannelReply> slackChannel = slackSession.openDirectMessageChannel(user);
            ConversationUser  conversationUser = fromSlackUser(user);
            ConversationEvent event = fromSlackChannel(slackChannel.getReply().getSlackChannel());
            //We should send any message to start talk
            slackFlowService.processMessage(EMPTY, conversationUser, event);
        });
    }
}
