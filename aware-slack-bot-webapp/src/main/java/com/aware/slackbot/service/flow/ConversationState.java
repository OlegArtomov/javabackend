package com.aware.slackbot.service.flow;

import com.aware.services.converstation.ConversationEvent;
import com.aware.services.converstation.ConversationUser;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;

import java.util.Map;

import static com.google.common.collect.Maps.newConcurrentMap;
import static org.apache.commons.lang3.BooleanUtils.isTrue;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author Oleg Artomov
 */
public class ConversationState {

    private final FlowStep startStep;

    private static final Logger logger = getLogger(ConversationState.class);


    private Map<String, StateHolder> conversationState = newConcurrentMap();

    public ConversationState(FlowStep startStep) {
        this.startStep = startStep;
    }

    public boolean answer(ConversationEvent event, ConversationUser conversationUser, String answer) {
        String userId = conversationUser.getUserId();
        StateHolder stateHolder = conversationState.get(userId);
        if (stateHolder != null) {
            FlowStep slackStep = stateHolder.getCurrentStep();
            logger.debug("For user {} current step is {}", userId, slackStep.getStepId());
            Pair<Boolean, FlowStep> acceptResult = slackStep.accept(event, conversationUser, answer);
            boolean movedToNextState = isTrue(acceptResult.getKey());
            if (movedToNextState) {
                FlowStep step = acceptResult.getRight();
                if (step != null) {
                    stateHolder.setCurrentStep(step);
                    logger.debug("For user {} next step is {}", userId, step.getStepId());
                    StepListener listener = step.getStepListener();
                    if (listener != null) {
                        listener.onMakeCurrent(event, conversationUser);
                    }
                }
            }
            return movedToNextState;
        }
        return false;
    }

    public void start(ConversationUser conversationUser) {
        logger.debug("Start conversation for user: {}", conversationUser.getUserId());
        StateHolder stateHolder = new StateHolder();
        stateHolder.setCurrentStep(startStep);
        conversationState.put(conversationUser.getUserId(), stateHolder);
    }

    public void finish(ConversationUser conversationUser) {
        logger.debug("Finish conversation for user: {}", conversationUser.getUserId());
        conversationState.remove(conversationUser.getUserId());
    }

    public boolean isConversationStarted(ConversationUser conversationUser) {
        return conversationState.containsKey(conversationUser.getUserId());
    }

    public FlowStep currentStep(ConversationUser conversationUser) {
        StateHolder stateHolder = conversationState.get(conversationUser.getUserId());
        return stateHolder != null ? stateHolder.getCurrentStep() : null;
    }

    public void setDurationForUser(ConversationUser conversationUser, Integer duration){
        StateHolder stateHolder = conversationState.get(conversationUser.getUserId());
        if (stateHolder != null){
            stateHolder.setDuration(duration);
        }
    }

    public StateHolder conversationState(ConversationUser conversationUser){
        return conversationState.get(conversationUser.getUserId());
    }

}
