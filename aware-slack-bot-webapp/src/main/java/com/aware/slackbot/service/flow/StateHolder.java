package com.aware.slackbot.service.flow;

/**
 * @author Oleg Artomov
 */
public class StateHolder {
    private FlowStep currentStep;

    private Integer duration;

    public FlowStep getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(FlowStep currentStep) {
        this.currentStep = currentStep;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getDuration() {
        return duration;
    }
}
