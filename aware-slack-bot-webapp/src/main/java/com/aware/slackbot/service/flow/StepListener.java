package com.aware.slackbot.service.flow;

import com.aware.services.converstation.ConversationEvent;
import com.aware.services.converstation.ConversationUser;

/**
 * @author Oleg Artomov
 */
public interface StepListener {
    void onMakeCurrent(ConversationEvent event, ConversationUser conversationUser);

    void onAcceptAnswer(ConversationEvent event, ConversationUser conversationUser, Integer duration);
}
