package com.aware.slackbot.service.events;

import com.aware.services.config.AwareConfiguration;
import com.aware.slack.SlackSession;
import com.aware.slack.events.SlackChannelJoined;
import com.aware.slack.listeners.SlackChannelJoinedListener;
import com.aware.slackbot.service.flow.SlackService;
import org.slf4j.Logger;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author Oleg Artomov
 */
public class AwareSlackChannelJoinedListener implements SlackChannelJoinedListener {

    private static final Logger logger = getLogger(AwareSlackChannelJoinedListener.class);

    private final SlackService slackService;

    private final AwareConfiguration awareConfiguration;

    public AwareSlackChannelJoinedListener(SlackService slackService, AwareConfiguration awareConfiguration) {
        this.slackService = slackService;
        this.awareConfiguration = awareConfiguration;
    }

    @Override
    public void onEvent(SlackChannelJoined event, SlackSession session) {
        try {
            slackService.sendAnswerFromSlack(session, event.getSlackChannel().getId(), awareConfiguration.getSlackWelcomeCommand());
        } catch (Exception e) {
            //TODO: Fix me
            logger.error("Error", e);
        }
    }
}
