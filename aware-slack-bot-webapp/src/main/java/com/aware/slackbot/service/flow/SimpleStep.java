package com.aware.slackbot.service.flow;

import com.aware.services.converstation.ConversationEvent;
import com.aware.services.converstation.ConversationUser;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;

import static org.apache.commons.lang3.tuple.Pair.of;

/**
 * @author Oleg Artomov
 */
public class SimpleStep extends FlowStep {

    private final Collection<String> positiveAnswers;

    private final Collection<String> negativeAnswers;

    public SimpleStep(FlowStep positiveStep, FlowStep negativeStep,
                      Collection<String> positiveAnswers, Collection<String> negativeAnswers,
                      String stepId, StepListener stepListener) {
        super(positiveStep, negativeStep, stepId, stepListener);
        this.positiveAnswers = positiveAnswers;
        this.negativeAnswers = negativeAnswers;
    }

    @Override
    public Pair<Boolean, FlowStep> accept(ConversationEvent event, ConversationUser conversationUser, String answer) {
        if (positiveAnswers.stream().anyMatch(s -> s.equalsIgnoreCase(answer))){
            return of(true, getPositiveStep());
        }
        if (negativeAnswers.stream().anyMatch(s -> s.equalsIgnoreCase(answer))){
            return of(true, getNegativeStep());
        }
        return MISSED_NEXT_STEP;
    }
}
