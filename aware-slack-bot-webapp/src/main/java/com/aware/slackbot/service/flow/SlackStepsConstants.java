package com.aware.slackbot.service.flow;

/**
 * @author Oleg Artomov
 */
public class SlackStepsConstants {
    public static final String FIRST_STEP_ID = "1";

    public static final String SECOND_STEP_ID = "2";

    public static final String THIRD_STEP_ID = "3";

    public static final String FIFTH_STEP_ID = "5";

    public static final String SIXTH_STEP_ID = "6";
}
