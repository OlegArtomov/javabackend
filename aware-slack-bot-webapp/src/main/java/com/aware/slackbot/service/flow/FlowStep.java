package com.aware.slackbot.service.flow;

import com.aware.services.converstation.ConversationEvent;
import com.aware.services.converstation.ConversationUser;
import org.apache.commons.lang3.tuple.Pair;

import static org.apache.commons.lang3.tuple.Pair.of;

/**
 * @author Oleg Artomov
 */
public abstract class FlowStep {

    private final FlowStep positiveStep;

    private final FlowStep negativeStep;

    private final String stepId;

    private final StepListener stepListener;

    protected static final Pair<Boolean, FlowStep> MISSED_NEXT_STEP = of(false, null);

    public FlowStep(FlowStep positiveStep, FlowStep negativeStep,
                    String stepId, StepListener stepListener) {
        this.positiveStep = positiveStep;
        this.negativeStep = negativeStep;
        this.stepId = stepId;
        this.stepListener = stepListener;
    }

    public StepListener getStepListener() {
        return stepListener;
    }

    public String getStepId() {
        return stepId;
    }

    public FlowStep getPositiveStep() {
        return positiveStep;
    }

    public FlowStep getNegativeStep() {
        return negativeStep;
    }

    public abstract Pair<Boolean, FlowStep> accept(ConversationEvent event, ConversationUser conversationUser, String answer);
}
