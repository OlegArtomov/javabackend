package com.aware.slackbot.service.flow;

import com.aware.model.AwareUser;
import com.aware.services.converstation.ConversationUser;
import com.aware.slack.SlackSession;

/**
 * @author Oleg Artomov
 */
public interface BotService {
    void reply(SlackSession slackSession, String sourceId, String message);

    AwareUser findUser(ConversationUser conversationUser);
}
