package com.aware.slackbot.service.events;

import com.aware.services.converstation.ConversationEvent;
import com.aware.services.converstation.ConversationUser;
import com.aware.slack.SlackSession;
import com.aware.slack.SlackUser;
import com.aware.slack.events.SlackMessagePosted;
import com.aware.slack.listeners.SlackMessagePostedListener;
import com.aware.slackbot.service.flow.SlackFlowService;
import org.slf4j.Logger;
import org.springframework.util.StopWatch;

import static com.aware.slackbot.utils.SlackBotUtils.fromSlackEvent;
import static com.aware.slackbot.utils.SlackBotUtils.fromSlackUser;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.util.StringUtils.isEmpty;
/**
 * @author Oleg Artomov
 */
public class AwareSlackMessageListener implements SlackMessagePostedListener {

    private static final Logger logger = getLogger(AwareSlackMessageListener.class);

    private final SlackFlowService slackFlowService;

    private final String botId;

    public AwareSlackMessageListener(SlackFlowService slackFlowService, String botId) {
        this.slackFlowService = slackFlowService;
        this.botId = botId;
    }

    @Override
    public void onEvent(SlackMessagePosted event, SlackSession session) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            SlackUser author = event.getSender();
            //TODO Fix me: Check why author can be null
            if ((author != null) && (!botId.equals(author.getId()))) {
                ConversationUser conversationUser = fromSlackUser(author);
                ConversationEvent conversationEvent = fromSlackEvent(event);
                String originalMessage = event.getMessageContent();
                if (!isEmpty(originalMessage)) {
                    logger.debug("Original message: {}", originalMessage);
                    slackFlowService.processMessage(originalMessage.trim(), conversationUser, conversationEvent);
                } else {
                    logger.warn("Empty message from user {}. We do not process it", author.getUserName());
                }
            }
        } catch (Exception e) {
            //TODO: Fix me
            logger.error("Error. Message {}", event, e);
        } finally {
            stopWatch.stop();
            logger.debug("Response time for slack listener: {}", stopWatch.getTotalTimeMillis());
        }
    }
}
