package com.aware.slackbot.service.core;

import com.aware.slack.SlackSession;
import com.aware.slackbot.format.RegisterBotRequest;
import com.aware.slackbot.format.SendToSlackRequest;
import com.aware.slackbot.service.flow.SlackService;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author Oleg Artomov
 */
@Service
public class RedisMessageListener {

    private static final Logger logger = getLogger(RedisMessageListener.class);

    @Resource
    private SlackSessionsManager slackSessionsManager;

    @Resource
    private SlackService slackService;

    public void sendFromSlack(SendToSlackRequest request) {
        SlackSession slackSessionById = slackSessionsManager.getSlackSessionById(request.getSessionId());
        if (slackSessionById != null) {
            slackService.sendAnswerFromSlack(slackSessionById, request.getChannelId(), request.getMessage());
        } else {
            logger.warn("Can not find session by id: {}", request.getSessionId());
        }
    }

    public void acceptCode(RegisterBotRequest request) {
        if (isNotEmpty(request.getCode())) {
            slackSessionsManager.acceptClientCode(request.getCode());
        } else {
            logger.warn("Empty accept code");
        }
    }

    public static final String SEND_FROM_SLACK_METHOD_NAME = "sendFromSlack";

    public static final String REGISTER_BOT_METHOD_NAME = "acceptCode";
}
