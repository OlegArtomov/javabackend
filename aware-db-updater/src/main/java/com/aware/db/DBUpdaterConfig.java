package com.aware.db;

import org.springframework.beans.factory.annotation.Value;

/**
 * @author Oleg Artomov
 */
public class DBUpdaterConfig {
    @Value("${db.username}")
    private String dbUserName;

    @Value("${db.password}")
    private String dbPassword;

    @Value("${db.jdbcurl}")
    private String dbJdbcUrl;

    public String getDbUserName() {
        return dbUserName;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public String getDbJdbcUrl() {
        return dbJdbcUrl;
    }
}
