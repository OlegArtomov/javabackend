package com.aware.db;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.slf4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Properties;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author Oleg Artomov
 */
public class DbUpdater {

    private final static Logger logger = getLogger(DbUpdater.class);

    public static void main(String[] args) throws JSchException {
        Session session = connectBySSH();
        try {
            new AnnotationConfigApplicationContext(DbUpdateConfiguration.class);
        } finally {
            session.disconnect();
        }
    }

    private static Session connectBySSH() throws JSchException {
        Properties config = new Properties();
        final String proxyHost = "107.178.222.167";
        final String proxyUser = "ubuntu";
        final String targetMySQLHost = "130.211.170.37";
        final int targetMySQLPort = 3306;
        final int localPort = 1015;
        final String privateKey = "D:\\WorkSoftware\\Amazon\\aware-java-google";
        JSch jsch = new JSch();
        Session session = jsch.getSession(proxyUser, proxyHost, 22);
        jsch.addIdentity(privateKey);
        config.put("StrictHostKeyChecking", "no");
        config.put("ConnectionAttempts", "3");
        session.setConfig(config);
        session.connect();
        session.setPortForwardingL(localPort, targetMySQLHost, targetMySQLPort);
        logger.info("SSH Connected");
        return session;
    }
}
