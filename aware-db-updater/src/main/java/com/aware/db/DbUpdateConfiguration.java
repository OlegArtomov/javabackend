package com.aware.db;

import com.aware.config.AwareProperties;
import com.aware.spring.resourceloaders.ClasspathResourceLoader;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

import static com.aware.AwareTestConstants.LIQUEBASE_CHANGELOG_XML;

/**
 * @author Oleg Artomov
 */
@Configuration
@Import(AwareProperties.class)
public class DbUpdateConfiguration {

    @Bean
    public DataSource dataSource(DBUpdaterConfig awareDaoConfig) {
        return new DriverManagerDataSource(awareConfiguration().getDbJdbcUrl(),
                awareDaoConfig.getDbUserName(), awareDaoConfig.getDbPassword());
    }

    @Bean
    public SpringLiquibase dbUpdater(final DataSource dataSource) {
        SpringLiquibase result = new SpringLiquibase();
        result.setDataSource(dataSource);
        result.setChangeLog(LIQUEBASE_CHANGELOG_XML);
        result.setContexts("update");
        result.setResourceLoader(new ClasspathResourceLoader());
        return result;
    }

    @Bean
    public DBUpdaterConfig awareConfiguration() {
        return new DBUpdaterConfig();
    }
}
