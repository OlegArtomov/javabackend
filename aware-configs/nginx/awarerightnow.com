server {
    listen 443 ssl;

    root /var/www/awarerightnow.com;
    index index.php index.html index.htm;

    server_name awarerightnow.com www.awarerightnow.com;

    ssl on;
    ssl_certificate /etc/nginx/ssl/nginx.crt;
    ssl_certificate_key /etc/nginx/ssl/nginx.key;
    ssl_session_timeout 5m;
    ssl_protocols TLSv1.2 TLSv1.1 TLSv1;
    ssl_ciphers HIGH:!aNULL:!eNULL:!LOW:!MD5;
    ssl_prefer_server_ciphers on;

    # BEGIN W3TC Browser Cache
    gzip on;
    gzip_types text/css text/x-component application/x-javascript application/json  application/javascript text/javascript text/x-js text/richtext image/svg+xml text/plain text$;

    location /blog {
        try_files $uri $uri/ /blog/index.php?q=$uri&$args;
    }

    location ~* ^.+\.(rss|atom|jpg|jpeg|gif|png|ico|rtf|js|css|json)$ {
            expires max;
    }
    location ~ ([^/]*)sitemap(.*)\.x(m|s)l$ {

    }
    location ~ ([^/]*)robots(.*)\.txt$ {

    }

    location ~ \.php$ {
        if ($request_uri ~ ^/([^?]*[^store\-address])\.php($|\?)[^blog\/]) {  return 302 /$1$args;  }
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;
    }

    location /images/ {

    }
    location /ngx_pagespeed_beacon {

    }

    
    location / {
        rewrite ^(/.*[^/][^blog\/])\.(html|php)$ $1/ permanent;
        rewrite ^(/.*[^/][^blog\/])(?!/)$ $1/ permanent;
        try_files $uri $uri.html $uri/ @extensionless-php;
        index index.php;
    }
    

    location @extensionless-php {
       rewrite ^(.*)/$ $1.php last;
    }
}

server {
    listen 80;
    server_name awarerightnow.com;
    rewrite ^(.*) https://awarerightnow.com$1 permanent;
}

server {
    listen 80;
    server_name www.awarerightnow.com;
    return 301 https://awarerightnow.com$request_uri;
}