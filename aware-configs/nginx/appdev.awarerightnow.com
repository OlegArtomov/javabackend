server {
        listen 443 ssl;

        root /var/www/appdev.awarerightnow.com/public/;
        index index.html index.htm;

        server_name appdev.awarerightnow.com;

        #ssl on;
        ssl_certificate /etc/nginx/ssl/nginx.crt;
        ssl_certificate_key /etc/nginx/ssl/nginx.key;
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_ciphers ALL:!DH:!EXPORT:!RC4:+HIGH:+MEDIUM:!LOW:!aNULL:!eNULL;

        location / {
                try_files $uri /index.html;
        }
}

server {
    listen 80;
    server_name appdev.awarerightnow.com;
    rewrite ^(.*) https://appdev.awarerightnow.com$1 permanent;
}