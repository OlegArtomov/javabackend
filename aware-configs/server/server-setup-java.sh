#!/usr/bin/env bash
# Sets up load balancer host with nginx & java app and frontend app

set -x
set -o nounset 
set -o errexit
set -o pipefail

# Check user
if [[ $(whoami) != 'ubuntu' ]]; then 
    echo "This script should be run as 'ubuntu' user";
    exit 1 
fi

# fix locale
sudo cat > /tmp/locale << EOL
LANGUAGE=en_US.UTF-8
LC_ALL=en_US.UTF-8
LANG=en_US.UTF-8
LC_TYPE=en_US.UTF-8
EOL
sudo cp /tmp/locale /etc/default/locale
sudo locale-gen en_US.UTF-8
sudo dpkg-reconfigure locales
reset

# Java8 repo & accept Oracle licence
sudo add-apt-repository -y ppa:webupd8team/java
echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | sudo debconf-set-selections

# Update packages
sudo apt-get update -y
sudo apt-get dist-upgrade -y

# Install required packages.
sudo apt-get install -y \
    git \
    oracle-java8-installer \
    maven \
    nginx \
    byobu \
    tig \
    glances



# Prepare ssh dir
mkdir -p ~/.ssh
chown ubuntu ~/.ssh
chmod 700 ~/.ssh

# Write deploy ssh key.
cat > ~/.ssh/id_rsa_deploy << EOL
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAyrk0azdJ499UNqXwh+X12+CryTjeJcMkI0iajTuc4zlZCtvV
I9UJcsIiLgEJN0eCryS/S9p9y9Oy88N1QLnJGUY0KC4Oab4jKIaWaBX0TMjtDrlV
GulJLCyJlFMaWQAs7HHFFUtwytd33GzRlsN0U7Fo063UQAlf2MQK738IW6GZ0AxQ
z8EykHxFM3ahOu+WQgcpkryzeuwW95NUjs36grHL3j8rTfffRL8CsHlyD/8ECUxd
A2kne9hQ40oBIEAxHESUFTgVgyuanpbdxk9awVgyumBbvQyoWicUdGpZUjac4fYN
LuTx2yTNf324ib16sI7ILvIJUkI0xSbqZn8QiQIDAQABAoIBAGpTKwgUzn9i0eN7
syVD2miBdPgsCG+b4udSnCT5IDcZC/ejBy4s/TCgW5bvWisRKZy81Exvg00VcqVh
Xa0eBDhqVs7AITUa0Yve0yIyaZigRZ1UXSupMYSms14FHCuT6Pv2S9z4XWc2DZqh
xGejie6ffRCQ+mK/x/KEGAw2/2Y28+ArZ0bECskB4OaEPk4SxEonJ2ykF/5tsJB3
CSX1jVP3YbRRAPPmvu5UBDPF9Q1lw6MhTHTECFj9lCmndLrL2CZXNC2A3z71WEg1
bLtq/811NR4A119qQTXZ2yDgp/8nHjMBrACru5D6aBwrsowyxMa47Gv4WByKPMI8
xk8nmRUCgYEA5d0QeGeVS8bSuMDSQGeOrtBvK9ryv80iGLopby4dnnSvNLLVT/vs
Jdvm1bN2sRPg3yJ3oXmcYz6fNrt8/DQ0Q4beUNAQ7rIfCaLpnTzjuN4QQO68rpTF
BJcDNB6pD02aeLHV6Y0rc8BPPmjm9NLOJJZZ12rIgKUOQW1onLk25LMCgYEA4cYj
mQ4zaYJvWHHrKZ0N7xXudkw9edhsDD2M+kLXOh5+emTWZcgQfA4HCkqAwWROGYPS
WE34A7klXz8A2E70jrxVXC1slqUprXlSXkEa71F2WrWw976EitlRpIJxYE2PMdLl
aBDvifD+Qo8Kx47yISFw1XihHRhnEcNZ7x8Pq9MCgYEAjwf707BAyiIxxRzWt5S4
yDaStqtLGeY/LTvJxJDhhVP+IcCV02tjCjvHa3p8hLtl1fLQlf3iPi8dSyVnJFgn
nU0y0P5nSuAzR//DdkAJQ2OKoZzsc4XHApAlQi1B7W/QiwG9pdJeE2y64o7MB9OU
yld0GAMuOD58nGHEEqF9ctkCgYAhJ4+isksqe1UOj+wQuFeLfJUAGkIM2pNDWOaS
+a3RgxjfsF8roy2IiibOhBJUU/6EEK+a+Bgrs8yhKAdfvHh9XXrLhoAjvy1XpDFM
VhM2gaf3uia6uueL3E348o7jrYARuP6nQZVrlFBCRe83Tu6PnvGK56QBOcwo958F
VnJesQKBgQCd7OykE+Xuj5jXgl+bGEvo1usAoVjlHHzTwunYAgaKmc9TDO+qhejU
qBOOAemq6kyFnHiOHrr92In9MN4Xujzik2d1hMegI2Wkn2KUHgKWEo8oxkvfCOKr
62GShQq/8+KxHe4fAw4Vt54Bjs5gts7hUUDBbpzSzE/PxsPAc9VCTA==
-----END RSA PRIVATE KEY-----
EOL
cat > ~/.ssh/id_rsa_deploy.pub << EOL
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDKuTRrN0nj31Q2pfCH5fXb4KvJON4lwyQjSJqNO5zjOVkK29Uj1QlywiIuAQk3R4KvJL9L2n3L07Lzw3VAuckZRjQoLg5pviMohpZoFfRMyO0OuVUa6UksLImUUxpZACzsccUVS3DK13fcbNGWw3RTsWjTrdRACV/YxArvfwhboZnQDFDPwTKQfEUzdqE675ZCBymSvLN67Bb3k1SOzfqCscvePytN999EvwKweXIP/wQJTF0DaSd72FDjSgEgQDEcRJQVOBWDK5qelt3GT1rBWDK6YFu9DKhaJxR0allSNpzh9g0u5PHbJM1/fbiJvXqwjsgu8glSQjTFJupmfxCJ ubuntu@deply-key
EOL

cp ~/.ssh/id_rsa_deploy.pub ~/.ssh/id_rsa.pub
cp ~/.ssh/id_rsa_deploy ~/.ssh/id_rsa

chmod 600 ~/.ssh/*


# Clone the server codebase.
ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts

#copy backend app
mkdir -p ~/aware
git clone git@bitbucket.org:awarebackend/java-backend.git ~/aware/backend-app

# Configure nginx
# Copy ssl configs, key and crt files
cd /etc/nginx/
sudo mkdir -p ssl
cd ssl

#Keys for Production, Developemnt & App
sudo cp ~/aware/backend-app/aware-configs/nginx/ssl/nginx.crt /etc/nginx/ssl/nginx.crt
sudo cp ~/aware/backend-app/aware-configs/nginx/ssl/nginx.key /etc/nginx/ssl/nginx.key
sudo chmod 600 /etc/nginx/ssl/nginx.key


#copy config files
cd /etc/nginx/sites-available/

#API
sudo cp ~/aware/backend-app/aware-configs/nginx/api.awarerightnow.com api.awarerightnow.com
sudo cp ~/aware/backend-app/aware-configs/nginx/apidev.awarerightnow.com apidev.awarerightnow.com


#link config files
cd /etc/nginx/sites-enabled/

# App + API
sudo ln -s /etc/nginx/sites-available/api.awarerightnow.com api.awarerightnow.com
sudo ln -s /etc/nginx/sites-available/apidev.awarerightnow.com apidev.awarerightnow.com

# Remove default file
sudo rm -f /etc/nginx/sites-enabled/default


sudo service nginx restart