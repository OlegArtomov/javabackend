package com.aware.repositories;

import com.aware.model.FacebookUserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author Oleg Artomov
 */
public interface FacebookUserInfoRepository extends JpaRepository<FacebookUserInfo, Long> {

    @Query("select u from #{#entityName} u where u.facebookUserId = ?1")
    FacebookUserInfo findByFacebookId(String facebookUserId);
}
