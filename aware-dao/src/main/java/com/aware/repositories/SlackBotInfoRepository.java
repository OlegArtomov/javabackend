package com.aware.repositories;

import com.aware.model.SlackBotInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author Oleg Artomov
 */
public interface SlackBotInfoRepository  extends JpaRepository<SlackBotInfo, Long> {
    @Query("select u from #{#entityName} u where u.teamId = ?1")
    SlackBotInfo findByTeamId(String teamId);
}
