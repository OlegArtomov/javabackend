package com.aware.repositories;

import com.aware.model.UserSubscription;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Oleg Artomov
 */
public interface UserSubscriptionRepository extends JpaRepository<UserSubscription, Long> {
}
