package com.aware.repositories;

import com.aware.model.AwareUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

/**
 * @author Oleg Artomov
 */
public interface AwareUserRepository extends JpaRepository<AwareUser, Long> {
    @Query("select u from #{#entityName} u where u.email = ?1")
    AwareUser findByEmail(String userEmail);

    @Query("select u from #{#entityName} u where ((u.email is not null) and (u.isTeacher = true) and (u.id != ?1))")
    List<AwareUser> findTeachersWithNotEmptyEmailExcludeUser(Long user);

    @Query("select u from #{#entityName} u where ((u.phoneNumber is not null) and (u.isTeacher = true) and (u.id != ?1))")
    List<AwareUser> findTeachersWithNotEmptyPhoneNumberExcludeUser(Long user);
}
