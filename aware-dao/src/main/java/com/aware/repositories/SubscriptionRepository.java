package com.aware.repositories;

import com.aware.model.Subscription;
import com.aware.model.SubscriptionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author Oleg Artomov
 */
public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {

    @Query("select m from #{#entityName} m  where m.type = ?1")
    Subscription findByType(SubscriptionType freeTrial);
}
