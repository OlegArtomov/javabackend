package com.aware.repositories;

import com.aware.model.Meditation;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Oleg Artomov
 */
public interface MeditationRepository extends JpaRepository<Meditation, Long> {
}
