package com.aware.config;

import org.springframework.beans.factory.annotation.Value;

/**
 * @author Oleg Artomov
 */
public class AwareDaoConfig {

    @Value("${db.username}")
    private String dbUserName;

    @Value("${db.password}")
    private String dbPassword;

    @Value("${db.jdbcurl}")
    private String dbJdbcUrl;

    @Value("${db.driverClassName}")
    private String dbDriverClassName;

    @Value("${db.validationQuery}")
    private String validationQuery;

    public String getDbUserName() {
        return dbUserName;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public String getDbJdbcUrl() {
        return dbJdbcUrl;
    }

    public String getDbDriverClassName() {
        return dbDriverClassName;
    }

    public String getValidationQuery() {
        return validationQuery;
    }
}
