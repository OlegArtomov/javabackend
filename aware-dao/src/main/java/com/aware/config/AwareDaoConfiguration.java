package com.aware.config;

import com.aware.spring.resourceloaders.CombinedResourceLoader;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import static com.aware.utils.AwareConstants.AWARE_BASE_PACKAGE;
import static com.aware.utils.AwareConstants.AWARE_HIBERNATE_PROPERTIES_FILE;
import static com.aware.utils.AwareConstants.AWARE_HIKARI_PROPERTIES_FILE;
import static com.aware.utils.AwareConstants.HIBERNATE_DEFAULT_PROPERTIES_FILE;
import static com.aware.utils.AwareConstants.HIKARI_DEFAULT_PROPERTIES_FILE;
import static com.aware.utils.PropertiesUtils.loadProperties;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.springframework.orm.jpa.vendor.Database.MYSQL;

/**
 * @author Oleg Artomov
 */
@Configuration
@EnableJpaRepositories(basePackages = AWARE_BASE_PACKAGE)
@EnableJpaAuditing
@EnableTransactionManagement(proxyTargetClass = true)
public class AwareDaoConfiguration {

    @Bean
    @DependsOn("dbUpdater")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            final CombinedResourceLoader combinedResourceLoader, final Database database, final DataSource dataSource) {
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setDatabase(database);
        factory.setJpaVendorAdapter(jpaVendorAdapter);
        factory.setPackagesToScan(AWARE_BASE_PACKAGE);
        factory.setJpaProperties(loadProperties(combinedResourceLoader, HIBERNATE_DEFAULT_PROPERTIES_FILE, AWARE_HIBERNATE_PROPERTIES_FILE));
        factory.setDataSource(dataSource);
        return factory;
    }

    @Bean
    public PlatformTransactionManager transactionManager(final EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }

    @Bean
    public Database database(){
        return MYSQL;
    }

    @Bean
    public Object dbUpdater(){
        return EMPTY;
    }

    @Bean
    public AwareDaoConfig awareDaoConfig(){
        return new AwareDaoConfig();
    }

    @Bean
    public DataSource dataSource(final CombinedResourceLoader combinedResourceLoader, final AwareDaoConfig awareDaoConfig) {
        HikariConfig config = new HikariConfig(loadProperties(combinedResourceLoader,
                HIKARI_DEFAULT_PROPERTIES_FILE, AWARE_HIKARI_PROPERTIES_FILE));
        config.setJdbcUrl(awareDaoConfig.getDbJdbcUrl());
        config.setUsername(awareDaoConfig.getDbUserName());
        config.setPassword(awareDaoConfig.getDbPassword());
        config.setDriverClassName(awareDaoConfig.getDbDriverClassName());
        config.setConnectionTestQuery(awareDaoConfig.getValidationQuery());
        return new HikariDataSource(config);
    }
}
