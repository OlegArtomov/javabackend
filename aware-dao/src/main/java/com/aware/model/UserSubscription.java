package com.aware.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Oleg Artomov
 */
@Entity
@Table(name = "user_subscription")
public class UserSubscription extends AwareAuditable {

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id")
    private AwareUser user;

    @ManyToOne(optional = false)
    @JoinColumn(name= "subscription_id")
    private Subscription subscription;

    public AwareUser getUser() {
        return user;
    }

    public void setUser(AwareUser user) {
        this.user = user;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    public static class Builder {

        private Long id;

        public Builder id(Long value){
            this.id = value;
            return this;
        }

        public UserSubscription build(){
            UserSubscription result = new UserSubscription();
            result.setId(id);
            return result;
        }
    }
}
