package com.aware.model;

/**
 * @author Oleg Artomov
 */
public enum SubscriptionType {
  FREE_TRIAL, REGULAR
}
