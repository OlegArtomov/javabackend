package com.aware.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import static javax.persistence.EnumType.STRING;

/**
 * @author Oleg Artomov
 */
@Entity
@Table(name = "subscription")
public class Subscription {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "type", nullable = false)
    @Enumerated(value = STRING)
    private SubscriptionType type;

    @Column(name = "name")
    private String name;

    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }

    public SubscriptionType getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Subscription that = (Subscription) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return type == that.type;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    public static class Builder {

        private SubscriptionType type;

        private String name;

        private Long id;

        public Subscription build() {
            Subscription result = new Subscription();
            result.type = type;
            result.name = name;
            result.id = id;
            return result;
        }

        public Builder type(SubscriptionType type) {
            this.type = type;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder id(Long id){
            this.id = id;
            return this;
        }
    }
}
