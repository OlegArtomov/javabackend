package com.aware.model;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.Date;

/**
 * @author Oleg Artomov
 */
@Entity
@Table(name = "meditation")
@DynamicUpdate
public class Meditation extends AwareAuditable {

    @ManyToOne
    @JoinColumn(name= "student_id")
    private AwareUser student;

    @ManyToOne(optional = true)
    @JoinColumn(name= "teacher_id")
    private AwareUser teacher;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "duration")
    private Integer duration;

    @Column(name = "teacher_score")
    private Byte teacherScore;

    @Column(name = "teacher_feedback")
    private String teacherFeedback;

    @Column(name = "student_score")
    private Byte studentScore;

    @Column(name = "student_feedback")
    private String studentFeedback;

    @Column(name = "completed", columnDefinition = "tinyint")
    private boolean completed;

    @Column(name = "open_tok_session_id")
    private String openTokSessionId;

    @Column(name = "open_tok_student_token")
    private String openTokStudentToken;

    @Column(name = "open_tok_teacher_token")
    private String openTokTeacherToken;

    @ManyToOne(optional = true)
    @JoinColumn(name= "user_subscription_id")
    private UserSubscription userSubscription;

    @Version
    @Column(name = "version")
    private Long version;

    public UserSubscription getUserSubscription() {
        return userSubscription;
    }

    public void setUserSubscription(UserSubscription userSubscription) {
        this.userSubscription = userSubscription;
    }

    public String getTeacherFeedback() {
        return teacherFeedback;
    }

    public void setTeacherFeedback(String teacherFeedback) {
        this.teacherFeedback = teacherFeedback;
    }

    public String getStudentFeedback() {
        return studentFeedback;
    }

    public void setStudentFeedback(String studentFeedback) {
        this.studentFeedback = studentFeedback;
    }

    public Byte getTeacherScore() {
        return teacherScore;
    }

    public void setTeacherScore(Byte teacherScore) {
        this.teacherScore = teacherScore;
    }

    public Byte getStudentScore() {
        return studentScore;
    }

    public void setStudentScore(Byte studentScore) {
        this.studentScore = studentScore;
    }

    public String getOpenTokSessionId() {
        return openTokSessionId;
    }

    public void setOpenTokSessionId(String openTokSessionId) {
        this.openTokSessionId = openTokSessionId;
    }

    public String getOpenTokStudentToken() {
        return openTokStudentToken;
    }

    public void setOpenTokStudentToken(String openTokStudentToken) {
        this.openTokStudentToken = openTokStudentToken;
    }

    public String getOpenTokTeacherToken() {
        return openTokTeacherToken;
    }

    public void setOpenTokTeacherToken(String openTokTeacherToken) {
        this.openTokTeacherToken = openTokTeacherToken;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public AwareUser getStudent() {
        return student;
    }

    public AwareUser getTeacher() {
        return teacher;
    }

    public void setStudent(AwareUser student) {
        this.student = student;
    }

    public void setTeacher(AwareUser teacher) {
        this.teacher = teacher;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Integer getDuration() {
        return duration;
    }

    public static class Builder {

        private Date createdDate;

        private AwareUser teacher;

        private Long id;

        public Meditation build() {
            Meditation result = new Meditation();
            result.creationDate = createdDate;
            result.teacher = teacher;
            result.setId(id);
            return result;
        }

        public Builder createdDate(Date createdDate) {
            this.createdDate = createdDate;
            return this;
        }

        public Builder teacher(AwareUser teacher) {
            this.teacher = teacher;
            return this;
        }

        public Builder id(Long id){
            this.id = id;
            return this;
        }
    }
}
