package com.aware.model;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Oleg Artomov
 */
@Entity
@Table(name = "facebook_user_info")
@DynamicUpdate
public class FacebookUserInfo {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "facebook_id")
    private String facebookUserId;

    @ManyToOne(optional = false)
    @JoinColumn(name= "user_id")
    private AwareUser user;

    @Column(name = "user_pic_url")
    private String userPicUrl;

    public String getUserPicUrl() {
        return userPicUrl;
    }

    public void setUserPicUrl(String userPicUrl) {
        this.userPicUrl = userPicUrl;
    }

    public void setFacebookUserId(String facebookUserId) {
        this.facebookUserId = facebookUserId;
    }

    public void setUser(AwareUser user) {
        this.user = user;
    }

    public AwareUser getUser() {
        return user;
    }

    public String getFacebookUserId() {
        return facebookUserId;
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FacebookUserInfo that = (FacebookUserInfo) o;

        return facebookUserId != null ? facebookUserId.equals(that.facebookUserId) : that.facebookUserId == null;

    }

    @Override
    public int hashCode() {
        return facebookUserId != null ? facebookUserId.hashCode() : 0;
    }

    public static class Builder {

        private String facebookUserId;

        private AwareUser user;

        private String userPicUrl;

        public FacebookUserInfo build() {
            FacebookUserInfo facebookUserInfo = new FacebookUserInfo();
            facebookUserInfo.setFacebookUserId(facebookUserId);
            facebookUserInfo.setUser(user);
            facebookUserInfo.setUserPicUrl(userPicUrl);
            return facebookUserInfo;
        }

        public Builder user(AwareUser awareUser) {
            this.user = awareUser;
            return this;
        }

        public Builder userPicUrl(String userPicUrl) {
            this.userPicUrl = userPicUrl;
            return this;
        }

        public Builder facebookUserId(String facebookUserId) {
            this.facebookUserId = facebookUserId;
            return this;
        }
    }
}
