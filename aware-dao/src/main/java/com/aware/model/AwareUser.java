package com.aware.model;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;
import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * @author Oleg Artomov
 */
@Entity
@Table(name = "user")
@DynamicUpdate
public class AwareUser extends AwareAuditable {

    @Column(name = "is_teacher", columnDefinition = "tinyint")
    private boolean isTeacher;

    @OneToOne(mappedBy = "user")
    private FacebookUserInfo facebookUserInfo;

    @Column(name = "user_email")
    private String email;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone_number")
    private String phoneNumber;

    @ManyToOne
    @JoinColumn(name= "current_subscription_id")
    private UserSubscription currentSubscription;

    @Column(name = "stripe_customer_id")
    private String stripeCustomerId;

    @Column(name = "customer_invoice_name")
    private String invoiceName;

    public String getInvoiceName() {
        return invoiceName;
    }

    public void setInvoiceName(String invoiceName) {
        this.invoiceName = invoiceName;
    }

    public String getStripeCustomerId() {
        return stripeCustomerId;
    }

    public void setStripeCustomerId(String stripeCustomerId) {
        this.stripeCustomerId = stripeCustomerId;
    }

    public void setCurrentSubscription(UserSubscription currentSubscription) {
        this.currentSubscription = currentSubscription;
    }

    public UserSubscription getCurrentSubscription() {
        return currentSubscription;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public FacebookUserInfo getFacebookUserInfo() {
        return facebookUserInfo;
    }

    public boolean isTeacher() {
        return isTeacher;
    }

    public void setTeacher(boolean teacher) {
        isTeacher = teacher;
    }

    public String getUserName() {
        if (!isEmpty(getFirstName()) || (!isEmpty(getLastName()))) {
            return defaultIfEmpty(getFirstName(), "") + " " + defaultIfEmpty(getLastName(), "");
        }
        return getEmail();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AwareUser awareUser = (AwareUser) o;

        if (isTeacher != awareUser.isTeacher) return false;
        return email != null ? email.equals(awareUser.email) : awareUser.email == null;

    }

    @Override
    public int hashCode() {
        int result = (isTeacher ? 1 : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }


    public static class Builder {

        private String email;

        private String phoneNumber;

        private String stripeCustomerId;

        private boolean teacher;

        private Long id;

        public AwareUser build() {
            AwareUser awareUser = new AwareUser();
            awareUser.setEmail(email);
            awareUser.setPhoneNumber(phoneNumber);
            awareUser.setTeacher(teacher);
            awareUser.setStripeCustomerId(stripeCustomerId);
            awareUser.setId(id);
            return awareUser;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder phoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public Builder teacher(boolean teacher) {
            this.teacher = teacher;
            return this;
        }

        public Builder stripeCustomerId(String stripeCustomerId) {
            this.stripeCustomerId = stripeCustomerId;
            return this;
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }
    }
}
