package com.aware.repositories;

import com.aware.config.AbstractRepositoryIT;
import com.aware.model.AwareUser;
import com.aware.model.FacebookUserInfo;
import org.junit.Test;

import javax.annotation.Resource;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Oleg Artomov
 */
public class FacebookUserInfoRepositoryIT extends AbstractRepositoryIT {

    @Resource
    private FacebookUserInfoRepository facebookUserInfoRepository;

    @Resource
    private AwareUserRepository awareUserRepository;

    @Test
    public void testFindByFacebookIdForExistingUser() {
        AwareUser awareUser = awareUserRepository.save(new AwareUser.Builder().email("aa@ukr.net").build());
        String facebookUserId = "11";
        FacebookUserInfo facebookUserInfo = new FacebookUserInfo.Builder().facebookUserId(facebookUserId)
                .user(awareUser).userPicUrl("http://google.com").build();
        facebookUserInfoRepository.save(facebookUserInfo);
        assertThat(facebookUserInfoRepository.findByFacebookId(facebookUserId), equalTo(facebookUserInfo));
    }

    @Test
    public void testFindByFacebookIdForUnknownUser() {
        AwareUser awareUser = awareUserRepository.save(new AwareUser.Builder().email("aa@ukr.net").build());
        String facebookUserId = "12";
        FacebookUserInfo facebookUserInfo = new FacebookUserInfo.Builder().facebookUserId(facebookUserId)
                .user(awareUser).userPicUrl("http://google.com").build();
        facebookUserInfoRepository.save(facebookUserInfo);
        assertThat(facebookUserInfoRepository.findByFacebookId("21"), nullValue());
    }
}