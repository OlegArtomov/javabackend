package com.aware.repositories;

import com.aware.config.AbstractRepositoryIT;
import com.aware.model.Subscription;
import org.junit.Test;

import javax.annotation.Resource;

import static com.aware.model.SubscriptionType.FREE_TRIAL;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Oleg Artomov
 */
public class SubscriptionRepositoryIT extends AbstractRepositoryIT {

    @Resource
    private SubscriptionRepository subscriptionRepository;

    @Test
    public void testFindByType(){
        subscriptionRepository.save(new Subscription.Builder().type(FREE_TRIAL).name("Free trial subscription").build());
        assertThat(subscriptionRepository.findByType(FREE_TRIAL).getType(), equalTo(FREE_TRIAL));
    }
}