package com.aware.repositories;

import com.aware.config.AbstractRepositoryIT;
import com.aware.model.AwareUser;
import org.junit.Test;

import javax.annotation.Resource;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Oleg Artomov
 */
public class AwareUserRepositoryIT extends AbstractRepositoryIT {

    @Resource
    private AwareUserRepository awareUserRepository;

    @Test
    public void testFindByEmailForExistingUser() {
        final String userEmail = "aa@ukr.net";
        awareUserRepository.save(new AwareUser.Builder().email(userEmail).build());
        assertThat(awareUserRepository.findByEmail(userEmail).getEmail(), equalTo(userEmail));
    }

    @Test
    public void testFindByEmailForUnExistingUser() {
        assertThat(awareUserRepository.findByEmail("UnknownUser"), nullValue());
    }

    @Test
    public void testFindTeachersWithNotEmptyEmailExcludeUserWhenEmailIsEmpty() {
        AwareUser awareUser = awareUserRepository.save(new AwareUser.Builder().teacher(true).build());
        assertThat(awareUserRepository.findTeachersWithNotEmptyEmailExcludeUser(awareUser.getId() + 1), empty());
    }

    @Test
    public void testFindTeachersWithNotEmptyEmailExcludeUserWhenUserIsStudent() {
        AwareUser awareUser = awareUserRepository.save(new AwareUser.Builder().teacher(false).email("test@ukr.net").build());
        assertThat(awareUserRepository.findTeachersWithNotEmptyEmailExcludeUser(awareUser.getId() + 1), empty());
    }

    @Test
    public void testFindTeachersWithNotEmptyEmailExcludeUserWhenExcludeCurrentUser() {
        AwareUser awareUser = awareUserRepository.save(new AwareUser.Builder().teacher(true).email("test@ukr.net").build());
        assertThat(awareUserRepository.findTeachersWithNotEmptyEmailExcludeUser(awareUser.getId()), empty());
    }

    @Test
    public void testFindTeachersWithNotEmptyEmailExcludeUser() {
        AwareUser awareUser = awareUserRepository.save(new AwareUser.Builder().teacher(true).email("test@ukr.net").build());
        assertThat(awareUserRepository.findTeachersWithNotEmptyEmailExcludeUser(awareUser.getId() + 1), hasSize(1));
    }

    @Test
    public void testFindTeachersWithNotEmptyPhoneNumberExcludeUserWhenPhoneNumberIsEmpty(){
        AwareUser awareUser = awareUserRepository.save(new AwareUser.Builder().teacher(true).build());
        assertThat(awareUserRepository.findTeachersWithNotEmptyPhoneNumberExcludeUser(awareUser.getId() + 1), empty());
    }

    @Test
    public void testFindTeachersWithNotEmptyPhoneNumberExcludeUserWhenUserIsStudent(){
        AwareUser awareUser = awareUserRepository.save(new AwareUser.Builder().teacher(false).build());
        assertThat(awareUserRepository.findTeachersWithNotEmptyPhoneNumberExcludeUser(awareUser.getId() + 1), empty());
    }

    @Test
    public void testFindTeachersWithNotEmptyPhoneNumberExcludeUserWhenUserIsCurrentUser(){
        AwareUser awareUser = awareUserRepository.save(new AwareUser.Builder().teacher(true).phoneNumber("+1").build());
        assertThat(awareUserRepository.findTeachersWithNotEmptyPhoneNumberExcludeUser(awareUser.getId()), empty());
    }

    @Test
    public void testFindTeachersWithNotEmptyPhoneNumberExcludeUser(){
        AwareUser awareUser = awareUserRepository.save(new AwareUser.Builder().teacher(true).phoneNumber("+1").build());
        assertThat(awareUserRepository.findTeachersWithNotEmptyPhoneNumberExcludeUser(awareUser.getId() + 1), hasSize(1));
    }
}