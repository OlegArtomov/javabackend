package com.aware.config;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.aware.AwareTestConstants.DEFAULTS_TEST_PROPERTIES;
import static com.aware.AwareTestConstants.TRUNCATE_ALL_DATA_SCRIPT;

/**
 * @author Oleg Artomov
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AwareDaoConfiguration.class, TestDaoConfiguration.class})
@Sql(scripts = {TRUNCATE_ALL_DATA_SCRIPT})
@TestPropertySource(locations = DEFAULTS_TEST_PROPERTIES)
public abstract class AbstractRepositoryIT {
}
