package com.aware.config;

import com.aware.spring.resourceloaders.ClasspathResourceLoader;
import com.aware.spring.resourceloaders.CombinedResourceLoader;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import liquibase.exception.LiquibaseException;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.orm.jpa.vendor.Database;

import javax.sql.DataSource;

import static com.aware.AwareTestConstants.LIQUEBASE_CHANGELOG_XML;
import static com.aware.AwareTestConstants.SET_MYSQL_SYNTAX_SCRIPT;
import static org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType.HSQL;

/**
 * @author Oleg Artomov
 */
@Import(AwareProperties.class)
public class TestDaoConfiguration {
    @Bean
    @Primary
    public DataSource dataSource() throws LiquibaseException {
        EmbeddedDatabase embeddedDatabase = new EmbeddedDatabaseBuilder().setType(HSQL).
                addScripts(SET_MYSQL_SYNTAX_SCRIPT).build();
        HikariConfig config = new HikariConfig();
        config.setDataSource(embeddedDatabase);
        return new HikariDataSource(config);
    }

    @Bean
    @Primary
    public SpringLiquibase dbUpdater() throws LiquibaseException {
        SpringLiquibase result = new SpringLiquibase();
        result.setDataSource(dataSource());
        result.setChangeLog(LIQUEBASE_CHANGELOG_XML);
        result.setResourceLoader(new ClasspathResourceLoader());
        return result;
    }

    @Bean
    public Database database(){
        return Database.HSQL;
    }

    @Bean
    public CombinedResourceLoader resourceLoader() {
        return new CombinedResourceLoader(new ClasspathResourceLoader());
    }
}
